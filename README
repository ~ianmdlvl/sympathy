Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
All rights reserved.

$Id: README,v 1.2 2008/03/10 11:49:32 james Exp $

       Sympathy is a replacement for screen(1), minicom(1) and consolidate(1).
It  is  a VT52/VT100/ANSI terminal emulator with some special features.  In
normal use sympathy would sit between a terminal  device  (a  serial port  or
a  pseudo-tty)  and  the  user’s  usual terminal emulator (eg xterm(1)).
Sympathy renders data from  the  terminal  device  into  an internal  frame
buffer and then expresses changes in this frame buffer to the outer terminal
emulator using a  small  subset  of  ANSI  escape codes.   Sympathy always
generates valid escape codes to the outer ter- minal, and will reset the state
of its internal terminal emulator  when it detects receive errors on the
terminal device.

       Sympathy, unlike screen(1), takes care to preserve the scroll-back fea-
tures of the outer terminal emulator: lines that scroll off the top  of the
internal frame buffer are scrolled off the top of the outer termi- nal
emulator. When sympathy is used in client/server mode, some history is added to
the outer terminal emulator when the client connects.

       Sympathy also supports automatic baud-rate detection, and advanced log-
ging features.  Sympathy logs whenever any of the modem  control  lines change
state, receive errors, and the contents of any line on the frame buffer as the
cursor moves off it.


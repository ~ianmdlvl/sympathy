/* 
 * clients.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: clients.h,v 1.12 2008/05/09 12:35:57 james Exp $
 */

/* 
 * $Log: clients.h,v $
 * Revision 1.12  2008/05/09 12:35:57  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/11 17:56:04  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/11 17:47:24  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/02 10:27:24  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/14 10:34:47  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/14 10:34:30  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/14 00:57:58  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 18:05:06  james
 * *** empty log message ***
 *
 */

#ifndef __CLIENTS_H__
#define __CLIENTS_H__

typedef struct Client_struct
{
  struct Client_struct *next;
  int initialized;
  Socket *s;
  int dead;
} Client;

typedef struct
{
  Client *head;
  int n;
} Clients;


/* clients.c */
extern void client_free (Client * c);
extern Client *clients_new_client (Clients * cs, Socket * s, Context * ctx);
extern void clients_reap (Clients * cs, Context * ctx);
extern Clients *clients_new (void);
extern void clients_pre_select (Clients * cs, fd_set * rfds, fd_set * wfds);
extern void clients_post_select (Clients * cs, Context * ctx, fd_set * rfds,
                                 fd_set * wfds);
extern void clients_shutdown (Clients * cs, Context * ctx);

extern int send_output (Clients * cs, void *buf, int len);
extern int send_status (Clients * cs, char *msg);
extern void send_history (History * h, Client * c);
extern void send_vt102 (VT102 * v, Client * c);

#endif

/* 
 * expand.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: expand.c,v 1.6 2008/03/11 17:56:04 james Exp $";

/* 
 * $Log: expand.c,v $
 * Revision 1.6  2008/03/11 17:56:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/07 14:19:29  staffcvs
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/07 14:16:44  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/07 13:56:39  james
 * *** empty log message ***
 *
 */

#include <sympathy.h>
#include <string.h>


static int
xdigit_to_i (char c)
{
  switch (c) {
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
  case '8':
  case '9':
    return c - '0';
  case 'a':
  case 'b':
  case 'c':
  case 'd':
  case 'e':
  case 'f':
    return 0xa + (c - 'a');
  case 'A':
  case 'B':
  case 'C':
  case 'D':
  case 'E':
  case 'F':
    return 0xA + (c - 'A');
  }
  return -1;
}

static int
my_isxdigit (char c)
{
  return (xdigit_to_i (c) == -1) ? 0 : 1;
}


static int
my_isodigit (char c)
{
  switch (c) {
  case '0':
  case '1':
  case '2':
  case '3':
  case '4':
  case '5':
  case '6':
  case '7':
    return 1;
  }
  return 0;
}

static int
octal (const char **in)
{
  int o = 0;

  while (**in) {
    if (!my_isodigit (**in))
      return o;
    o <<= 3;
    o += (*((*in)++)) - '0';
  }

  return o;
}

static int
hex (const char **in)
{
  int x = 0;

  (*in)++;
  while (**in) {
    printf ("%c %d\n", **in, x);
    if (!my_isxdigit (**in))
      return x;
    x <<= 4;
    x += xdigit_to_i (*((*in)++));
  }

  return x;
}

char *
expand (const char *in, int *len)
{
  const char *iptr = in;
  int l;
  char *optr;
  char *ret;

  if (!in)
    return (char *) 0;
  l = strlen (in);

  optr = ret = malloc (l + 1);
  if (!ret)
    return ret;



  l = 0;

  while (*iptr) {
    if (*iptr == '\\') {
      iptr++;
      switch (*iptr) {
      case '\'':
      case '\"':
      case '\?':
      case '\\':
        *(optr++) = *(iptr++);
        l++;
        break;
      case 'a':
        *(optr++) = '\a';
        l++;
        iptr++;
        break;
      case 'b':
        *(optr++) = '\b';
        l++;
        iptr++;
        break;
      case 'f':
        *(optr++) = '\f';
        l++;
        iptr++;
        break;
      case 'n':
        *(optr++) = '\n';
        l++;
        iptr++;
        break;
      case 'r':
        *(optr++) = '\r';
        l++;
        iptr++;
        break;
      case 't':
        *(optr++) = '\t';
        l++;
        iptr++;
        break;
      case 'v':
        *(optr++) = '\v';
        l++;
        iptr++;
        break;
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
        *(optr++) = octal (&iptr);
        l++;
        break;
      case 'x':
        *(optr++) = hex (&iptr);
        l++;
        break;
      default:
        *(optr++) = '\\';
        l++;
        *(optr++) = *(iptr++);
        l++;
      }
    } else {
      *(optr++) = *(iptr++);
      l++;
    }
  }

  if (*len)
    *len = l;


  *(optr++) = 0;
  return ret;
}

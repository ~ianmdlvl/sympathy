/* 
 * mainloop.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: mainloop.h,v 1.10 2008/03/07 13:16:02 james Exp $
 */

/* 
 * $Log: mainloop.h,v $
 * Revision 1.10  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/02 10:27:24  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/27 09:42:53  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/27 09:42:21  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/27 01:31:14  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/20 18:49:11  staffcvs
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/20 18:33:37  james
 * *** empty log message ***
 *
 */

#ifndef __MAINLOOP_H__
#define __MAINLOOP_H__

#include <sympathy.h>

extern void mainloop (Context *, ANSI * a, Socket *, Socket *);
#endif /* __MAINLOOP_H__ */

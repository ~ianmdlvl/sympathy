/* 
 * usage.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] =
  "$Id: usage.c,v 1.28 2011/02/28 18:11:10 james Exp $";

/* 
 * $Log: usage.c,v $
 * Revision 1.28  2011/02/28 18:11:10  james
 * *** empty log message ***
 *
 * Revision 1.27  2008/05/09 12:35:57  james
 * *** empty log message ***
 *
 * Revision 1.26  2008/05/09 12:26:58  staffcvs
 * *** empty log message ***
 *
 * Revision 1.25  2008/05/09 12:19:18  james
 * *** empty log message ***
 *
 * Revision 1.24  2008/03/12 01:30:23  james
 * *** empty log message ***
 *
 * Revision 1.23  2008/03/12 01:26:56  james
 * *** empty log message ***
 *
 * Revision 1.22  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.21  2008/03/06 21:34:09  james
 * *** empty log message ***
 *
 * Revision 1.20  2008/03/06 17:21:41  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/03/03 06:04:18  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/02/28 16:57:51  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/02/27 18:29:49  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/27 16:01:24  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/27 09:55:57  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/27 09:42:53  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/27 09:42:21  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/27 01:31:38  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/27 01:31:14  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/24 00:43:55  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/23 11:48:52  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/20 18:49:11  staffcvs
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/20 18:31:44  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/20 17:18:33  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/20 02:11:35  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/16 10:58:52  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/16 01:30:56  james
 * *** empty log message ***
 *
 */

#include <stdio.h>
#include <stdlib.h>

void
usage (void)
{

  fprintf (stderr, "Usage:\n"
           "sympathy -t      [-K] [-d serialdev|-p] [-b baud] [-f] [-L log] [-w WxH]\n"
           "                      [-F] [-P pidfile] [-u] [-N]\n"
           "                      [pty program] [pty program args]\n"
           "sympathy -s      [-K] [-d serialdev|-p] [-b baud] [-f] [-L log] [-w WxH]\n"
           "                      [-F] [-P pidfile]           [-n hlines] [-k skt]\n"
           "                      [-S] [pty program] [pty program args]\n"
           "sympathy [-s -c] [-K] [-d serialdev|-p] [-b baud] [-f] [-L log] [-w WxH]\n"
           "                      [-F] [-P pidfile] [-u] [-N] [-n hlines] [-k skt]\n"
           "                      [-S] [pty program] [pty program args]\n"
           "sympathy -c      [-H] [-I string ]      [-u] [-N]              -k skt\n"
           "sympathy -r id   [-H] [-I string ]      [-u] [-N]\n"
           "sympathy -C            -d serialdev\n"
           "sympathy {-l|-ls}\n"
           "sympathy -v\n"
           "sympathy -h\n"
           "\n"
           "Main mode:\n"
           "   -t  terminal emulator mode: one process is started which reads from\n"
           "          the serial device or ptty and writes to the user's terminal.\n"
           "   -s  server mode: a process is started (and daemonized unless -F is\n"
           "          given) which listens on a socket, and reads from the serial\n"
           "          device or ptty.\n"
           "   -s -c  server and client mode: Fork a server and connect a client\n"
           "            incompatible with -F. This is the default mode\n"
           "   -c     client mode: connect to server mode process\n"
           "   -r id  client mode: connect to server mode process on socket\n"
           "             ~/.sympathy/id or ~/.sympathy/hostname.id if id is an\n"
           "             integer\n"
           "   -l or -ls  list active sockets in ~/.sympathy\n"
           "   -C   clear inactive locks\n"
           "   -v   show version\n"
           "   -h   show help\n"
           "\n"
           "Options:\n"
           "   -K  lock the serial device. By default sympathy checks that no\n"
           "          other process has created a lock file, and temporarily ceases\n"
           "          to access the serial port if it spots one. With this option\n"
           "          sympathy creates a lock file and prevents other programs\n"
           "          accessing the serial port. Incompatible with -p\n"
           "   -d serialdev  the serial device to connect to, eg /dev/ttyS0\n"
           "   -p   fork a login shell in a ptty and connect to that rather than\n"
           "           connect to a serial port. This option is the default if\n"
           "           no -d option is specified\n"
           "   -b baud  the baudrate to set. If omitted sympathy does not set\n"
           "              a baudrate and uses the current setting\n"
           "   -f  set RTS/CTS flowcontrol. By default sympathy disables flow\n"
           "         control\n"
           "   -k socket  explicity set the name of the socket, by default\n"
           "                sympathy will use ~/.sympathy/$pid\n"
           "   -F  do not detach, run the server in the foreground\n"
           "   -H  instead of connecting the user's terminal to the session\n"
           "         emit HTML of the current state of the screen on stdout\n"
           "   -L log  log activity on the device to the file log\n"
           "   -n nlines  the number of lines of history to store in the\n"
           "                 server, that are replayed on connexion\n"
           "   -P pidfile  write the pid of the server/terminal process to pidfile\n"
           "   -R  rotate logile specified with -L option\n"
           "   -S  log errors to syslog\n"
           "   -u  don't emit utf-8 try to use ISO-2202 to the local terminal\n"
           "   -w W[xH]  start session with a screen of size W by H. 0<W<=132,\n"
           "             0<H<24, default 80 by 24\n"
           "   -N  provide dumb terminal emulation on stdin/stdout instead of\n"
           "              redering the display, for use with expect(1)\n"
           "   -I string  inject string into terminal then quit (see man page\n"
           "                    for escapes\n"
           "   -B  log the raw data to the log file, rather than running it\n"
	   "         thru the parity error, unicode, and vt102 parsers\n" );

  exit (1);
}

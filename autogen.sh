#! /bin/sh
#
# bootstrap:
#
# Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
# All rights reserved.
#
# $Id: autogen.sh,v 1.2 2008/03/07 14:36:25 james Exp $
#
# $Log: autogen.sh,v $
# Revision 1.2  2008/03/07 14:36:25  james
# *** empty log message ***
#
# Revision 1.1  2008/03/07 14:32:55  james
# *** empty log message ***
#
# Revision 1.1  2008/02/03 16:20:24  james
# *** empty log message ***
#
#
#
#
#
libtoolize -f -c  --automake
aclocal
autoheader
autoconf
automake -a -c -Wno-portability
#automake -a -c Makefile
#automake -a -c src/Makefile
#automake -a -c apps/Makefile
#automake -a -c test/Makefile

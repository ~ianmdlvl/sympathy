/* 
 * ansi.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: ansi.c,v 1.50 2008/03/07 13:16:02 james Exp $";

/* 
 * $Log: ansi.c,v $
 * Revision 1.50  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.49  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.48  2008/03/06 22:51:39  james
 * *** empty log message ***
 *
 * Revision 1.47  2008/03/06 21:34:09  james
 * *** empty log message ***
 *
 * Revision 1.46  2008/03/06 21:33:02  james
 * *** empty log message ***
 *
 * Revision 1.45  2008/03/06 17:21:41  james
 * *** empty log message ***
 *
 * Revision 1.44  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.43  2008/03/06 01:41:48  james
 * *** empty log message ***
 *
 * Revision 1.42  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.41  2008/03/02 12:30:54  staffcvs
 * *** empty log message ***
 *
 * Revision 1.40  2008/03/02 10:50:32  staffcvs
 * *** empty log message ***
 *
 * Revision 1.39  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.38  2008/03/02 10:27:24  james
 * *** empty log message ***
 *
 * Revision 1.37  2008/02/29 22:50:29  james
 * *** empty log message ***
 *
 * Revision 1.36  2008/02/28 22:00:42  james
 * *** empty log message ***
 *
 * Revision 1.35  2008/02/28 16:57:51  james
 * *** empty log message ***
 *
 * Revision 1.34  2008/02/27 09:42:53  james
 * *** empty log message ***
 *
 * Revision 1.33  2008/02/27 09:42:21  james
 * *** empty log message ***
 *
 * Revision 1.32  2008/02/26 23:56:12  james
 * *** empty log message ***
 *
 * Revision 1.31  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.30  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.29  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.28  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.27  2008/02/20 22:54:22  staffcvs
 * *** empty log message ***
 *
 * Revision 1.26  2008/02/20 20:16:07  james
 * *** empty log message ***
 *
 * Revision 1.25  2008/02/20 19:44:37  james
 * @@
 *
 * Revision 1.24  2008/02/20 19:36:06  james
 * @@
 *
 * Revision 1.23  2008/02/20 19:25:09  james
 * *** empty log message ***
 *
 * Revision 1.22  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.21  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.20  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/02/07 13:22:51  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/07 13:19:48  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/07 12:21:16  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/07 11:32:41  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/07 11:11:14  staffcvs
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/07 01:02:52  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/07 00:43:27  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/06 20:26:57  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/06 17:53:28  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/06 15:53:22  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/04 05:45:55  james
 * ::
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */
#include "project.h"


static int
ansi_move (ANSI * a, CRT_Pos p)
{
  char buf[16];
  int n;
  int dx = p.x - a->pos.x;
  int dy = p.y - a->pos.y;
  int err = 0;

  // a->pos.x = ANSI_INVAL;

  if (a->pos.x != ANSI_INVAL) {

    if ((!dx) && (!dy))
      return 0;

    if (!dy) {
      if (dx == 1) {
        if (a->terminal->xmit (a->terminal, "\033[C", 3) != 3)
          err++;
      } else if (dx == -1) {
        if (a->terminal->xmit (a->terminal, "\033[D", 3) != 3)
          err++;
      } else {
        n = snprintf (buf, sizeof (buf), "\033[%dG", p.x + 1);
        if (a->terminal->xmit (a->terminal, buf, n) != n)
          err++;
      }
    } else if (!dx) {
      if (dy == -1) {
        if (a->terminal->xmit (a->terminal, "\033[A", 3) != 3)
          err++;
      } else if (dy == 1) {
        if (a->terminal->xmit (a->terminal, "\033[B", 3) != 3)
          err++;
      } else if (dy < 0) {
        n = snprintf (buf, sizeof (buf), "\033[%dA", -dy);
        if (a->terminal->xmit (a->terminal, buf, n) != n)
          err++;
      } else {
        n = snprintf (buf, sizeof (buf), "\033[%dB", dy);
        if (a->terminal->xmit (a->terminal, buf, n) != n)
          err++;
      }
    } else if (!p.x) {
      if (dy == 1) {
        if (a->terminal->xmit (a->terminal, "\033[E", 3) != 3)
          err++;
      } else if (dy == -1) {
        if (a->terminal->xmit (a->terminal, "\033[F", 3) != 3)
          err++;
      } else if (dy > 0) {
        n = snprintf (buf, sizeof (buf), "\033[%dE", dy);
        if (a->terminal->xmit (a->terminal, buf, n) != n)
          err++;
      } else {
        n = snprintf (buf, sizeof (buf), "\033[%dF", -dy);
        if (a->terminal->xmit (a->terminal, buf, n) != n)
          err++;
      }
    } else {
      n = snprintf (buf, sizeof (buf), "\033[%d;%dH", p.y + 1, p.x + 1);
      if (a->terminal->xmit (a->terminal, buf, n) != n)
        err++;
    }
  } else {
    n = snprintf (buf, sizeof (buf), "\033[%d;%dH", p.y + 1, p.x + 1);
    if (a->terminal->xmit (a->terminal, buf, n) != n)
      err++;
  }

  a->pos = p;

  return err;
}


static int
ansi_showhide_cursor (ANSI * a, int hide)
{
  int err = 0;
  if (a->hide_cursor == hide)
    return err;

  if (hide) {
    if (a->terminal->xmit (a->terminal, "\033[?25l", 6) != 6)
      err++;
  } else {
    if (a->terminal->xmit (a->terminal, "\033[?25h", 6) != 6)
      err++;
  }

  a->hide_cursor = hide;
  return err;
}


static int
ansi_force_attr_normal (ANSI * a)
{
  if (a->terminal->xmit (a->terminal, "\033[0m", 4) != 4)
    return 1;
  a->attr = CRT_ATTR_NORMAL;
  a->color = ANSI_INVAL;
  return 0;
}

static int
ansi_set_title (ANSI * a, char *t)
{
  char buf[1024];
  int i;
  char *term = getenv ("TERM");

  if (!term)
    return 0;
  if (strncmp (term, "xterm", 5) && strncmp (term, "rxvt", 4))
    return 0;

  i = sprintf (buf, "\033]0;%s\007", t);

  if (a->terminal->xmit (a->terminal, buf, i) != i)
    return 1;
  return 0;
}

static int
ansi_set_color (ANSI * a, int color)
{
  int dif;
  char buf[16];
  int i;
  int fg, bg;

  if ((a->color != ANSI_INVAL) && (color == a->color))
    return 0;
  fg = CRT_COLOR_FG (color);
  bg = CRT_COLOR_BG (color);

  if (fg & CRT_COLOR_INTENSITY) {
    fg += 90;
  } else {
    fg += 30;
  }

  if (bg & CRT_COLOR_INTENSITY) {
    bg += 100;
  } else {
    bg += 40;
  }

  i = sprintf (buf, "\033[%d;%dm", fg, bg);
#if 0
  fprintf (stderr, "Color set to %d %d %x\n", fg, bg, color);
#endif

  if (a->terminal->xmit (a->terminal, buf, i) != i)
    return 1;
  a->color = color;

  return 0;
}

static int
ansi_set_attr (ANSI * a, int attr)
{
  int dif;
  int err = 0;

  dif = attr ^ a->attr;

  if (!dif)
    return 0;

  a->attr = attr;

#if 0
  if (attr == CRT_ATTR_NORMAL) {
    ansi_force_attr_normal (a);
    return;
  }
#endif

  if (dif & CRT_ATTR_UNDERLINE) {
    if (attr & CRT_ATTR_UNDERLINE) {
      if (a->terminal->xmit (a->terminal, "\033[4m", 4) != 4)
        err++;
    } else {
      if (a->terminal->xmit (a->terminal, "\033[24m", 5) != 5)
        err++;
    }
  }
  if (dif & CRT_ATTR_REVERSE) {
    if (attr & CRT_ATTR_REVERSE) {
      if (a->terminal->xmit (a->terminal, "\033[7m", 4) != 4)
        err++;
    } else {
      if (a->terminal->xmit (a->terminal, "\033[27m", 5) != 5)
        err++;
    }
  }
  if (dif & CRT_ATTR_BOLD) {
    if (attr & CRT_ATTR_BOLD) {
      if (a->terminal->xmit (a->terminal, "\033[1m", 4) != 4)
        err++;
    } else {
      if (a->terminal->xmit (a->terminal, "\033[21m", 5) != 5)
        err++;
      if (a->terminal->xmit (a->terminal, "\033[22m", 5) != 5)
        err++;
    }
  }

  return err;
}

static int
ascii_emit (TTY * t, uint32_t ch)
{
  int i;

  /* Some quick obvious subsititons for quotation marks */
  switch (ch) {
  case 0x2018:
    ch = '`';
    break;
  case 0x2019:
    ch = '\'';
    break;
  case 0x201c:
  case 0x201d:
    ch = '"';
    break;
  }

  /* Short cut the easy stuff */
  if (ch < 0x7f) {
    uint8_t c = ch;
    return (t->xmit (t, &c, 1) == 1) ? 0 : -1;
  }

  for (i = 0; i < VT102_CHARSET_SIZE; ++i) {
    if (vt102_charset_gl[i] == ch) {
      uint8_t c[3] = { 016, i, 017 };
      return (t->xmit (t, &c, 3) == 3) ? 0 : -1;
    }
  }

  return (t->xmit (t, "?", 1) == 1) ? 0 : -1;

}

static int
ansi_render (ANSI * a, CRT_CA ca)
{
  int err = 0;
  int dif;

  if ((ca.chr < VT102_CHARSET_SIZE) && (vt102_charset_c0[ca.chr]))
    ca.chr = vt102_charset_c0[ca.chr];

  if ((ca.chr >= 0x80) && (ca.chr < 0xa0))
    ca.chr = ' ';

  ansi_set_attr (a, ca.attr);
  ansi_set_color (a, ca.color);

  if (a->utf8) {
    if (utf8_emit (a->terminal, ca.chr))
      err++;
  } else {
    if (ascii_emit (a->terminal, ca.chr))
      err++;
  }

  a->pos.x++;

  /* Can't easily wrap round here as don't know size of destination screen */
  /* so invalidate the cached cursor position */

  if (a->pos.x >= a->size.x)
    a->pos.x = ANSI_INVAL;

  return err;
}

static int
ansi_cls (ANSI * a)
{
  CRT_Pos p = { 0 };
  int err;

  crt_cls (&a->crt);

  err += ansi_set_attr (a, CRT_ATTR_NORMAL);
  err += ansi_set_color (a, CRT_COLOR_NORMAL);
  err += ansi_move (a, p);
  if (a->terminal->xmit (a->terminal, "\033[2J", 4) != 4)
    err++;
  /* different emulators leave cursor in different places after cls
     differently */
  a->pos.x = ANSI_INVAL;

  return err;
}

static int
ansi_draw_line (ANSI * a, CRT_CA * cap, int y)
{
  int err = 0;

  CRT_Pos p = { 0, y };
  CRT_CA *acap = &a->crt.screen[CRT_ADDR_POS (&p)];

  for (p.x = 0; p.x < a->crt.size.x; ++p.x) {
    if (p.x >= a->size.x)
      continue;

    if (crt_ca_cmp (*acap, *cap)) {
      err += ansi_showhide_cursor (a, 1);

      *acap = *cap;

      err += ansi_move (a, p);
      err += ansi_render (a, *acap);
    }

    acap++;
    cap++;
  }

  return err;
}

static int
ansi_resize_check (ANSI * a, CRT_Pos * size)
{
  int err = 0;

  if ((size && crt_pos_cmp (a->crt.size, *size))
      || crt_pos_cmp (a->terminal->size, a->size)) {

    terminal_getsize (a->terminal);

    a->size = a->terminal->size;

    a->pos.x = ANSI_INVAL;
    a->hide_cursor = ANSI_INVAL;

    crt_reset (&a->crt);

    if (size)
      a->crt.size = *size;

    // a->terminal->xmit (a->terminal, "\033c", 3);
    // maybe - issue 132 column command if we're 132?

    ansi_cls (a);
    if (a->terminal->xmit (a->terminal, "\033=", 2) != 2)
      err++;
    if (a->terminal->xmit (a->terminal, "\033[?6l", 5) != 5)
      err++;
    if (a->terminal->xmit (a->terminal, "\033[r", 3) != 3)
      err++;
    if (a->utf8) {
      if (a->terminal->xmit (a->terminal, "\033%G", 3) != 3)
        err++;
    } else {
      if (a->terminal->xmit (a->terminal, "\033(B", 3) != 3)
        err++;
      if (a->terminal->xmit (a->terminal, "\033)0", 3) != 3)
        err++;
      if (a->terminal->xmit (a->terminal, "\017", 1) != 3)
        err++;
    }

  }
  return err;
}


static int
ansi_history (ANSI * a, History * h)
{
  char buf[32];
  int i;
  int guess_scroll;
  int err = 0;
  /* Do we need to catch up on history? */

  if (a->history_ptr == h->wptr)
    return err;

  err += ansi_resize_check (a, NULL);

  if ((a->size.x < a->crt.size.x) || (a->size.y < a->crt.size.y))
    return err;

  guess_scroll = a->crt.size.y - 1; /* Bototm line should be a status line */


  err += ansi_force_attr_normal (a);
  err += ansi_set_color (a, CRT_COLOR_NORMAL);

  i = sprintf (buf, "\033[%d;%dr", 1, guess_scroll);
  if (a->terminal->xmit (a->terminal, buf, i) != i)
    err++;


  while (a->history_ptr != h->wptr) {

    History_ent *e = &h->lines[a->history_ptr];

    HISTORY_INC (h, a->history_ptr);

    if (!e->valid)
      continue;

    /* If so write the line ot the top of the screen */
    err += ansi_draw_line (a, e->line, 0);


    /* Roll guess_scroll lines up putting the top line into the xterm's
       history */


    /* Make extra lines a predictable colour */
    err += ansi_set_color (a, CRT_COLOR_NORMAL);

    err += ansi_showhide_cursor (a, 1);

    i = sprintf (buf, "\033[%d;%dH", guess_scroll, 1);
    if (a->terminal->xmit (a->terminal, buf, i) != i)
      err++;

    if (a->terminal->xmit (a->terminal, "\033D", 2) != 2)
      err++;

    a->pos.x = ANSI_INVAL;

    /* now do the same in our image of the screen */

    {
      CRT_Pos s = { 0 }
      , e = {
      0};

      /* scroll lines up */
      for (s.y++; s.y < guess_scroll; s.y++, e.y++) {
        memcpy (&a->crt.screen[CRT_ADDR_POS (&e)],
                &a->crt.screen[CRT_ADDR_POS (&s)],
                sizeof (CRT_CA) * a->crt.size.x);
      }

      /* erase new line */
      s.y = e.y;
      e.x = CRT_COLS - 1;
      crt_erase (&a->crt, s, e, 1, CRT_COLOR_NORMAL);
    }

  }
  /* reset margins */
  if (a->terminal->xmit (a->terminal, "\033[r", 3) != 3)
    err++;

  a->pos.x = ANSI_INVAL;

  return err;
}




static int
ansi_draw (ANSI * a, CRT * c)
{
  CRT_Pos p;
  int o;
  int hidden_cursor = 0;
  int err = 0;

  err += ansi_resize_check (a, &c->size);


  for (p.y = 0; p.y < a->crt.size.y; ++p.y) {
    if (p.y >= a->size.y)
      continue;

    err += ansi_draw_line (a, &c->screen[CRT_ADDR (p.y, 0)], p.y);

  }


  if ((c->size.x > a->size.x) || (c->size.y > a->size.y)) {
    char msg[1024];             // = "Window is too small";
    int i;
    p.x = 0;
    p.y = 0;

    i =
      sprintf (msg, "Window too small (%dx%d need %dx%d)", a->size.x,
               a->size.y, c->size.x, c->size.y);

    err += ansi_showhide_cursor (a, 1);
    err += ansi_set_attr (a, CRT_ATTR_REVERSE);
    err +=
      ansi_set_color (a, CRT_MAKE_COLOR (CRT_COLOR_WHITE, CRT_COLOR_RED));
    err += ansi_move (a, p);

    if (a->terminal->xmit (a->terminal, msg, i) != i)
      err++;

    a->pos.x = ANSI_INVAL;
  }


  if ((c->pos.x >= a->size.x) || (c->pos.y >= a->size.y)) {
    err += ansi_showhide_cursor (a, 1);
    return err;
  }

  a->crt.pos = c->pos;
  err += ansi_move (a, a->crt.pos);

  a->crt.hide_cursor = c->hide_cursor;
  err += ansi_showhide_cursor (a, a->crt.hide_cursor);

  return err;
}

static int
ansi_reset (ANSI * a, CRT * c)
{
  a->size.x = -1;
  return ansi_draw (a, c ? c : &a->crt);
}

static void
ansi_terminal_reset (ANSI * a)
{
  CRT_Pos p = { 0, a->crt.size.y };
  ansi_force_attr_normal (a);

  ansi_move (a, p);
}


static int
ansi_key (ANSI * a, Context * c, int key)
{
  if (!c->d)
    return c->k->key (c->k, c, key);

  cmd_show_status (c->d, c);

  if (c->d->active) {
    if (key == CMD_CANCEL_KEY) {
      return cmd_deactivate (c->d, c);
    } else if (key == CMD_KEY) {
      cmd_deactivate (c->d, c);
    } else {
      return cmd_key (c->d, c, a, key);
    }
  } else if (key == CMD_KEY) {
    return cmd_activate (c->d, c);
  }

  return c->k->key (c->k, c, key);
}


static void
ansi_flush_escape (ANSI * a, Context * c)
{
  ANSI_Parser *p = &a->parser;
  int i;

  for (i = 0; i < p->escape_ptr; ++i) {
    ansi_key (a, c, p->escape_buf[i]);
  }

  p->escape_ptr = 0;
  p->in_escape = 0;
}

static void
ansi_parse_deckey (ANSI * a, Context * c)
{
  ANSI_Parser *p = &a->parser;
  if ((p->escape_buf[1] != '[') && (p->escape_buf[1] != 'O')) {
    ansi_flush_escape (a, c);
    return;
  }

  if ((p->escape_buf[2] >= 'A') || (p->escape_buf[2] <= 'Z')) {
    ansi_key (a, c, KEY_UP + (p->escape_buf[2] - 'A'));
  } else if ((p->escape_buf[2] >= 'a') || (p->escape_buf[2] <= 'z')) {
    ansi_key (a, c, KEY_154 + (p->escape_buf[2] - 'a'));
  } else {
    ansi_flush_escape (a, c);
    return;
  }
  p->in_escape = 0;
  p->escape_ptr = 0;
}

static void
ansi_parse_ansikey (ANSI * a, Context * c)
{
  ANSI_Parser *p = &a->parser;
  int l = p->escape_ptr - 1;
  char *end;
  int k;

  if ((p->escape_buf[1] != '[') || (p->escape_buf[l] != '~')) {
    ansi_flush_escape (a, c);
    return;
  }

  k = strtol (&p->escape_buf[2], &end, 10);

  if (end != &p->escape_buf[l]) {
    ansi_flush_escape (a, c);
    return;
  }

  ansi_key (a, c, KEY_180 + k);

  p->in_escape = 0;
  p->escape_ptr = 0;
}



static void
ansi_parse_escape (ANSI * a, Context * c)
{
  ANSI_Parser *p = &a->parser;
  switch (p->escape_ptr) {
  case 0:
  case 1:
    return;
  case 2:
    switch (p->escape_buf[1]) {
    case '[':
    case 'O':
      break;
    default:
      ansi_flush_escape (a, c);
    }
    break;
  case 3:
    switch (p->escape_buf[1]) {
    case 'O':
      ansi_parse_deckey (a, c);
      break;
    case '[':
      if ((p->escape_buf[2] >= 'A') && (p->escape_buf[2] <= 'Z'))
        ansi_parse_deckey (a, c);
      break;
    default:
      ansi_flush_escape (a, c);
    }
    break;
  case 4:
  case 5:
  case 6:
  case 7:
    if (p->escape_buf[p->escape_ptr - 1] == '~')
      ansi_parse_ansikey (a, c);
    break;
  default:
    ansi_flush_escape (a, c);
  }
}


static void
ansi_check_escape (ANSI * a, Context * c)
{
  ANSI_Parser *p = &a->parser;
  struct timeval now, diff;
  gettimeofday (&now, NULL);
  timersub (&now, &p->last_escape, &diff);

#if 0
  fprintf (stderr, "ie %d tl %d.%06d eb %d\n",
           p->in_escape, diff.tv_sec, diff.tv_usec, p->escape_ptr);
#endif

  if (!p->in_escape)
    return;


  /* Time up? */
  if (diff.tv_sec || (diff.tv_usec > ANSI_ESCAPE_TIMEOUT))
    ansi_flush_escape (a, c);

}


static void
ansi_parse_char (ANSI * a, Context * c, int ch)
{
  ANSI_Parser *p = &a->parser;


  /* See if it's time to flush the escape */
  ansi_check_escape (a, c);

  if (ch == 033) {
    if (p->in_escape)
      ansi_flush_escape (a, c);

    p->in_escape++;
    p->escape_ptr = 0;
    gettimeofday (&p->last_escape, NULL);
  }

  if (p->in_escape) {
    p->escape_buf[p->escape_ptr++] = ch;
    ansi_parse_escape (a, c);
  } else {
    ansi_key (a, c, ch);
  }

}

static void
ansi_parse (ANSI * a, Context * c, char *buf, int len)
{
  while (len--)
    ansi_parse_char (a, c, *(buf++));
}

static int
ansi_dispatch (ANSI * a, Context * c)
{
  char buf[1024];
  int red;

  ansi_check_escape (a, c);


  if (!a->terminal)
    return 0;

  red = a->terminal->recv (a->terminal, buf, sizeof (buf));
  if (red <= 0)
    return red;

  ansi_parse (a, c, buf, red);

  return 0;
}



static int
ansi_update (ANSI * a, Context * c)
{
  int err = 0;

  err += ansi_history (a, c->h);
  err += ansi_draw (a, &c->v->crt);
  tty_length (a->terminal, c->v->crt.size.y);
  return err;
}

static void
ansi_free (ANSI * a)
{
  a->terminal_reset (a);
  if (a->terminal)
    a->terminal->close (a->terminal);

  free (a);
}

ANSI *
ansi_new_from_terminal (TTY * t, int utf8)
{
  ANSI *ret;

  ret = xmalloc (sizeof (ANSI));
  memset (ret, 0, sizeof (ANSI));

  ret->terminal = t;

  ret->utf8 = utf8;
  ret->update = ansi_update;
  ret->reset = ansi_reset;
  ret->terminal_reset = ansi_terminal_reset;
  ret->set_title = ansi_set_title;
  ret->close = ansi_free;
  ret->dispatch = ansi_dispatch;

  return ret;
}

/* 
 * ansi.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: ansi.h,v 1.20 2008/03/10 11:49:32 james Exp $
 */

/* 
 * $Log: ansi.h,v $
 * Revision 1.20  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/03/06 21:34:09  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/06 21:33:02  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/20 20:16:07  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/20 19:44:37  james
 * @@
 *
 * Revision 1.9  2008/02/20 19:36:06  james
 * @@
 *
 * Revision 1.8  2008/02/20 19:25:09  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/07 00:43:27  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */

#ifndef __ANSI_H__
#define __ANSI_H__

#define ANSI_INVAL -1

#define ANSI_ESCAPE_BUF_LEN 10
#define ANSI_ESCAPE_TIMEOUT	100000 /* in ms */

typedef struct {
  int in_escape;
  struct timeval last_escape;
  char escape_buf[ANSI_ESCAPE_BUF_LEN];
  int escape_ptr;
} ANSI_Parser;

struct CRT_struct;
struct Context_struct;

typedef struct ANSI_struct {
  ANSI_Parser parser;

  TTY *terminal;
  int eof;


  CRT crt;
  CRT_Pos pos;
  CRT_Pos size;
  int hide_cursor;
  int attr;
  int color;

  int utf8;

  int history_ptr;
  FILE *file;

  int (*dispatch) (struct ANSI_struct *, struct Context_struct *);
  int (*update) (struct ANSI_struct *, struct Context_struct *);
  int (*one_shot) (struct ANSI_struct *, struct CRT_struct *);
  int (*reset) (struct ANSI_struct *, struct CRT_struct *);
  int (*set_title) (struct ANSI_struct *, char *);
  void (*terminal_reset) (struct ANSI_struct *);
  void (*close) (struct ANSI_struct *);
} ANSI;


#endif /* __ANSI_H__ */

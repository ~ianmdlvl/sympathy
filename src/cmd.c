/* 
 * cmd.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: cmd.c,v 1.13 2008/03/07 13:16:02 james Exp $";

/* 
 * $Log: cmd.c,v $
 * Revision 1.13  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/02 10:50:32  staffcvs
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/29 22:50:29  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/28 22:00:42  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 16:57:51  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/28 15:37:06  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/15 15:14:19  james
 * *** empty log message ***
 *
 */

#include "project.h"


int
cmd_parse (Cmd * c, Context * ctx, ANSI * a, char *buf)
{
  if (!strcmp (buf, "quit"))
    c->disconnect++;
  else if (!strcmp (buf, "flow"))
    ctx->k->set_flow (ctx->k, ctx, 1);
  else if (!strcmp (buf, "noflow"))
    ctx->k->set_flow (ctx->k, ctx, 0);
  else if (!strcmp (buf, "ansi"))
    ctx->k->set_ansi (ctx->k, ctx, 0);
  else if (!strcmp (buf, "noansi"))
    ctx->k->set_ansi (ctx->k, ctx, 1);
  else if (!strncmp (buf, "baud", 4))
    ctx->k->set_baud (ctx->k, ctx, atoi (buf + 4));
  else if (!strcmp (buf, "break"))
    ctx->k->send_break (ctx->k, ctx);
  else if (!strcmp (buf, "hangup"))
    ctx->k->hangup (ctx->k, ctx);
  else if (!strcmp (buf, "reset"))
    ctx->k->reset (ctx->k, ctx);
  else if (!strcmp (buf, "expand")) {
    int w = a->terminal->size.x;
    int h = a->terminal->size.y - 1;
    ctx->k->set_size (ctx->k, ctx, w, h);
  } else if (!strncmp (buf, "width", 5))
    ctx->k->set_size (ctx->k, ctx, atoi (buf + 5), 0);
  else if (!strncmp (buf, "height", 6))
    ctx->k->set_size (ctx->k, ctx, 0, atoi (buf + 6));
  else
    return -1;

  return 0;


}

void
cmd_show_status (Cmd * c, Context * ctx)
{
  if (!ctx->v)
    return;

  if (c->error)
    vt102_status_line (ctx->v, "Command not recognized - press any key");
  else if (!c->active)
    vt102_status_line (ctx->v, c->csl);
  else
    vt102_status_line (ctx->v, c->buf);

}

int
cmd_key (Cmd * c, Context * ctx, ANSI * a, int key)
{

  if (c->error) {
    c->error = 0;
    c->active = 0;
    cmd_show_status (c, ctx);
    return 0;
  }

  if (key == 13) {
    if (cmd_parse (c, ctx, a, c->buf + 1)) {
      c->error++;
    } else {
      c->active = 0;
    }
    cmd_show_status (c, ctx);
    return 0;
  }

  if (((key == 8) || (key == 127)) && (c->ptr > 1)) {
    c->ptr--;
    c->buf[c->ptr] = 0;
  }

  if ((key >= 32) && (key < 127)) {

    c->buf[c->ptr] = key;
    c->ptr++;
    c->buf[c->ptr] = 0;

  }

  cmd_show_status (c, ctx);

  return 0;
}


int
cmd_deactivate (Cmd * c, Context * ctx)
{
  c->active = 0;
  cmd_show_status (c, ctx);
  return 0;
}

int
cmd_activate (Cmd * c, Context * ctx)
{
  c->active = 1;
  c->ptr = 1;
  c->buf[0] = ':';
  c->buf[1] = 0;

  cmd_show_status (c, ctx);

  return 0;
}

void
cmd_new_status (Cmd * c, Context * ctx, char *msg)
{
  strcpy (c->csl, msg);
  cmd_show_status (c, ctx);
}



Cmd *
cmd_new (void)
{
  Cmd *ret;

  ret = (Cmd *) xmalloc (sizeof (Cmd));

  ret->disconnect = 0;
  ret->active = 0;
  ret->error = 0;
  ret->csl[0] = 0;
}

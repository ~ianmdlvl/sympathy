/* 
 * cmd.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: cmd.h,v 1.8 2008/03/10 11:49:32 james Exp $
 */

/* 
 * $Log: cmd.h,v $
 * Revision 1.8  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/02 12:30:54  staffcvs
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/15 15:14:19  james
 * *** empty log message ***
 *
 */

#ifndef __CMD_H__
#define __CMD_H__

#define CMD_KEY 2               /* CTRL B */
#define CMD_CANCEL_KEY 3        /* CTRL C */

typedef struct {
  int active;
  int error;
  int disconnect;
  char csl[128];
  char buf[128];
  int ptr;
} Cmd;

#endif /* __CMD_H__ */

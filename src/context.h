/* 
 * context.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: context.h,v 1.14 2010/07/27 14:49:35 james Exp $
 */

/* 
 * $Log: context.h,v $
 * Revision 1.14  2010/07/27 14:49:35  james
 * add support for byte logging
 *
 * Revision 1.13  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/09 15:47:28  james
 * *** empty log message ***
 *
 */

#ifndef __CONTEXT_H__
#define __CONTEXT_H__

typedef struct Context_struct {
  VT102 *v;
  TTY *t;
  TTY_Parser *tp;
  History *h;
  Log *l;
  KeyDis *k;
  Cmd *d;
  UTF8 *u;
  RX *r;
  int byte_logging;
} Context;

#endif /* __CONTEXT_H__ */

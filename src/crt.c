/* 
 * crt.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: crt.c,v 1.18 2008/03/07 12:37:04 james Exp $";

/* 
 * $Log: crt.c,v $
 * Revision 1.18  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/27 09:42:21  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/27 00:27:21  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/26 23:56:12  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/07 13:22:51  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/07 13:19:48  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/07 12:41:06  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/05 01:11:46  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */

#include "project.h"

void
crt_erase (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color)
{
  CRT_CA *ps = &c->screen[CRT_ADDR_POS (&s)];
  CRT_CA *pe = &c->screen[CRT_ADDR_POS (&e)];

  while (ps <= pe) {
    ps->chr = ' ';
    if (ea) {
      ps->attr = CRT_ATTR_NORMAL;
      ps->color = color;
    }
    ps++;
  }

}

void
crt_cls (CRT * c)
{
  CRT_Pos s = { 0, 0 };
  CRT_Pos e = { CRT_COLS - 1, CRT_ROWS - 1 };
  int i;

  crt_erase (c, s, e, 1, CRT_COLOR_NORMAL);
#if 0
  c->sh.dir = 0;
#endif
}

void
crt_scroll_up (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color)
{
  int l, n;
  int p;


  s.x = 0;
  e.x = CRT_COLS - 1;

#if 0
  c->sh.s = s;
  c->sh.e = e;
  c->sh.dir = -1;
#endif

  l = e.x - s.x;
  l++;
  l *= sizeof (CRT_CA);

  n = e.y - s.y;


  p = CRT_ADDR_POS (&s);

  while (n--) {
    memcpy (&c->screen[p], &c->screen[p + CRT_COLS], l);
    p += CRT_COLS;
  }

  s.y = e.y;
  crt_erase (c, s, e, ea, color);

}

void
crt_scroll_down (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color)
{
  int l, n;
  int p;

  s.x = 0;
  e.x = CRT_COLS - 1;

#if 0
  c->sh.s = s;
  c->sh.e = e;
  c->sh.dir = 1;
#endif

  l = e.x - s.x;
  l++;
  l *= sizeof (CRT_CA);

  n = e.y - s.y;
  // n++;

  p = CRT_ADDR_POS (&e);

  while (n--) {
    p -= CRT_COLS;
    memcpy (&c->screen[p], &c->screen[p - CRT_COLS], l);
  }

  e.y = s.y;
  crt_erase (c, s, e, ea, color);

}

void
crt_reset (CRT * c)
{
  crt_cls (c);

  c->pos.x = 0;
  c->pos.y = 0;
  c->hide_cursor = 1;
  c->size.x = CRT_COLS;
  c->size.y = CRT_ROWS;
#if 0
  c->sh.dir = 0;
#endif
}

void
crt_insert (CRT * c, CRT_CA ca)
{
  if (c->pos.x < 0)
    c->pos.x = 0;
  if (c->pos.x >= CRT_COLS)
    c->pos.x = CRT_COLS - 1;
  if (c->pos.y < 0)
    c->pos.y = 0;
  if (c->pos.y >= CRT_ROWS)
    c->pos.y = CRT_ROWS - 1;

  c->screen[CRT_ADDR (c->pos.y, c->pos.x)] = ca;

#if 0
  c->sh.dir = 0;
#endif
}

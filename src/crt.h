/* 
 * crt.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: crt.h,v 1.18 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: crt.h,v $
 * Revision 1.18  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/28 16:57:51  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/26 19:08:27  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/20 19:25:09  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/07 13:22:51  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/07 12:41:06  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */

#ifndef __CRT_H__
#define __CRT_H__

#define CRT_ROWS 60
#define CRT_COLS 132

#define CRT_CELS (CRT_ROWS*CRT_COLS)
#define CRT_ADDR(r,c) (((r)*CRT_COLS)+(c))
#define CRT_ADDR_POS(p) ((((p)->y)*CRT_COLS)+((p)->x))

#define CRT_ATTR_NORMAL    0x0
#define CRT_ATTR_UNDERLINE 0x1
#define CRT_ATTR_REVERSE   0x2
#define CRT_ATTR_BLINK	   0x4
#define CRT_ATTR_BOLD	   0x8


#define CRT_COLOR_BLACK		0x0
#define CRT_COLOR_RED		0x1
#define CRT_COLOR_GREEN		0x2
#define CRT_COLOR_YELLOW	0x3
#define CRT_COLOR_BLUE		0x4
#define CRT_COLOR_MAGENTA	0x5
#define CRT_COLOR_CYAN		0x6
#define CRT_COLOR_WHITE		0x7
#define CRT_COLOR_INTENSITY	0x8

#define CRT_COLOR_FG_MASK	0xf0
#define CRT_COLOR_FG_SHIFT	4

#define CRT_COLOR_BG_MASK	0xf
#define CRT_COLOR_BG_SHIFT	0

#define CRT_COLOR_BG(a)		(((a) & CRT_COLOR_BG_MASK) >> CRT_COLOR_BG_SHIFT)
#define CRT_COLOR_FG(a)		(((a) & CRT_COLOR_FG_MASK) >> CRT_COLOR_FG_SHIFT)

#define CRT_MAKE_COLOR(f,b)	(((f) << CRT_COLOR_FG_SHIFT)|(b))

#define CRT_BGCOLOR_NORMAL	CRT_COLOR_BLACK
#define CRT_FGCOLOR_NORMAL	CRT_COLOR_WHITE

#define CRT_COLOR_NORMAL	CRT_MAKE_COLOR(CRT_FGCOLOR_NORMAL,CRT_BGCOLOR_NORMAL)

typedef struct __attribute__ ((packed)) {
  uint32_t chr;
  uint8_t attr;
  uint8_t color;
} CRT_CA;

typedef struct {
  int x;
  int y;
} CRT_Pos;


typedef struct {
  CRT_Pos s;
  CRT_Pos e;
  int dir;
} CRT_ScrollHint;

typedef struct CRT_struct {
  CRT_CA screen[CRT_CELS];
  CRT_Pos pos;
  int hide_cursor;
  CRT_Pos size;
} CRT;


static inline
crt_ca_cmp (CRT_CA a, CRT_CA b)
{
  return memcmp (&a, &b, sizeof (a));
}

static inline
crt_pos_cmp (CRT_Pos a, CRT_Pos b)
{
  return memcmp (&a, &b, sizeof (a));
}

#endif /* __CRT_H__ */

/* 
 * history.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: history.c,v 1.7 2008/03/07 13:16:02 james Exp $";

/* 
 * $Log: history.c,v $
 * Revision 1.7  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#include "project.h"

History *
history_new (int n)
{
  History *ret;

  ret = (History *) xmalloc (sizeof (History));
  ret->lines = xmalloc (n * sizeof (History_ent));
  memset (ret->lines, 0, n * sizeof (History_ent));

  ret->wptr = 0;
  ret->nlines = n;

  return ret;
}

void
history_free (History * h)
{
  if (!h)
    return;
  if (h->lines)
    free (h->lines);
  free (h);
}


void
history_add (History * h, CRT_CA * c)
{
  History_ent *e;
  if (!h)
    return;

  e = &h->lines[h->wptr];
  HISTORY_INC (h, h->wptr);

  memcpy (e->line, c, sizeof (CRT_CA) * CRT_COLS);
  time (&e->t);
  e->valid = 1;

#if 0
  {
    int i = CRT_COLS;
    while (i--) {
      fputc (c->chr, stderr);
      c++;
    }
    fputc ('\n', stderr);
  }
#endif


}

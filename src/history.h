/* 
 * history.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: history.h,v 1.7 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: history.h,v $
 * Revision 1.7  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#ifndef __HISTORY_H__
#define __HISTORY_H__

typedef struct {
  int valid;
  time_t t;
  CRT_CA line[CRT_COLS];
} History_ent;

typedef struct {
  History_ent *lines;
  int nlines;
  int wptr;
} History;

#define HISTORY_INC(h,a) do { (a)++; if ((a)==((h)->nlines)) { (a)=0; }; } while (0)

#endif /* __HISTORY_H__ */

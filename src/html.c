/* 
 * html.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: html.c,v 1.15 2008/03/07 13:16:02 james Exp $";

/* 
 * $Log: html.c,v $
 * Revision 1.15  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/27 01:52:38  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/27 01:52:08  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/20 23:42:05  staffcvs
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/20 23:31:48  staffcvs
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/20 22:54:22  staffcvs
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/20 20:16:07  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */

#include "project.h"


#define V(i) (((i)==0)?0x80:(((i)==1)?0xc0:0xff))
#define COLOR(r,g,b,i) ((((r)?(V(i)):0) << 0)| (((g)?(V(i)):0) << 8)| (((b)?(V(i)):0) << 16))

static int colormap[] = {
  [CRT_COLOR_BLACK] = COLOR (0, 0, 0, 0),
  [CRT_COLOR_RED] = COLOR (0, 0, 1, 0),
  [CRT_COLOR_GREEN] = COLOR (0, 1, 0, 0),
  [CRT_COLOR_YELLOW] = COLOR (0, 1, 1, 0),
  [CRT_COLOR_BLUE] = COLOR (1, 0, 0, 0),
  [CRT_COLOR_MAGENTA] = COLOR (1, 0, 1, 0),
  [CRT_COLOR_CYAN] = COLOR (1, 1, 0, 0),
  [CRT_COLOR_WHITE] = COLOR (1, 1, 1, 1),
  [CRT_COLOR_BLACK | CRT_COLOR_INTENSITY] = COLOR (1, 1, 1, 0),
  [CRT_COLOR_RED | CRT_COLOR_INTENSITY] = COLOR (0, 0, 1, 2),
  [CRT_COLOR_GREEN | CRT_COLOR_INTENSITY] = COLOR (0, 1, 0, 2),
  [CRT_COLOR_YELLOW | CRT_COLOR_INTENSITY] = COLOR (0, 1, 1, 2),
  [CRT_COLOR_BLUE | CRT_COLOR_INTENSITY] = COLOR (1, 0, 0, 2),
  [CRT_COLOR_MAGENTA | CRT_COLOR_INTENSITY] = COLOR (1, 0, 1, 2),
  [CRT_COLOR_CYAN | CRT_COLOR_INTENSITY] = COLOR (1, 1, 0, 2),
  [CRT_COLOR_WHITE | CRT_COLOR_INTENSITY] = COLOR (1, 1, 1, 2),
};


static void
html_entity (FILE * f, int c)
{
  switch (c) {
  case 32:
    fprintf (f, "&nbsp;");
    break;
  case 38:
    fprintf (f, "&amp;");
    break;
  case 60:
    fprintf (f, "&lt;");
    break;
  case 62:
    fprintf (f, "&gt;");
    break;
  default:

    if ((c >= 32) && (c < 127)) {
      fputc (c, f);
    } else if (c > 127) {
      fprintf (f, "&#x%04x;", c);
    } else {
      fputc (' ', f);
    }
  }
}

static void
html_render (FILE * f, CRT_CA c)
{
  int fg, bg;

  if (c.attr & CRT_ATTR_REVERSE) {
    fg = CRT_COLOR_BG (c.color);
    bg = CRT_COLOR_FG (c.color);
  } else {
    fg = CRT_COLOR_FG (c.color);
    bg = CRT_COLOR_BG (c.color);
    if (c.attr & CRT_ATTR_BOLD)
      fg |= CRT_COLOR_INTENSITY;
  }
#ifdef CSS
  fprintf (f, "<span style='color: #%06x; background-color: #%06x'>",
           colormap[fg], colormap[bg]);
#else
  fprintf (f, "<td bgcolor='#%06x'><font color='#%06x'>", colormap[bg],
           colormap[fg]);
  fprintf (f, "<tt>");
#endif

  if (c.attr & CRT_ATTR_UNDERLINE)
    fprintf (f, "<ul>");
  if (c.attr & CRT_ATTR_BOLD)
    fprintf (f, "<b>");

  html_entity (f, c.chr);

  if (c.attr & CRT_ATTR_BOLD)
    fprintf (f, "</b>");
  if (c.attr & CRT_ATTR_UNDERLINE)
    fprintf (f, "</ul>");
  if (c.attr & CRT_ATTR_REVERSE) {
    fprintf (f, "</font>");
  }
#ifdef CSS
  fprintf (f, "</span>");
#else
  fprintf (f, "</tt>");
  fprintf (f, "</td>");
#endif
}

static void
html_draw (FILE * f, CRT * c)
{
  CRT_Pos p;
  int o;

#ifdef CSS
  fprintf (f, "<pre>");
#else
  fprintf (f, "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n");
#endif
  for (p.y = 0; p.y < c->size.y; ++p.y) {
    o = CRT_ADDR (p.y, 0);
#ifndef CSS
    fprintf (f, "<tr>");
#endif
    for (p.x = 0; p.x < c->size.x; ++p.x, ++o) {
      html_render (f, c->screen[o]);
    }
#ifdef CSS
    fprintf (f, "\n");
#else
    fprintf (f, "</tr>\n");
#endif
  }
#ifdef CSS
  fprintf (f, "</pre>\n");
#else
  fprintf (f, "</table>");
#endif
}


static int
html_one_shot (ANSI * a, CRT * c)
{
  html_draw (a->file, c);
  return 0;
}


static void
ansi_free (ANSI * a)
{
  free (a);
}

ANSI *
ansi_new_html (FILE * f)
{
  ANSI *ret;

  ret = xmalloc (sizeof (ANSI));
  memset (ret, 0, sizeof (ANSI));

  ret->file = f;
  ret->close = ansi_free;
  ret->one_shot = html_one_shot;

  return ret;
}

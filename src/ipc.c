/* 
 * ipc.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: ipc.c,v 1.11 2008/03/07 14:13:40 james Exp $";

/* 
 * $Log: ipc.c,v $
 * Revision 1.11  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 18:16:16  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/03 18:15:19  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 16:57:51  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 12:17:42  james
 * *** empty log message ***
 *
 */

#include "project.h"

IPC_Msg *
ipc_check_for_message_in_slide (Slide * s)
{
  IPC_Msg *m;
  if (SLIDE_BYTES (s) < sizeof (IPC_Msg_hdr))
    return NULL;
  m = (IPC_Msg *) SLIDE_RPTR (s);
  if (SLIDE_BYTES (s) < m->hdr.size)
    return NULL;
  if (m->hdr.size < sizeof (IPC_Msg_hdr))
    crash_out ("ipc_check_for_message_in_slide test failed");

  return m;
}

void
ipc_consume_message_in_slide (Slide * s)
{
  IPC_Msg *m = ipc_check_for_message_in_slide (s);
  if (!m)
    crash_out ("ipc_consume_message_in_slide test failed");

  slide_consume (s, m->hdr.size);
}


int
ipc_msg_send (Socket * s, IPC_Msg * m)
{
  int len = m->hdr.size;
  return (socket_write (s, m, len) == len) ? 0 : -1;
}


int
ipc_msg_send_debug (Socket * s, char *msg)
{
  char buf[sizeof (IPC_Msg_hdr) + IPC_MAX_BUF];
  IPC_Msg_debug *m;
  int len;


  m = (IPC_Msg_debug *) buf;
  m->type = IPC_MSG_TYPE_DEBUG;
  strncpy (m->msg, msg, IPC_MAX_BUF);
  m->msg[IPC_MAX_BUF - 1] = 0;

  m->size = sizeof (IPC_Msg_hdr) + strlen (m->msg) + 1;


  return ipc_msg_send (s, (IPC_Msg *) m);
}

int
ipc_msg_send_initialize (Socket * s)
{
  IPC_Msg_initialize m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_INITIALIZE;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_history (Socket * s, History_ent * l)
{
  IPC_Msg_history m;
  int len;


  m.type = IPC_MSG_TYPE_HISTORY;
  m.history = *l;
  m.size = sizeof (m);

  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_vt102 (Socket * s, VT102 * v)
{
  IPC_Msg_VT102 m;
  int len;


  m.type = IPC_MSG_TYPE_VT102;
  m.len = sizeof (VT102);
  m.vt102 = *v;
  m.size = sizeof (m);

  return ipc_msg_send (s, (IPC_Msg *) & m);
}


int
ipc_msg_send_key (Socket * s, int key)
{
  IPC_Msg_key m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_KEY;
  m.key = key;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}



int
ipc_msg_send_term (Socket * s, void *buf, int len)
{
  char mbuf[IPC_MAX_BUF + sizeof (IPC_Msg_hdr)];

  IPC_Msg_term *m = (IPC_Msg_term *) mbuf;

  if (!len)
    return 0;

  m->size = len + sizeof (IPC_Msg_hdr);
  m->type = IPC_MSG_TYPE_TERM;
  m->len = len;
  memcpy (m->term, buf, len);

  return ipc_msg_send (s, (IPC_Msg *) & m);
}


int
ipc_msg_send_status (Socket * s, char *buf)
{
  char mbuf[IPC_MAX_BUF + sizeof (IPC_Msg_hdr)];
  IPC_Msg_status *m = (IPC_Msg_status *) mbuf;
  int len;

  if (!buf)
    return 0;
  len = strlen (buf) + 1;

  m->size = len + sizeof (IPC_Msg_hdr);
  m->type = IPC_MSG_TYPE_STATUS;
  strncpy (m->status, buf, IPC_MAX_BUF - 1);
  m->status[IPC_MAX_BUF - 1] = 0;

  return ipc_msg_send (s, (IPC_Msg *) & m);
}


int
ipc_msg_send_setbaud (Socket * s, int baud)
{
  IPC_Msg_setbaud m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_SETBAUD;
  m.baud = baud;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}


int
ipc_msg_send_sendbreak (Socket * s)
{
  IPC_Msg_sendbreak m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_SENDBREAK;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}


int
ipc_msg_send_setflow (Socket * s, int flow)
{
  IPC_Msg_setflow m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_SETFLOW;
  m.flow = flow;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_setansi (Socket * s, int ansi)
{
  IPC_Msg_setansi m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_SETANSI;
  m.ansi = ansi;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_hangup (Socket * s)
{
  IPC_Msg_hangup m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_HANGUP;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_setsize (Socket * s, CRT_Pos size)
{
  IPC_Msg_setsize m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_SETSIZE;
  m.winsize = size;

  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_reset (Socket * s)
{
  IPC_Msg_reset m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_RESET;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}

int
ipc_msg_send_killme (Socket * s)
{
  IPC_Msg_killme m;

  m.size = sizeof (m);
  m.type = IPC_MSG_TYPE_KILLME;
  return ipc_msg_send (s, (IPC_Msg *) & m);
}

/* 
 * ipc.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: ipc.h,v 1.12 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: ipc.h,v $
 * Revision 1.12  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 12:17:42  james
 * *** empty log message ***
 *
 */

#ifndef __IPC_H__
#define __IPC_H__

#define IPC_MAX_BUF 1024

#define IPC_MSG_TYPE_NOOP  0
#define IPC_MSG_TYPE_DEBUG 1
#define IPC_MSG_TYPE_INITIALIZE 2
#define IPC_MSG_TYPE_VT102 3
#define IPC_MSG_TYPE_HISTORY 4
#define IPC_MSG_TYPE_KEY 5
#define IPC_MSG_TYPE_TERM 6
#define IPC_MSG_TYPE_STATUS 7
#define IPC_MSG_TYPE_SETBAUD 8
#define IPC_MSG_TYPE_SENDBREAK 9
#define IPC_MSG_TYPE_SETFLOW 10
#define IPC_MSG_TYPE_SETANSI 11
#define IPC_MSG_TYPE_HANGUP 12
#define IPC_MSG_TYPE_SETSIZE 13
#define IPC_MSG_TYPE_RESET 14
#define IPC_MSG_TYPE_KILLME 15

typedef struct {
  int32_t size;
  int32_t type;
  uint8_t payload[0];
} IPC_Msg_hdr;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_noop;


typedef struct {
  int32_t size;
  int32_t type;
  char msg[0];
} IPC_Msg_debug;

typedef struct {
  int32_t size;
  int32_t type;
  char msg[0];
} IPC_Msg_initialize;

typedef struct {
  int32_t size;
  int32_t type;
  History_ent history;
} IPC_Msg_history;

typedef struct {
  int32_t size;
  int32_t type;
  int32_t len;
  VT102 vt102;
} IPC_Msg_VT102;


typedef struct {
  int32_t size;
  int32_t type;
  int32_t key;
} IPC_Msg_key;

typedef struct {
  int32_t size;
  int32_t type;
  int32_t len;
  uint8_t term[0];
} IPC_Msg_term;


typedef struct {
  int32_t size;
  int32_t type;
  char status[0];
} IPC_Msg_status;

typedef struct {
  int32_t size;
  int32_t type;
  int32_t baud;
} IPC_Msg_setbaud;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_sendbreak;


typedef struct {
  int32_t size;
  int32_t type;
  int32_t flow;
} IPC_Msg_setflow;


typedef struct {
  int32_t size;
  int32_t type;
  int32_t ansi;
} IPC_Msg_setansi;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_hangup;


typedef struct {
  int32_t size;
  int32_t type;
  CRT_Pos winsize;
} IPC_Msg_setsize;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_reset;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_killme;



typedef union {
  IPC_Msg_hdr hdr;
  IPC_Msg_noop noop;
  IPC_Msg_debug debug;
  IPC_Msg_initialize initialize;
  IPC_Msg_history history;
  IPC_Msg_VT102 vt102;
  IPC_Msg_key key;
  IPC_Msg_term term;
  IPC_Msg_status status;
  IPC_Msg_setbaud setbaud;
  IPC_Msg_sendbreak sendbreak;
  IPC_Msg_setflow setflow;
  IPC_Msg_setansi setansi;
  IPC_Msg_hangup hangup;
  IPC_Msg_setsize setsize;
  IPC_Msg_reset reset;
  IPC_Msg_killme killme;
} IPC_Msg;



#endif /* __IPC_H__ */

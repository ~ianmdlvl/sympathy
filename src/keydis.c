/* 
 * keydis.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: keydis.c,v 1.16 2008/03/10 11:49:33 james Exp $";

/* 
 * $Log: keydis.c,v $
 * Revision 1.16  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/03/03 18:16:16  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/03/03 18:15:19  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/28 22:00:42  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/28 16:37:16  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 12:12:25  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 */


#include "project.h"

#define CMD_BUFLEN	128

typedef struct {
  KEYDIS_SIGNATURE;
} KeyDis_VT102;

typedef struct {
  KEYDIS_SIGNATURE;
  Socket *s;
} KeyDis_IPC;


static void
keydis_close (KeyDis * t)
{
  free (t);
}


static int
keydis_ipc_key (KeyDis * _t, Context * c, int key)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  return ipc_msg_send_key (t->s, key);
}

static int
keydis_ipc_set_baud (KeyDis * _t, Context * c, int baud)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  ipc_msg_send_setbaud (t->s, baud);

  return 0;
}

static int
keydis_ipc_send_break (KeyDis * _t, Context * c)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  ipc_msg_send_sendbreak (t->s);

  return 0;
}

static int
keydis_ipc_set_flow (KeyDis * _t, Context * c, int flow)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  ipc_msg_send_setflow (t->s, flow);

  return 0;
}


static int
keydis_ipc_set_ansi (KeyDis * _t, Context * c, int ansi)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  vt102_set_ansi (c->v, ansi);

  ipc_msg_send_setansi (t->s, ansi);

  return 0;
}


static int
keydis_ipc_hangup (KeyDis * _t, Context * c)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  ipc_msg_send_hangup (t->s);

  return 0;
}


static int
keydis_ipc_set_size (KeyDis * _t, Context * c, int w, int h)
{
  CRT_Pos p = { w, h };

  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  vt102_resize (c, p);
  ipc_msg_send_setsize (t->s, p);

  return 0;
}


static int
keydis_ipc_reset (KeyDis * _t, Context * c)
{
  KeyDis_IPC *t = (KeyDis_IPC *) _t;

  vt102_reset (c);
  ipc_msg_send_reset (t->s);

  return 0;
}

static int
keydis_vt102_key (KeyDis * _t, Context * c, int key)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  vt102_send (c, key);
  return 0;
}

static int
keydis_vt102_set_baud (KeyDis * _t, Context * c, int baud)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  tty_set_baud (c->t, baud);
  tty_parse_reset (c);

  log_f (c->l, "<baud changed to %d>", baud);

  return 0;
}

static int
keydis_vt102_send_break (KeyDis * _t, Context * c)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  log_f (c->l, "<break sent>");
  tty_send_break (c->t);

  return 0;
}

static int
keydis_vt102_set_flow (KeyDis * _t, Context * c, int flow)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  log_f (c->l, "<flow control turned %s>", flow ? "on" : "off");
  tty_set_flow (c->t, flow);

  return 0;
}

static int
keydis_vt102_set_ansi (KeyDis * _t, Context * c, int ansi)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  if (c->v)
    c->v->xn_glitch = ansi ? 0 : 1;
  return 0;
}


static int
keydis_vt102_hangup (KeyDis * _t, Context * c)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  log_f (c->l, "<hangup initiated>");
  tty_hangup (c->t);

  return 0;
}

static int
keydis_vt102_set_size (KeyDis * _t, Context * c, int w, int h)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;
  CRT_Pos p = { w, h };

  vt102_resize (c, p);

  return 0;
}

static int
keydis_vt102_reset (KeyDis * _t, Context * c)
{
  KeyDis_VT102 *t = (KeyDis_VT102 *) _t;

  vt102_reset (c);

  return 0;
}



KeyDis *
keydis_vt102_new (void)
{
  KeyDis_VT102 *t = xmalloc (sizeof (KeyDis_VT102));
  t->key = keydis_vt102_key;
  t->close = keydis_close;
  t->set_baud = keydis_vt102_set_baud;
  t->send_break = keydis_vt102_send_break;
  t->set_flow = keydis_vt102_set_flow;
  t->set_ansi = keydis_vt102_set_ansi;
  t->hangup = keydis_vt102_hangup;
  t->set_size = keydis_vt102_set_size;
  t->reset = keydis_vt102_reset;
  return (KeyDis *) t;
}


KeyDis *
keydis_ipc_new (Socket * s)
{
  KeyDis_IPC *t = xmalloc (sizeof (KeyDis_IPC));
  t->key = keydis_ipc_key;
  t->close = keydis_close;
  t->set_baud = keydis_ipc_set_baud;
  t->send_break = keydis_ipc_send_break;
  t->set_flow = keydis_ipc_set_flow;
  t->set_ansi = keydis_ipc_set_ansi;
  t->hangup = keydis_ipc_hangup;
  t->set_size = keydis_ipc_set_size;
  t->reset = keydis_ipc_reset;
  t->s = s;
  return (KeyDis *) t;
}






#if 0
int
keydis_key (KeyDis * t, Context * c, int key)
{

  if (!c->d)
    return t->key (t, c, key);

  cmd_show_status (c->d, c);

  if (c->d->active)
    return cmd_key (c->d, c, key);

  if (key == CMD_KEY)
    return cmd_activate (c->d, c);


  return t->key (t, c, key);
}
#endif

/* 
 * keydis.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: keydis.h,v 1.10 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: keydis.h,v $
 * Revision 1.10  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 */

#ifndef __KEYDIS_H__
#define __KEYDIS_H__


struct Context_struct;

#define KEYDIS_SIGNATURE \
	void (*close)(struct KeyDis_struct *); \
	int (*key)(struct KeyDis_struct *,struct Context_struct *,int key); \
	int (*set_baud)(struct KeyDis_struct *,struct Context_struct *,int rate); \
	int (*send_break)(struct KeyDis_struct *,struct Context_struct *); \
	int (*set_flow)(struct KeyDis_struct *,struct Context_struct *,int flow); \
	int (*set_ansi)(struct KeyDis_struct *,struct Context_struct *,int ansi); \
	int (*hangup)(struct KeyDis_struct *,struct Context_struct *); \
	int (*reset)(struct KeyDis_struct *,struct Context_struct *); \
	int (*set_size)(struct KeyDis_struct *,struct Context_struct *,int width, int height)



typedef struct KeyDis_struct {
  KEYDIS_SIGNATURE;
} KeyDis;


#endif /* __KEYDIS_H__ */

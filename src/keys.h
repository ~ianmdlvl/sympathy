/* 
 * src/keys.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: keys.h,v 1.10 2008/03/07 14:16:44 james Exp $
 */

/* 
 * $Log: keys.h,v $
 * Revision 1.10  2008/03/07 14:16:44  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/06 01:49:19  james
 * *** empty log message ***
 *
 * Revision 1.5  2008 /03 /06 01:41:48  james
 * *** empty log message ***
 *
 * Revision 1.4  2008 /02 /07 00:44:07  james
 * *** empty log message ***
 *
 */

#ifndef __KEYS_H__
#define __KEYS_H__

#define KEY_UP		128             /* A */
#define KEY_DOWN	129            /* B */
#define KEY_RIGHT	130           /* C */
#define KEY_LEFT	131            /* D */
#define KEY_MIDDLE	132          /* E */
#define KEY_END		133            /* F */
#define KEY_134		134            /* G */
#define KEY_HOME	135            /* H */
#define KEY_136		136            /* I */
#define KEY_137		137            /* J */
#define KEY_138		138            /* K */
#define KEY_139		139            /* L */
#define KEY_ENTER	140           /* M */
#define KEY_141		141            /* N */
#define KEY_142		142            /* O */
#define KEY_PF1		143	/* P */    /* Also F1 */
#define KEY_PF2		144	/* Q */    /* Also F2 */
#define KEY_PF3		145	/* R */    /* Also F3 */
#define KEY_PF4		146	/* S */    /* Also F4 */
#define KEY_147		147            /* T */
#define KEY_148		148            /* U */
#define KEY_149		149            /* V */
#define KEY_150		150            /* W */
#define KEY_151		151            /* X */
#define KEY_152		152            /* Y */
#define KEY_153		153            /* Z */
#define KEY_154		154            /* a */
#define KEY_155		155            /* b */
#define KEY_156		156            /* c */
#define KEY_157		157            /* d */
#define KEY_158		158            /* e */
#define KEY_159		159            /* f */
#define KEY_160		160            /* g */
#define KEY_161		161            /* h */
#define KEY_162		162            /* i */
#define KEY_STAR	163            /* j */
#define KEY_PLUS	164            /* k */
#define KEY_COMMA	165           /* l */
#define KEY_MINUS	166           /* m */
#define KEY_PERIOD	167          /* n */
#define KEY_DIVIDE	168          /* o */
#define KEY_0		169              /* p */
#define KEY_1		170              /* q */
#define KEY_2		171              /* r */
#define KEY_3		172              /* s */
#define KEY_4		173              /* t */
#define KEY_5		174              /* u */
#define KEY_6		175              /* v */
#define KEY_7		176              /* w */
#define KEY_8		177              /* x */
#define KEY_9		178              /* y */
#define KEY_179		179            /* z */
#define KEY_180		180            /* 0 */
#define KEY_VT220_HOME	181      /* 1 */
#define KEY_INSERT	182          /* 2 */
#define KEY_DELETE	183          /* 3 */
#define KEY_VT220_END	184       /* 4 */
#define KEY_PGUP	185            /* 5 */
#define KEY_PGDN	186            /* 6 */
#define KEY_187		187            /* 7 */
#define KEY_188		188            /* 8 */
#define KEY_189		189            /* 9 */
#define KEY_190		190            /* 10 */
#define KEY_F1		191             /* 11 */
#define KEY_F2		192             /* 12 */
#define KEY_F3		193             /* 13 */
#define KEY_F4		194             /* 14 */
#define KEY_F5		195             /* 15 */
#define KEY_196		196            /* 16 */
#define KEY_F6		197             /* 17 */
#define KEY_F7		198             /* 18 */
#define KEY_F8		199             /* 19 */
#define KEY_F9		200             /* 20 */
#define KEY_F10		201            /* 21 */
#define KEY_202		202            /* 22 */
#define KEY_F11		203            /* 23 */
#define KEY_F12 	204            /* 24 */
#define KEY_F13		205            /* 25 */
#define KEY_F14		206            /* 26 */
#define KEY_207		207            /* 27 */
#define KEY_F15		208            /* 28 */
#define KEY_F16		209            /* 29 */
#define KEY_210		210            /* 30 */
#define KEY_F17		211            /* 31 */
#define KEY_F18		212            /* 32 */
#define KEY_F19		213            /* 33 */
#define KEY_F20		214            /* 34 */
#define KEY_NUM		215
#endif /* __KEYS_H__ */

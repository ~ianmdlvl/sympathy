/* 
 * lockfile.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: lockfile.h,v 1.11 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: lockfile.h,v $
 * Revision 1.11  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/15 20:52:36  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/15 19:51:30  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 18:16:36  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 16:48:56  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/15 15:09:17  james
 * *** empty log message ***
 *
 */

#ifndef __LOCKFILE_H__
#define __LOCKFILE_H__

#define SERIAL_LOCK_PASSIVE 0
#define SERIAL_LOCK_ACTIVE  1

#define FILE_LIST_MAX_LEN 1024

typedef struct Filelist_ent {
  char name[FILE_LIST_MAX_LEN];
  struct Filelist_ent *next;
} Filelist_ent;

typedef struct {
  Filelist_ent *head;
} Filelist;


typedef struct {
  int mode;
  int i;
  struct timeval last_stale_purge;
  Filelist *locks_to_check;
  Filelist *locks_held;
} Serial_lock;



#endif /* __LOCKFILE_H__ */

/* 
 * log.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: log.h,v 1.7 2010/07/27 14:49:35 james Exp $
 */

/* 
 * $Log: log.h,v $
 * Revision 1.7  2010/07/27 14:49:35  james
 * add support for byte logging
 *
 * Revision 1.6  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 12:14:50  james
 * *** empty log message ***
 *
 */

#ifndef __LOG_H__
#define __LOG_H__

#define LOG_SIGNATURE \
	struct Log_struct *next; \
	void (*log)(struct Log_struct *,char *); \
	void (*log_bytes)(struct Log_struct *,void *,int); \
	void (*sighup)(struct Log_struct *); \
	void (*close)(struct Log_struct *)

typedef struct Log_struct {
  LOG_SIGNATURE;
} Log;


#endif /* __LOG_H__ */

/* 
 * project.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: project.h,v 1.11 2008/03/07 12:37:04 james Exp $
 */

/* 
 * $Log: project.h,v $
 * Revision 1.11  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/22 14:51:54  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/13 18:05:06  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/09 15:47:28  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/06 20:26:58  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/04 05:45:55  james
 * ::
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 16:20:24  james
 * *** empty log message ***
 *
 *
 */

#ifndef __PROJECT_H__
#define __PROJECT_H__

#include "config.h"

#ifdef TM_IN_SYS_TIME
#include <sys/time.h>
#ifdef TIME_WITH_SYS_TIME
#include <time.h>
#endif
#else
#ifdef TIME_WITH_SYS_TIME
#include <sys/time.h>
#endif
#include <time.h>
#endif

#include <stdio.h>
#include <stdlib.h>

#ifdef HAVE_MALLOC_H
#include <malloc.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_STRINGS_H
#include <strings.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#if defined(HAVE_STDINT_H)
#include <stdint.h>
#elif defined(HAVE_SYS_INT_TYPES_H)
#include <sys/int_types.h>
#endif

#include <termio.h>
#include <termios.h>

#include <signal.h>
#include <fcntl.h>
#include <errno.h>

#include <utmp.h>
#include <pty.h>

#include <stdarg.h>

#include "sympathy.h"

#endif /* __PROJECT_H__ */

/* ansi.c */
extern ANSI *ansi_new_from_terminal (TTY * t, int utf8);
/* crt.c */
extern void crt_erase (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color);
extern void crt_cls (CRT * c);
extern void crt_scroll_up (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color);
extern void crt_scroll_down (CRT * c, CRT_Pos s, CRT_Pos e, int ea,
                             int color);
extern void crt_reset (CRT * c);
extern void crt_insert (CRT * c, CRT_CA ca);
/* html.c */
extern ANSI *ansi_new_html (FILE * f);
/* libsympathy.c */
/* render.c */
/* version.c */
extern char *libsympathy_version (void);
/* vt102.c */
extern int vt102_cmd_length[128];
extern int vt102_cmd_termination[128];
extern void vt102_crt_update (Context * c);
extern void vt102_do_resize (Context * c);
extern void vt102_log_line (Context * c, int line);
extern void vt102_history (Context * c, CRT_Pos t, CRT_Pos b);
extern void vt102_clip_cursor (VT102 * v, CRT_Pos tl, CRT_Pos br);
extern void vt102_cursor_normalize (VT102 * v);
extern void vt102_cursor_carriage_return (VT102 * v);
extern void vt102_cursor_advance_line (Context * c);
extern void vt102_cursor_retreat_line (Context * c);
extern void vt102_do_pending_wrap (Context * c);
extern void vt102_cursor_advance (Context * c);
extern void vt102_cursor_retreat (VT102 * v);
extern void vt102_reset_tabs (VT102 * v);
extern void vt102_cursor_advance_tab (VT102 * v);
extern void vt102_cursor_retreat_tab (VT102 * v);
extern int vt102_cursor_home (VT102 * v);
extern int vt102_cursor_absolute (VT102 * v, int x, int y);
extern int vt102_cursor_relative (VT102 * v, int x, int y);
extern void vt102_delete_from_line (VT102 * v, CRT_Pos p);
extern void vt102_insert_into_line (VT102 * v, CRT_Pos p);
extern void vt102_change_mode (Context * c, int private, char *ns, int set);
extern void vt102_parse_mode_string (Context * c, char *buf, int len);
extern void vt102_change_attr (VT102 * v, char *na);
extern void vt102_parse_attr_string (VT102 * v, char *buf, int len);
extern void vt102_save_state (VT102 * v);
extern void vt102_restore_state (VT102 * v);
extern void vt102_regular_char (Context * c, VT102 * v, uint32_t ch);
extern int vt102_send_id (Context * c, char *buf);
extern void vt102_scs (Context * c, int g, int s);
extern void vt102_status_line (VT102 * v, char *str);
extern void vt102_parser_reset (VT102_parser * p);
extern void vt102_reset_state (Context * c);
extern int vt102_rx_hook (Context * c, int ch);
extern int vt102_parse_char (Context * c, int ch);
extern void vt102_send (Context * c, uint8_t key);
extern void vt102_reset (Context * c);
extern VT102 *vt102_new (CRT_Pos * size);
extern void vt102_set_ansi (VT102 * v, int ansi);
extern void vt102_resize (Context * c, CRT_Pos size);
extern void vt102_free (VT102 * v);
/* tty.c */
extern void tty_pre_select (TTY * t, fd_set * rfds, fd_set * wfds);
extern int tty_get_status (TTY * t, TTY_Status * s);
extern int tty_get_baud (TTY * t);
extern void tty_set_baud (TTY * t, int rate);
extern void tty_send_break (TTY * t);
extern void tty_set_flow (TTY * t, int flow);
extern void tty_hangup (TTY * t);
extern void tty_length (TTY * t, int l);
extern void tty_winch (TTY * t, CRT_Pos size);
extern void tty_parse_reset (Context * c);
extern void tty_analyse (Context * c);
extern TTY_Parser *tty_parser_new (void);
extern int tty_parse (Context * c, uint8_t * buf, int len);
/* keydis.c */
extern KeyDis *keydis_vt102_new (void);
extern KeyDis *keydis_ipc_new (Socket * s);
/* history.c */
extern History *history_new (int n);
extern void history_free (History * h);
extern void history_add (History * h, CRT_CA * c);
/* ring.c */
extern int ring_read (Ring * r, void *b, int n);
extern int ring_write (Ring * r, void *b, int n);
extern int ring_space (Ring * r);
extern int ring_bytes (Ring * r);
extern Ring *ring_new (int n);
/* ptty.c */
extern TTY *ptty_open (char *path, char *argv[], CRT_Pos * size);
/* terminal.c */
extern int terminal_winches;
extern void terminal_atexit (void);
extern void terminal_getsize (TTY * _t);
extern void terminal_dispatch (void);
extern void terminal_register_handlers (void);
extern TTY *terminal_open (int rfd, int wfd);
/* util.c */
extern int wrap_read (int fd, void *buf, int len);
extern int wrap_write (int fd, void *buf, int len);
extern void set_nonblocking (int fd);
extern void set_blocking (int fd);
extern void default_termios (struct termios *termios);
extern void client_termios (struct termios *termios);
extern int fput_cp (FILE * f, uint32_t ch);
extern void crash_out (char *why);
extern void *xmalloc (size_t s);
extern void *xrealloc (void *p, size_t s);
extern char *xstrdup (const char *s);
/* log.c */
extern Log *file_log_new (char *fn, int rotate);
extern void log_f (Log * log, char *fmt, ...);
/* ipc.c */
extern IPC_Msg *ipc_check_for_message_in_slide (Slide * s);
extern void ipc_consume_message_in_slide (Slide * s);
extern int ipc_msg_send (Socket * s, IPC_Msg * m);
extern int ipc_msg_send_debug (Socket * s, char *msg);
extern int ipc_msg_send_initialize (Socket * s);
extern int ipc_msg_send_history (Socket * s, History_ent * l);
extern int ipc_msg_send_vt102 (Socket * s, VT102 * v);
extern int ipc_msg_send_key (Socket * s, int key);
extern int ipc_msg_send_term (Socket * s, void *buf, int len);
extern int ipc_msg_send_status (Socket * s, char *buf);
extern int ipc_msg_send_setbaud (Socket * s, int baud);
extern int ipc_msg_send_sendbreak (Socket * s);
extern int ipc_msg_send_setflow (Socket * s, int flow);
extern int ipc_msg_send_setansi (Socket * s, int ansi);
extern int ipc_msg_send_hangup (Socket * s);
extern int ipc_msg_send_setsize (Socket * s, CRT_Pos size);
extern int ipc_msg_send_reset (Socket * s);
/* slide.c */
extern void slide_free (Slide * s);
extern void slide_consume (Slide * s, int n);
extern void slide_added (Slide * s, int n);
extern Slide *slide_new (int n);
extern void slide_expand (Slide * s, int n);
/* symsocket.c */
extern int wrap_recv (int fd, void *buf, int len);
extern int wrap_send (int fd, void *buf, int len);
extern void socket_free (Socket * s);
extern void socket_free_parent (Socket * s);
extern Socket *socket_listen (char *path);
extern Socket *socket_accept (Socket * l);
extern Socket *socket_connect (char *path);
extern void socket_consume_msg (Socket * s);
extern void socket_pre_select (Socket * s, fd_set * rfds, fd_set * wfds);
extern int socket_post_select (Socket * s, fd_set * rfds, fd_set * wfds);
extern int socket_write (Socket * s, void *buf, int len);
/* serial.c */
extern TTY *serial_open (char *path, int lock_mode);
/* cmd.c */
extern int cmd_parse (Cmd * c, Context * ctx, ANSI * a, char *buf);
extern void cmd_show_status (Cmd * c, Context * ctx);
extern int cmd_key (Cmd * c, Context * ctx, ANSI * a, int key);
extern int cmd_deactivate (Cmd * c, Context * ctx);
extern int cmd_activate (Cmd * c, Context * ctx);
extern void cmd_new_status (Cmd * c, Context * ctx, char *msg);
extern Cmd *cmd_new (void);
/* lockfile.c */
extern Filelist *filelist_new (void);
extern void filelist_remove (Filelist * fl, Filelist_ent * fle);
extern void filelist_add (Filelist * fl, char *fn);
extern void filelist_free (Filelist * fl);
extern void filelist_print (Filelist * fl, FILE * f);
extern int lockfile_make (char *name);
extern void lockfile_add_places (Filelist * fl, char *leaf);
extern void lockfile_regularize_and_add (Filelist * fl, char *leaf);
extern void lockfile_add_name_from_path (Filelist * fl, char *file);
extern void lockfile_add_name_from_dev (Filelist * fl, dev_t dev);
extern void lockfile_check_dir_for_dev (Filelist * fl, char *dir, dev_t dev);
extern Filelist *lockfile_make_list (char *device);
extern void lockfile_remove_stale (Filelist * fl);
extern Filelist *lockfile_lock (Filelist * fl);
extern void lockfile_unlock (Filelist * fl);
extern int serial_lock_check (Serial_lock * l);
extern void serial_lock_free (Serial_lock * l);
extern Serial_lock *serial_lock_new (char *dev, int mode);
/* utf8.c */
extern int utf8_flush (Context * c);
extern int utf8_parse (Context * c, uint32_t ch);
extern UTF8 *utf8_new (void);
extern int utf8_encode (char *ptr, int ch);
extern int utf8_emit (TTY * t, int ch);
/* vt102_charset.c */
extern uint32_t vt102_charset_c0[128];
extern uint32_t vt102_charset_us[128];
extern uint32_t vt102_charset_uk[128];
extern uint32_t vt102_charset_vt52[128];
extern uint32_t vt102_charset_gl[128];
extern uint32_t *charset_from_csid[];
/* rotate.c */
extern void rotate_gzip (char *file);
extern void rotate (char *file);
extern int rotate_check (char *file);
/* raw.c */
extern RX *rx_new_raw (int rfd, int wfd);
extern TTY *terminal_new_raw (int rfd, int wfd);
extern ANSI *ansi_new_raw (int rfd, int wfd);

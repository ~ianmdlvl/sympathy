/* 
 * raw.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: raw.c,v 1.10 2011/02/28 18:10:43 james Exp $";

/* 
 * $Log: raw.c,v $
 * Revision 1.10  2011/02/28 18:10:43  james
 * *** empty log message ***
 *
 * Revision 1.9  2011/02/06 16:51:22  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/06 21:34:09  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/06 21:33:02  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/06 17:21:41  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 *
 */

#include "project.h"

typedef struct {
  RX_SIGNATURE;
  int rfd;
  int wfd;
} RX_Raw;

typedef struct {
  TTY_SIGNATURE;
} RAW_TERMINAL;


static int
rx_raw_rx (RX * _r, int ch)
{
  RX_Raw *r = (RX_Raw *) _r;
  int ret;
  uint8_t c = ch;
  set_blocking (r->wfd);
  ret = (write (r->wfd, &c, 1) == 1) ? 0 : -1;
}

static void
rx_raw_close (RX * r)
{
  free (r);
}

RX *
rx_new_raw (int rfd, int wfd)
{
  RX_Raw *ret;

  ret = xmalloc (sizeof (RX_Raw));
  memset (ret, 0, sizeof (RX_Raw));

  ret->rx = rx_raw_rx;
  ret->close = rx_raw_close;

  ret->rfd = rfd;
  ret->wfd = wfd;

  return (RX *) ret;
}

static int my_wrap_read (int fd, void *buf, int len)
{
  int red;

  red = read (fd, buf, len);
#if 1
  if (!red)
    return -1;
#endif

  if ((red < 0) && (errno == EAGAIN))
    red = 0;

  return red;
}



static int
raw_terminal_read (TTY * _t, void *buf, int len)
{
  RAW_TERMINAL *t = (RAW_TERMINAL *) _t;
  int red, done = 0;

  set_nonblocking (t->rfd);

  do {

    red = my_wrap_read (t->rfd, buf, len);
    if (red < 0)
	return done ? done:-1;
    if (!red)
      return done;

    buf += red;
    len -= red;
    done += red;
  }
  while (len);


  return done;
}


static int
raw_terminal_write (TTY * _t, void *buf, int len)
{
  int writ, done = 0;
  RAW_TERMINAL *t = (RAW_TERMINAL *) _t;

  set_blocking (t->wfd);

  do {

    writ = wrap_write (t->wfd, buf, len);
    if (writ < 0)
      return -1;

    if (!writ)
      usleep (1000);

    buf += writ;
    len -= writ;
    done += writ;
  }
  while (len);


  return done;
}



static void
raw_terminal_close (TTY * _t)
{
  RAW_TERMINAL *t = (RAW_TERMINAL *) _t;
  set_blocking (t->rfd);
  set_blocking (t->wfd);

  free (t);
}


TTY *
terminal_new_raw (int rfd, int wfd)
{
  RAW_TERMINAL *t;

  t = (RAW_TERMINAL *) malloc (sizeof (RAW_TERMINAL));
  memset (t, 0, sizeof (t));

  strcpy (t->name, "raw");
  t->rfd = rfd;
  t->wfd = wfd;

  set_nonblocking (rfd);
  set_nonblocking (wfd);

  t->recv = raw_terminal_read;
  // t->xmit = raw_terminal_write;
  t->close = raw_terminal_close;
  t->blocked = 0;

  return (TTY *) t;
}

static void
ansi_raw_one_shot (ANSI * a, CRT * c)
{
}

static int
ansi_raw_key (ANSI * a, Context * c, int key)
{
  return c->k->key (c->k, c, key);
}

static void
ansi_raw_parse (ANSI * a, Context * c, uint8_t * buf, int red)
{
  while (red--)
    ansi_raw_key (a, c, *(buf++));
}

static int
ansi_raw_dispatch (ANSI * a, Context * c)
{
  char buf[1024];
  int red;

  if (!a->terminal)
    return 0;

  red = a->terminal->recv (a->terminal, buf, sizeof (buf));
  if (red <= 0)
    return red;

  ansi_raw_parse (a, c, buf, red);

  return 0;
}
static void
ansi_raw_free (ANSI * a)
{
  free (a);
}

ANSI *
ansi_new_raw (int rfd, int wfd)
{
  ANSI *ret;

  ret = malloc (sizeof (ANSI));
  memset (ret, 0, sizeof (ANSI));

  ret->terminal = terminal_new_raw (rfd, wfd);
  ret->dispatch = ansi_raw_dispatch;
  ret->close = ansi_raw_free;

  return ret;
}

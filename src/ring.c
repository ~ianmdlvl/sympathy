/* 
 * ring.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: ring.c,v 1.8 2008/03/07 13:16:02 james Exp $";

/* 
 * $Log: ring.c,v $
 * Revision 1.8  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#include "project.h"

int
ring_read (Ring * r, void *b, int n)
{
  int red = 0;

  while (n--) {

    if (!ring_read_one (r, b))
      break;

    b++;
    red++;
  }

  return red;
}

int
ring_write (Ring * r, void *b, int n)
{
  int writ = 0;

  while (n--) {

    if (!ring_write_one (r, b))
      break;

    b++;
    writ++;
  }

  return writ;
}

int
ring_space (Ring * r)
{
  int i;

  i = r->size - RING_NEXT_W (r) + r->rptr;
  i %= r->size;
  return i;
}

int
ring_bytes (Ring * r)
{
  int i;

  i = r->size + r->wptr - r->rptr;
  i %= r->size;
  return i;
}




Ring *
ring_new (int n)
{
  Ring *ret = (Ring *) xmalloc (sizeof (Ring));
  ret->ring = (uint8_t *) xmalloc (n);
  ret->size = n;
  ret->wptr = ret->rptr = 0;

  return ret;
}

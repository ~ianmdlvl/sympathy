/* 
 * ring.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: ring.h,v 1.6 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: ring.h,v $
 * Revision 1.6  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#ifndef __RING_H__
#define __RING_H__

typedef struct {
  uint8_t *ring;
  int wptr;
  int rptr;
  int size;
} Ring;

#define RING_NEXT(r,a) (((a)+1) % ((r)->size))
#define RING_NEXT_R(r) RING_NEXT(r,r->rptr)
#define RING_NEXT_W(r) RING_NEXT(r,r->wptr)

#define RING_EMPTY(r) (((r)->wptr) == ((r)->rptr))
#define RING_FULL(r) (RING_NEXT_W(r) == ((r)->rptr))

static inline int
ring_write_one (Ring * r, uint8_t * c)
{
  if (RING_FULL (r))
    return 0;

  r->ring[r->wptr++] = *c;

  if (r->wptr == r->size)
    r->wptr = 0;

  return 1;
}

static inline int
ring_read_one (Ring * r, uint8_t * c)
{
  if (RING_EMPTY (r))
    return 0;

  *c = r->ring[r->rptr++];

  if (r->rptr == r->size)
    r->rptr = 0;

  return 1;
}



#endif /* __RING_H__ */

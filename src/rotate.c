/* 
 * rotate.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: rotate.c,v 1.11 2010/07/16 11:04:10 james Exp $";

/* 
 * $Log: rotate.c,v $
 * Revision 1.11  2010/07/16 11:04:10  james
 * ignore tedious return values
 *
 * Revision 1.10  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/06 01:41:48  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/03 18:16:16  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/03 06:30:15  staffcvs
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/03 06:26:05  staffcvs
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:22:51  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/03 06:20:14  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/03 06:04:18  james
 * *** empty log message ***
 *
 */

#include "project.h"
#include <sys/stat.h>

#define ROTATE_IF_OVER	(4*1024*1024)
#define NUM_FILES_TO_KEEP 10
#define GZIP_AFTER 2

void
rotate_gzip (char *file)
{
  switch (fork ()) {
  case 0:
    break;
  case -1:
  default:
    return;
  }

  int result;

  result = daemon (1, 0);
  execlp ("gzip", "gzip", file, (char *) 0);
  _exit (-1);
}

void
rotate (char *file)
{
  char *buf1, *buf2;
  int i;
  if (!file)
    return;

  i = strlen (file) + 32;
  buf1 = xmalloc (i);
  buf2 = xmalloc (i);

  for (i = NUM_FILES_TO_KEEP; i > 0; --i) {
    sprintf (buf1, "%s.%d", file, i - 1);
    sprintf (buf2, "%s.%d", file, i);
    rename (buf1, buf2);

    sprintf (buf1, "%s.%d.gz", file, i - 1);
    sprintf (buf2, "%s.%d.gz", file, i);
    rename (buf1, buf2);
  }

  sprintf (buf1, "%s.%d", file, 0);
  rename (file, buf1);

  sprintf (buf1, "%s.%d", file, GZIP_AFTER);

  if (!access (buf1, R_OK))
    rotate_gzip (buf1);

  free (buf2);
  free (buf1);
}



int
rotate_check (char *file)
{
  struct stat st;
  if (!file)
    return 0;
  if (stat (file, &st))
    return 0;
  if (st.st_size <= ROTATE_IF_OVER)
    return 0;
  return 1;
}

/* 
 * rx.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: rx.h,v 1.5 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: rx.h,v $
 * Revision 1.5  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/06 15:17:26  james
 * *** empty log message ***
 *
 *
 */

#ifndef __RX_H__
#define __RX_H__
#define RX_SIGNATURE \
  int (*rx)(struct RX_struct *,int); \
  void (*close)(struct RX_struct *);


typedef struct RX_struct {
  RX_SIGNATURE;
} RX;


#endif /* __RX_H__ */

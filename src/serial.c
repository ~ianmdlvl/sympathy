/* 
 * serial.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: serial.c,v 1.18 2011/02/06 16:51:22 james Exp $";

/* 
 * $Log: serial.c,v $
 * Revision 1.18  2011/02/06 16:51:22  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/24 00:47:14  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/23 13:05:58  staffcvs
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/15 19:51:30  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/15 19:09:00  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/15 16:48:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/14 16:21:17  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 12:51:14  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/14 10:39:14  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/09 15:47:28  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/07 11:11:14  staffcvs
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/07 01:02:52  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/06 17:53:28  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/04 01:32:39  james
 * *** empty log message ***
 *
 */

#include "project.h"
#include <pwd.h>
#include <dirent.h>
#include <sys/stat.h>


typedef struct {
  TTY_SIGNATURE;
  Serial_lock *lock;
  int fd;
} Serial;


static void
serial_close (TTY * _t)
{
  Serial *t = (Serial *) _t;

  if (!t)
    return;

  tcflush (t->fd, TCIOFLUSH);
  close (t->fd);
  free (t);
}


static int
serial_read (TTY * _t, void *buf, int len)
{
  Serial *t = (Serial *) _t;
  int red, done = 0;

  t->blocked = serial_lock_check (t->lock);

  if (t->blocked)
    return 0;

  do {

    red = wrap_read (t->fd, buf, len);
    if (red < 0) 
	return done ? done:-1;
    if (!red)
      return done;

    buf += red;
    len -= red;
    done += red;
  }
  while (len);


  return done;
}


static int
serial_write (TTY * _t, void *buf, int len)
{
  int writ, done = 0;
  Serial *t = (Serial *) _t;

  t->blocked = serial_lock_check (t->lock);
  if (t->blocked)
    return 0;

  do {

    writ = wrap_write (t->fd, buf, len);
    if (writ < 0)
      return -1;
    if (!writ)
      sleep (1);

    buf += writ;
    len -= writ;
    done += writ;
  }
  while (len);


  return done;
}

TTY *
serial_open (char *path, int lock_mode)
{
  Serial *t;
  pid_t child;
  char name[1024];
  struct winsize winsize = { 0 };
  struct termios termios;
  int fd;
  Serial_lock *l;

  l = serial_lock_new (path, lock_mode);
  if (!l)
    return NULL;


  fd = open (path, O_RDWR | O_NOCTTY | O_NONBLOCK);

  set_nonblocking (fd);


  if (tcgetattr (fd, &termios)) {
    close (fd);
    return NULL;
  }
  default_termios (&termios);

  if (tcsetattr (fd, TCSANOW, &termios)) {
    close (fd);
    return NULL;
  }

  t = (Serial *) xmalloc (sizeof (Serial));

  t->lock = l;

  strncpy (t->name, path, sizeof (t->name));
  t->name[sizeof (t->name) - 1] = 0;

  t->recv = serial_read;
  t->xmit = serial_write;
  t->close = serial_close;
  t->fd = fd;
  t->rfd = t->fd;
  t->wfd = t->fd;
  t->size.x = VT102_COLS_80;
  t->size.y = VT102_ROWS_24;
  t->blocked = serial_lock_check (t->lock);
  t->hanging_up = 0;

  return (TTY *) t;
}

/* 
 * slide.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: slide.c,v 1.8 2008/03/07 14:13:40 james Exp $";

/* 
 * $Log: slide.c,v $
 * Revision 1.8  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/14 00:57:58  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#include "project.h"

void
slide_free (Slide * s)
{
  free (s->slide);
  free (s);
}

void
slide_consume (Slide * s, int n)
{
  s->nbytes -= n;

  if (s->nbytes < 0)
    crash_out ("slide_consume called with -ve number of bytes");

  memmove (s->slide, s->slide + n, s->nbytes);

  if ((s->size > s->target_size) && (s->nbytes <= s->target_size)) {
    s->size = s->target_size;
    s->slide = realloc (s->slide, s->size);
  }

}

void
slide_added (Slide * s, int n)
{
  s->nbytes += n;
}

Slide *
slide_new (int n)
{
  Slide *ret = (Slide *) xmalloc (sizeof (Slide));

  ret->slide = (uint8_t *) xmalloc (n);
  ret->size = n;
  ret->target_size = n;
  ret->nbytes = 0;

  return ret;
}


void
slide_expand (Slide * s, int n)
{
  n += s->nbytes;

  if (n <= s->size)
    return;

  while (n > s->size)
    s->size <<= 1;

  s->slide = xrealloc (s->slide, s->size);

}

/* 
 * ring.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: slide.h,v 1.5 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: slide.h,v $
 * Revision 1.5  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#ifndef __SLIDE_H__
#define __SLIDE_H__

typedef struct {
  uint8_t *slide;
  int nbytes;
  int target_size;
  int size;
} Slide;

#define SLIDE_FULL(s) ((s)->nbytes==(s)->size)
#define SLIDE_EMPTY(s) (!((s)->nbytes))
#define SLIDE_SPACE(s) (((s)->size)-((s)->nbytes))
#define SLIDE_BYTES(s) ((s)->nbytes)
#define SLIDE_RPTR(s) ((s)->slide)
#define SLIDE_WPTR(s) (((s)->slide)+((s)->nbytes))

#endif /* __SLIDE_H__ */

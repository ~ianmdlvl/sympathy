/*
 * sympathy.h.head.in:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/*
 * $Id: sympathy.h.head.in,v 1.4 2010/07/16 11:06:39 james Exp $
 */

/*
 * $Log: sympathy.h.head.in,v $
 * Revision 1.4  2010/07/16 11:06:39  james
 * add missing G2
 *
 * Revision 1.3  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/13 18:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 16:20:24  james
 * *** empty log message ***
 *
 *
 */

/* MAKE ABSOLUTELY SURE THAT YOU ARE EDITING THE sympathy.h.in */
/* FILE FROM WHICH THIS IS GENERATED - OTHERWISE YOUR EDITS */
/* WILL BE LOST */

#ifndef __SYMPATHY_H__
#define __SYMPATHY_H__

#ifdef __cplusplus
extern "C" { 
#endif

#include <stdio.h>
#include <stdlib.h>

/* the integer constants here are set by configure */

/* get uint32_t and friends defined */
#if 1
#include <stdint.h>
#elif 0
#include <sys/int_types.h>
#endif
#if 1
#include <unistd.h>
#endif

/* If the following is <> then configure failed to find where */
/* struct tm was defined - report it as a bug */

/* get struct tm defined */
#include <time.h>

#if 0
#include <sys/time.h>
#if 0
#include <time.h>
#endif
#else
#if 0
#include <sys/time.h>
#endif
#include <time.h>
#endif


#if 1
#include <malloc.h>
#endif

#if 1
#include <unistd.h>
#endif

#if 1
#include <stdint.h>
#elif 0
#include <sys/int_types.h>
#endif

#include <termio.h>
#include <termios.h>


/* 
 * crt.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: crt.h,v 1.18 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: crt.h,v $
 * Revision 1.18  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/28 16:57:51  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/26 19:08:27  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/20 19:25:09  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/07 13:22:51  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/07 12:41:06  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */

#ifndef __CRT_H__
#define __CRT_H__

#define CRT_ROWS 60
#define CRT_COLS 132

#define CRT_CELS (CRT_ROWS*CRT_COLS)
#define CRT_ADDR(r,c) (((r)*CRT_COLS)+(c))
#define CRT_ADDR_POS(p) ((((p)->y)*CRT_COLS)+((p)->x))

#define CRT_ATTR_NORMAL    0x0
#define CRT_ATTR_UNDERLINE 0x1
#define CRT_ATTR_REVERSE   0x2
#define CRT_ATTR_BLINK	   0x4
#define CRT_ATTR_BOLD	   0x8


#define CRT_COLOR_BLACK		0x0
#define CRT_COLOR_RED		0x1
#define CRT_COLOR_GREEN		0x2
#define CRT_COLOR_YELLOW	0x3
#define CRT_COLOR_BLUE		0x4
#define CRT_COLOR_MAGENTA	0x5
#define CRT_COLOR_CYAN		0x6
#define CRT_COLOR_WHITE		0x7
#define CRT_COLOR_INTENSITY	0x8

#define CRT_COLOR_FG_MASK	0xf0
#define CRT_COLOR_FG_SHIFT	4

#define CRT_COLOR_BG_MASK	0xf
#define CRT_COLOR_BG_SHIFT	0

#define CRT_COLOR_BG(a)		(((a) & CRT_COLOR_BG_MASK) >> CRT_COLOR_BG_SHIFT)
#define CRT_COLOR_FG(a)		(((a) & CRT_COLOR_FG_MASK) >> CRT_COLOR_FG_SHIFT)

#define CRT_MAKE_COLOR(f,b)	(((f) << CRT_COLOR_FG_SHIFT)|(b))

#define CRT_BGCOLOR_NORMAL	CRT_COLOR_BLACK
#define CRT_FGCOLOR_NORMAL	CRT_COLOR_WHITE

#define CRT_COLOR_NORMAL	CRT_MAKE_COLOR(CRT_FGCOLOR_NORMAL,CRT_BGCOLOR_NORMAL)

typedef struct __attribute__ ((packed)) {
  uint32_t chr;
  uint8_t attr;
  uint8_t color;
} CRT_CA;

typedef struct {
  int x;
  int y;
} CRT_Pos;


typedef struct {
  CRT_Pos s;
  CRT_Pos e;
  int dir;
} CRT_ScrollHint;

typedef struct CRT_struct {
  CRT_CA screen[CRT_CELS];
  CRT_Pos pos;
  int hide_cursor;
  CRT_Pos size;
} CRT;


static inline
crt_ca_cmp (CRT_CA a, CRT_CA b)
{
  return memcmp (&a, &b, sizeof (a));
}

static inline
crt_pos_cmp (CRT_Pos a, CRT_Pos b)
{
  return memcmp (&a, &b, sizeof (a));
}

#endif /* __CRT_H__ */
/* 
 * utf8.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: utf8.h,v 1.8 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: utf8.h,v $
 * Revision 1.8  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/22 19:12:05  james
 * *** empty log message ***
 *
 *
 */

#ifndef __UTF8_H__
#define __UTF8_H__


typedef struct {
  int in_utf8;

  uint8_t utf_buf[4];
  int utf_ptr;

  uint32_t ch;
  int sh;
} UTF8;

#endif /* __UTF8_H__ */
/* 
 * tty.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: tty.h,v 1.16 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: tty.h,v $
 * Revision 1.16  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/22 19:12:05  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/14 10:36:18  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/14 10:34:30  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/09 15:47:28  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/07 00:43:27  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/06 20:26:58  james
 * *** empty log message ***
 *
 */

#ifndef __TTY_H__
#define __TTY_H__


#define SYM_CHAR_RESET 	(-1)

#define TTY_SIGNATURE \
	char name[1024]; \
        int blocked; \
	CRT_Pos size; \
	void (*close)(struct TTY_struct *); \
	int (*recv)(struct TTY_struct *,void *buf,int len); \
	int (*xmit)(struct TTY_struct *,void *buf,int len); \
	int rfd; \
	int wfd; \
	int hanging_up; \
	struct timeval hangup_clock; \
	int displayed_length;


#define TTY_BITFREQ_LEN	10

typedef struct {
  int in_dle;
  int in_errmark;

  int bitfreq[TTY_BITFREQ_LEN];
  int biterrs;

  struct timeval lasterr;
  int guessed_baud;
} TTY_Parser;


typedef struct TTY_struct {
  TTY_SIGNATURE;
} TTY;

typedef struct {
  int lines;
  int blocked;
  struct termios termios;
  int baud;
} TTY_Status;

#endif /* __TTY_H__ */
/* 
 * ansi.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: ansi.h,v 1.20 2008/03/10 11:49:32 james Exp $
 */

/* 
 * $Log: ansi.h,v $
 * Revision 1.20  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/03/06 21:34:09  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/03/06 21:33:02  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/20 20:16:07  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/20 19:44:37  james
 * @@
 *
 * Revision 1.9  2008/02/20 19:36:06  james
 * @@
 *
 * Revision 1.8  2008/02/20 19:25:09  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/07 00:43:27  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:31:25  james
 * *** empty log message ***
 *
 */

#ifndef __ANSI_H__
#define __ANSI_H__

#define ANSI_INVAL -1

#define ANSI_ESCAPE_BUF_LEN 10
#define ANSI_ESCAPE_TIMEOUT	100000 /* in ms */

typedef struct {
  int in_escape;
  struct timeval last_escape;
  char escape_buf[ANSI_ESCAPE_BUF_LEN];
  int escape_ptr;
} ANSI_Parser;

struct CRT_struct;
struct Context_struct;

typedef struct ANSI_struct {
  ANSI_Parser parser;

  TTY *terminal;
  int eof;


  CRT crt;
  CRT_Pos pos;
  CRT_Pos size;
  int hide_cursor;
  int attr;
  int color;

  int utf8;

  int history_ptr;
  FILE *file;

  int (*dispatch) (struct ANSI_struct *, struct Context_struct *);
  int (*update) (struct ANSI_struct *, struct Context_struct *);
  int (*one_shot) (struct ANSI_struct *, struct CRT_struct *);
  int (*reset) (struct ANSI_struct *, struct CRT_struct *);
  int (*set_title) (struct ANSI_struct *, char *);
  void (*terminal_reset) (struct ANSI_struct *);
  void (*close) (struct ANSI_struct *);
} ANSI;


#endif /* __ANSI_H__ */
/* 
 * vt102.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: vt102.h,v 1.25 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: vt102.h,v $
 * Revision 1.25  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.24  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.23  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.22  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.21  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.20  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/02/26 19:08:27  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/02/26 16:53:24  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/02/24 12:22:42  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/22 14:51:54  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/07 01:57:46  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/06 20:26:58  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/06 17:53:28  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/06 15:53:22  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:36:41  james
 * *** empty log message ***
 *
 */

#ifndef __VT102_H__
#define __VT102_H__

#define VT102_CMD_LEN 128

#define VT102_MAX_ROWS		((CRT_ROWS) - 1)
#define VT102_ROWS_24		24
#define VT102_COLS_132		132
#define VT102_COLS_80		80
#define VT102_MAX_COLS		VT102_COLS_132
#define VT102_STATUS_ROW	24

#define VT102_NMODES		32


typedef struct {
  int in_escape;
  int in_cmd;

  int cmd_ptr;
  int cmd_more_bytes;
  int cmd_termination;
  char cmd_buf[VT102_CMD_LEN];
} VT102_parser;

typedef struct {
  CRT_Pos pos;
  int attr;
  int color;
  int origin_mode;
} VT102_State;


typedef struct {
  CRT_Pos top_margin, bottom_margin;
  CRT_Pos screen_start, screen_end;
  VT102_parser parser;
  int attr;
  int color;
  CRT crt;

  int pending_wrap;
  CRT_Pos pos, current_line;

  VT102_State saved;

  uint8_t modes[VT102_NMODES];
  uint8_t private_modes[VT102_NMODES];

  uint8_t tabs[VT102_COLS_132];

  int application_keypad_mode;

  int last_reg_char;
  int xn_glitch;

  CRT_Pos current_size;
  CRT_Pos original_size;

  int g[2];
  int cs;

} VT102;

#define VT102_PRIVATE_MODE_CURSOR_MODE		1
#define VT102_PRIVATE_MODE_VT52	 		2
#define VT102_PRIVATE_MODE_132COLS	 	3
#define VT102_PRIVATE_MODE_SMOOTH_SCROLL 	4
#define VT102_PRIVATE_MODE_REVERSE_SCREEN 	5
#define VT102_PRIVATE_MODE_ORIGIN_MODE		6
#define VT102_PRIVATE_MODE_AUTO_WRAP		7
#define VT102_PRIVATE_MODE_AUTO_REPEAT		8
#define VT102_PRIVATE_MODE_SHOW_CURSOR		25

#define VT102_MODE_KEYBOARD_DISABLE		2
#define VT102_MODE_INSERT			4
#define VT102_MODE_LOCAL_ECHO_OFF		12
#define VT102_MODE_NEWLINE_MODE			20

#endif /* __VT102_H__ */
/* 
 * src/keys.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: keys.h,v 1.10 2008/03/07 14:16:44 james Exp $
 */

/* 
 * $Log: keys.h,v $
 * Revision 1.10  2008/03/07 14:16:44  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/06 01:49:19  james
 * *** empty log message ***
 *
 * Revision 1.5  2008 /03 /06 01:41:48  james
 * *** empty log message ***
 *
 * Revision 1.4  2008 /02 /07 00:44:07  james
 * *** empty log message ***
 *
 */

#ifndef __KEYS_H__
#define __KEYS_H__

#define KEY_UP		128             /* A */
#define KEY_DOWN	129            /* B */
#define KEY_RIGHT	130           /* C */
#define KEY_LEFT	131            /* D */
#define KEY_MIDDLE	132          /* E */
#define KEY_END		133            /* F */
#define KEY_134		134            /* G */
#define KEY_HOME	135            /* H */
#define KEY_136		136            /* I */
#define KEY_137		137            /* J */
#define KEY_138		138            /* K */
#define KEY_139		139            /* L */
#define KEY_ENTER	140           /* M */
#define KEY_141		141            /* N */
#define KEY_142		142            /* O */
#define KEY_PF1		143	/* P */    /* Also F1 */
#define KEY_PF2		144	/* Q */    /* Also F2 */
#define KEY_PF3		145	/* R */    /* Also F3 */
#define KEY_PF4		146	/* S */    /* Also F4 */
#define KEY_147		147            /* T */
#define KEY_148		148            /* U */
#define KEY_149		149            /* V */
#define KEY_150		150            /* W */
#define KEY_151		151            /* X */
#define KEY_152		152            /* Y */
#define KEY_153		153            /* Z */
#define KEY_154		154            /* a */
#define KEY_155		155            /* b */
#define KEY_156		156            /* c */
#define KEY_157		157            /* d */
#define KEY_158		158            /* e */
#define KEY_159		159            /* f */
#define KEY_160		160            /* g */
#define KEY_161		161            /* h */
#define KEY_162		162            /* i */
#define KEY_STAR	163            /* j */
#define KEY_PLUS	164            /* k */
#define KEY_COMMA	165           /* l */
#define KEY_MINUS	166           /* m */
#define KEY_PERIOD	167          /* n */
#define KEY_DIVIDE	168          /* o */
#define KEY_0		169              /* p */
#define KEY_1		170              /* q */
#define KEY_2		171              /* r */
#define KEY_3		172              /* s */
#define KEY_4		173              /* t */
#define KEY_5		174              /* u */
#define KEY_6		175              /* v */
#define KEY_7		176              /* w */
#define KEY_8		177              /* x */
#define KEY_9		178              /* y */
#define KEY_179		179            /* z */
#define KEY_180		180            /* 0 */
#define KEY_VT220_HOME	181      /* 1 */
#define KEY_INSERT	182          /* 2 */
#define KEY_DELETE	183          /* 3 */
#define KEY_VT220_END	184       /* 4 */
#define KEY_PGUP	185            /* 5 */
#define KEY_PGDN	186            /* 6 */
#define KEY_187		187            /* 7 */
#define KEY_188		188            /* 8 */
#define KEY_189		189            /* 9 */
#define KEY_190		190            /* 10 */
#define KEY_F1		191             /* 11 */
#define KEY_F2		192             /* 12 */
#define KEY_F3		193             /* 13 */
#define KEY_F4		194             /* 14 */
#define KEY_F5		195             /* 15 */
#define KEY_196		196            /* 16 */
#define KEY_F6		197             /* 17 */
#define KEY_F7		198             /* 18 */
#define KEY_F8		199             /* 19 */
#define KEY_F9		200             /* 20 */
#define KEY_F10		201            /* 21 */
#define KEY_202		202            /* 22 */
#define KEY_F11		203            /* 23 */
#define KEY_F12 	204            /* 24 */
#define KEY_F13		205            /* 25 */
#define KEY_F14		206            /* 26 */
#define KEY_207		207            /* 27 */
#define KEY_F15		208            /* 28 */
#define KEY_F16		209            /* 29 */
#define KEY_210		210            /* 30 */
#define KEY_F17		211            /* 31 */
#define KEY_F18		212            /* 32 */
#define KEY_F19		213            /* 33 */
#define KEY_F20		214            /* 34 */
#define KEY_NUM		215
#endif /* __KEYS_H__ */
/* 
 * history.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: history.h,v 1.7 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: history.h,v $
 * Revision 1.7  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#ifndef __HISTORY_H__
#define __HISTORY_H__

typedef struct {
  int valid;
  time_t t;
  CRT_CA line[CRT_COLS];
} History_ent;

typedef struct {
  History_ent *lines;
  int nlines;
  int wptr;
} History;

#define HISTORY_INC(h,a) do { (a)++; if ((a)==((h)->nlines)) { (a)=0; }; } while (0)

#endif /* __HISTORY_H__ */
/* 
 * ring.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: ring.h,v 1.6 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: ring.h,v $
 * Revision 1.6  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#ifndef __RING_H__
#define __RING_H__

typedef struct {
  uint8_t *ring;
  int wptr;
  int rptr;
  int size;
} Ring;

#define RING_NEXT(r,a) (((a)+1) % ((r)->size))
#define RING_NEXT_R(r) RING_NEXT(r,r->rptr)
#define RING_NEXT_W(r) RING_NEXT(r,r->wptr)

#define RING_EMPTY(r) (((r)->wptr) == ((r)->rptr))
#define RING_FULL(r) (RING_NEXT_W(r) == ((r)->rptr))

static inline int
ring_write_one (Ring * r, uint8_t * c)
{
  if (RING_FULL (r))
    return 0;

  r->ring[r->wptr++] = *c;

  if (r->wptr == r->size)
    r->wptr = 0;

  return 1;
}

static inline int
ring_read_one (Ring * r, uint8_t * c)
{
  if (RING_EMPTY (r))
    return 0;

  *c = r->ring[r->rptr++];

  if (r->rptr == r->size)
    r->rptr = 0;

  return 1;
}



#endif /* __RING_H__ */
/* 
 * ring.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: slide.h,v 1.5 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: slide.h,v $
 * Revision 1.5  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 */

#ifndef __SLIDE_H__
#define __SLIDE_H__

typedef struct {
  uint8_t *slide;
  int nbytes;
  int target_size;
  int size;
} Slide;

#define SLIDE_FULL(s) ((s)->nbytes==(s)->size)
#define SLIDE_EMPTY(s) (!((s)->nbytes))
#define SLIDE_SPACE(s) (((s)->size)-((s)->nbytes))
#define SLIDE_BYTES(s) ((s)->nbytes)
#define SLIDE_RPTR(s) ((s)->slide)
#define SLIDE_WPTR(s) (((s)->slide)+((s)->nbytes))

#endif /* __SLIDE_H__ */
/* 
 * log.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: log.h,v 1.7 2010/07/27 14:49:35 james Exp $
 */

/* 
 * $Log: log.h,v $
 * Revision 1.7  2010/07/27 14:49:35  james
 * add support for byte logging
 *
 * Revision 1.6  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 12:14:50  james
 * *** empty log message ***
 *
 */

#ifndef __LOG_H__
#define __LOG_H__

#define LOG_SIGNATURE \
	struct Log_struct *next; \
	void (*log)(struct Log_struct *,char *); \
	void (*log_bytes)(struct Log_struct *,void *,int); \
	void (*sighup)(struct Log_struct *); \
	void (*close)(struct Log_struct *)

typedef struct Log_struct {
  LOG_SIGNATURE;
} Log;


#endif /* __LOG_H__ */
/* 
 * ipc.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: ipc.h,v 1.12 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: ipc.h,v $
 * Revision 1.12  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 12:17:42  james
 * *** empty log message ***
 *
 */

#ifndef __IPC_H__
#define __IPC_H__

#define IPC_MAX_BUF 1024

#define IPC_MSG_TYPE_NOOP  0
#define IPC_MSG_TYPE_DEBUG 1
#define IPC_MSG_TYPE_INITIALIZE 2
#define IPC_MSG_TYPE_VT102 3
#define IPC_MSG_TYPE_HISTORY 4
#define IPC_MSG_TYPE_KEY 5
#define IPC_MSG_TYPE_TERM 6
#define IPC_MSG_TYPE_STATUS 7
#define IPC_MSG_TYPE_SETBAUD 8
#define IPC_MSG_TYPE_SENDBREAK 9
#define IPC_MSG_TYPE_SETFLOW 10
#define IPC_MSG_TYPE_SETANSI 11
#define IPC_MSG_TYPE_HANGUP 12
#define IPC_MSG_TYPE_SETSIZE 13
#define IPC_MSG_TYPE_RESET 14
#define IPC_MSG_TYPE_KILLME 15

typedef struct {
  int32_t size;
  int32_t type;
  uint8_t payload[0];
} IPC_Msg_hdr;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_noop;


typedef struct {
  int32_t size;
  int32_t type;
  char msg[0];
} IPC_Msg_debug;

typedef struct {
  int32_t size;
  int32_t type;
  char msg[0];
} IPC_Msg_initialize;

typedef struct {
  int32_t size;
  int32_t type;
  History_ent history;
} IPC_Msg_history;

typedef struct {
  int32_t size;
  int32_t type;
  int32_t len;
  VT102 vt102;
} IPC_Msg_VT102;


typedef struct {
  int32_t size;
  int32_t type;
  int32_t key;
} IPC_Msg_key;

typedef struct {
  int32_t size;
  int32_t type;
  int32_t len;
  uint8_t term[0];
} IPC_Msg_term;


typedef struct {
  int32_t size;
  int32_t type;
  char status[0];
} IPC_Msg_status;

typedef struct {
  int32_t size;
  int32_t type;
  int32_t baud;
} IPC_Msg_setbaud;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_sendbreak;


typedef struct {
  int32_t size;
  int32_t type;
  int32_t flow;
} IPC_Msg_setflow;


typedef struct {
  int32_t size;
  int32_t type;
  int32_t ansi;
} IPC_Msg_setansi;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_hangup;


typedef struct {
  int32_t size;
  int32_t type;
  CRT_Pos winsize;
} IPC_Msg_setsize;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_reset;


typedef struct {
  int32_t size;
  int32_t type;
} IPC_Msg_killme;



typedef union {
  IPC_Msg_hdr hdr;
  IPC_Msg_noop noop;
  IPC_Msg_debug debug;
  IPC_Msg_initialize initialize;
  IPC_Msg_history history;
  IPC_Msg_VT102 vt102;
  IPC_Msg_key key;
  IPC_Msg_term term;
  IPC_Msg_status status;
  IPC_Msg_setbaud setbaud;
  IPC_Msg_sendbreak sendbreak;
  IPC_Msg_setflow setflow;
  IPC_Msg_setansi setansi;
  IPC_Msg_hangup hangup;
  IPC_Msg_setsize setsize;
  IPC_Msg_reset reset;
  IPC_Msg_killme killme;
} IPC_Msg;



#endif /* __IPC_H__ */
/* 
 * symsocket.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: symsocket.h,v 1.6 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: symsocket.h,v $
 * Revision 1.6  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/20 18:31:53  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 18:05:06  james
 * *** empty log message ***
 *
 */

#ifndef __SYMSOCKET_H__
#define __SYMSOCKET_H__

typedef struct {
  int fd;

  Slide *read_buf;
  Slide *write_buf;

  IPC_Msg *msg;

  char *path_to_unlink;

} Socket;

#define SOCKET_IS_LISTENER(s) (!((s)->read_buf))

#endif /* __SYMSOCKET_H__ */
/* 
 * keydis.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: keydis.h,v 1.10 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: keydis.h,v $
 * Revision 1.10  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 */

#ifndef __KEYDIS_H__
#define __KEYDIS_H__


struct Context_struct;

#define KEYDIS_SIGNATURE \
	void (*close)(struct KeyDis_struct *); \
	int (*key)(struct KeyDis_struct *,struct Context_struct *,int key); \
	int (*set_baud)(struct KeyDis_struct *,struct Context_struct *,int rate); \
	int (*send_break)(struct KeyDis_struct *,struct Context_struct *); \
	int (*set_flow)(struct KeyDis_struct *,struct Context_struct *,int flow); \
	int (*set_ansi)(struct KeyDis_struct *,struct Context_struct *,int ansi); \
	int (*hangup)(struct KeyDis_struct *,struct Context_struct *); \
	int (*reset)(struct KeyDis_struct *,struct Context_struct *); \
	int (*set_size)(struct KeyDis_struct *,struct Context_struct *,int width, int height)



typedef struct KeyDis_struct {
  KEYDIS_SIGNATURE;
} KeyDis;


#endif /* __KEYDIS_H__ */
/* 
 * cmd.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: cmd.h,v 1.8 2008/03/10 11:49:32 james Exp $
 */

/* 
 * $Log: cmd.h,v $
 * Revision 1.8  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/02 12:30:54  staffcvs
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/15 15:14:19  james
 * *** empty log message ***
 *
 */

#ifndef __CMD_H__
#define __CMD_H__

#define CMD_KEY 2               /* CTRL B */
#define CMD_CANCEL_KEY 3        /* CTRL C */

typedef struct {
  int active;
  int error;
  int disconnect;
  char csl[128];
  char buf[128];
  int ptr;
} Cmd;

#endif /* __CMD_H__ */
/* 
 * lockfile.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: lockfile.h,v 1.11 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: lockfile.h,v $
 * Revision 1.11  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/15 20:52:36  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/15 19:51:30  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/15 18:16:36  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/15 16:48:56  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/15 15:09:17  james
 * *** empty log message ***
 *
 */

#ifndef __LOCKFILE_H__
#define __LOCKFILE_H__

#define SERIAL_LOCK_PASSIVE 0
#define SERIAL_LOCK_ACTIVE  1

#define FILE_LIST_MAX_LEN 1024

typedef struct Filelist_ent {
  char name[FILE_LIST_MAX_LEN];
  struct Filelist_ent *next;
} Filelist_ent;

typedef struct {
  Filelist_ent *head;
} Filelist;


typedef struct {
  int mode;
  int i;
  struct timeval last_stale_purge;
  Filelist *locks_to_check;
  Filelist *locks_held;
} Serial_lock;



#endif /* __LOCKFILE_H__ */
/* 
 * rx.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: rx.h,v 1.5 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: rx.h,v $
 * Revision 1.5  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/03/06 15:17:26  james
 * *** empty log message ***
 *
 *
 */

#ifndef __RX_H__
#define __RX_H__
#define RX_SIGNATURE \
  int (*rx)(struct RX_struct *,int); \
  void (*close)(struct RX_struct *);


typedef struct RX_struct {
  RX_SIGNATURE;
} RX;


#endif /* __RX_H__ */
/* 
 * context.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: context.h,v 1.14 2010/07/27 14:49:35 james Exp $
 */

/* 
 * $Log: context.h,v $
 * Revision 1.14  2010/07/27 14:49:35  james
 * add support for byte logging
 *
 * Revision 1.13  2008/03/10 11:49:32  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/14 02:46:44  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/12 22:36:46  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/09 15:47:28  james
 * *** empty log message ***
 *
 */

#ifndef __CONTEXT_H__
#define __CONTEXT_H__

typedef struct Context_struct {
  VT102 *v;
  TTY *t;
  TTY_Parser *tp;
  History *h;
  Log *l;
  KeyDis *k;
  Cmd *d;
  UTF8 *u;
  RX *r;
  int byte_logging;
} Context;

#endif /* __CONTEXT_H__ */
/* 
 * vt102_charset.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: vt102_charset.h,v 1.2 2008/03/07 12:37:04 james Exp $
 */

/* 
 * $Log: vt102_charset.h,v $
 * Revision 1.2  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/27 01:32:41  james
 * *** empty log message ***
 *
 */

#ifndef __VT102_CHARSET_H__
#define __VT102_CHARSET_H__

#define VT102_CHARSET_SIZE 128

#define VT102_CSID_US	0
#define VT102_CSID_UK	1
#define VT102_CSID_GL	2
#define VT102_CSID_VT52	3

#endif /* __VT102_CHARSET_H__ */
/* ansi.c */
extern ANSI *ansi_new_from_terminal (TTY * t, int utf8);
/* crt.c */
extern void crt_erase (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color);
extern void crt_cls (CRT * c);
extern void crt_scroll_up (CRT * c, CRT_Pos s, CRT_Pos e, int ea, int color);
extern void crt_scroll_down (CRT * c, CRT_Pos s, CRT_Pos e, int ea,
                             int color);
extern void crt_reset (CRT * c);
extern void crt_insert (CRT * c, CRT_CA ca);
/* html.c */
extern ANSI *ansi_new_html (FILE * f);
/* libsympathy.c */
/* render.c */
/* version.c */
extern char *libsympathy_version (void);
/* vt102.c */
extern int vt102_cmd_length[128];
extern int vt102_cmd_termination[128];
extern void vt102_crt_update (Context * c);
extern void vt102_do_resize (Context * c);
extern void vt102_log_line (Context * c, int line);
extern void vt102_history (Context * c, CRT_Pos t, CRT_Pos b);
extern void vt102_clip_cursor (VT102 * v, CRT_Pos tl, CRT_Pos br);
extern void vt102_cursor_normalize (VT102 * v);
extern void vt102_cursor_carriage_return (VT102 * v);
extern void vt102_cursor_advance_line (Context * c);
extern void vt102_cursor_retreat_line (Context * c);
extern void vt102_do_pending_wrap (Context * c);
extern void vt102_cursor_advance (Context * c);
extern void vt102_cursor_retreat (VT102 * v);
extern void vt102_reset_tabs (VT102 * v);
extern void vt102_cursor_advance_tab (VT102 * v);
extern void vt102_cursor_retreat_tab (VT102 * v);
extern int vt102_cursor_home (VT102 * v);
extern int vt102_cursor_absolute (VT102 * v, int x, int y);
extern int vt102_cursor_relative (VT102 * v, int x, int y);
extern void vt102_delete_from_line (VT102 * v, CRT_Pos p);
extern void vt102_insert_into_line (VT102 * v, CRT_Pos p);
extern void vt102_change_mode (Context * c, int private, char *ns, int set);
extern void vt102_parse_mode_string (Context * c, char *buf, int len);
extern void vt102_change_attr (VT102 * v, char *na);
extern void vt102_parse_attr_string (VT102 * v, char *buf, int len);
extern void vt102_save_state (VT102 * v);
extern void vt102_restore_state (VT102 * v);
extern void vt102_regular_char (Context * c, VT102 * v, uint32_t ch);
extern int vt102_send_id (Context * c, char *buf);
extern void vt102_scs (Context * c, int g, int s);
extern void vt102_status_line (VT102 * v, char *str);
extern void vt102_parser_reset (VT102_parser * p);
extern void vt102_reset_state (Context * c);
extern int vt102_rx_hook (Context * c, int ch);
extern int vt102_parse_char (Context * c, int ch);
extern void vt102_send (Context * c, uint8_t key);
extern void vt102_reset (Context * c);
extern VT102 *vt102_new (CRT_Pos * size);
extern void vt102_set_ansi (VT102 * v, int ansi);
extern void vt102_resize (Context * c, CRT_Pos size);
extern void vt102_free (VT102 * v);
/* tty.c */
extern void tty_pre_select (TTY * t, fd_set * rfds, fd_set * wfds);
extern int tty_get_status (TTY * t, TTY_Status * s);
extern int tty_get_baud (TTY * t);
extern void tty_set_baud (TTY * t, int rate);
extern void tty_send_break (TTY * t);
extern void tty_set_flow (TTY * t, int flow);
extern void tty_hangup (TTY * t);
extern void tty_length (TTY * t, int l);
extern void tty_winch (TTY * t, CRT_Pos size);
extern void tty_parse_reset (Context * c);
extern void tty_analyse (Context * c);
extern TTY_Parser *tty_parser_new (void);
extern int tty_parse (Context * c, uint8_t * buf, int len);
/* keydis.c */
extern KeyDis *keydis_vt102_new (void);
extern KeyDis *keydis_ipc_new (Socket * s);
/* history.c */
extern History *history_new (int n);
extern void history_free (History * h);
extern void history_add (History * h, CRT_CA * c);
/* ring.c */
extern int ring_read (Ring * r, void *b, int n);
extern int ring_write (Ring * r, void *b, int n);
extern int ring_space (Ring * r);
extern int ring_bytes (Ring * r);
extern Ring *ring_new (int n);
/* ptty.c */
extern TTY *ptty_open (char *path, char *argv[], CRT_Pos * size);
/* terminal.c */
extern int terminal_winches;
extern void terminal_atexit (void);
extern void terminal_getsize (TTY * _t);
extern void terminal_dispatch (void);
extern void terminal_register_handlers (void);
extern TTY *terminal_open (int rfd, int wfd);
/* util.c */
extern int wrap_read (int fd, void *buf, int len);
extern int wrap_write (int fd, void *buf, int len);
extern void set_nonblocking (int fd);
extern void set_blocking (int fd);
extern void default_termios (struct termios *termios);
extern void client_termios (struct termios *termios);
extern int fput_cp (FILE * f, uint32_t ch);
extern void crash_out (char *why);
extern void *xmalloc (size_t s);
extern void *xrealloc (void *p, size_t s);
extern char *xstrdup (const char *s);
/* log.c */
extern Log *file_log_new (char *fn, int rotate);
extern void log_f (Log * log, char *fmt, ...);
/* ipc.c */
extern IPC_Msg *ipc_check_for_message_in_slide (Slide * s);
extern void ipc_consume_message_in_slide (Slide * s);
extern int ipc_msg_send (Socket * s, IPC_Msg * m);
extern int ipc_msg_send_debug (Socket * s, char *msg);
extern int ipc_msg_send_initialize (Socket * s);
extern int ipc_msg_send_history (Socket * s, History_ent * l);
extern int ipc_msg_send_vt102 (Socket * s, VT102 * v);
extern int ipc_msg_send_key (Socket * s, int key);
extern int ipc_msg_send_term (Socket * s, void *buf, int len);
extern int ipc_msg_send_status (Socket * s, char *buf);
extern int ipc_msg_send_setbaud (Socket * s, int baud);
extern int ipc_msg_send_sendbreak (Socket * s);
extern int ipc_msg_send_setflow (Socket * s, int flow);
extern int ipc_msg_send_setansi (Socket * s, int ansi);
extern int ipc_msg_send_hangup (Socket * s);
extern int ipc_msg_send_setsize (Socket * s, CRT_Pos size);
extern int ipc_msg_send_reset (Socket * s);
/* slide.c */
extern void slide_free (Slide * s);
extern void slide_consume (Slide * s, int n);
extern void slide_added (Slide * s, int n);
extern Slide *slide_new (int n);
extern void slide_expand (Slide * s, int n);
/* symsocket.c */
extern int wrap_recv (int fd, void *buf, int len);
extern int wrap_send (int fd, void *buf, int len);
extern void socket_free (Socket * s);
extern void socket_free_parent (Socket * s);
extern Socket *socket_listen (char *path);
extern Socket *socket_accept (Socket * l);
extern Socket *socket_connect (char *path);
extern void socket_consume_msg (Socket * s);
extern void socket_pre_select (Socket * s, fd_set * rfds, fd_set * wfds);
extern int socket_post_select (Socket * s, fd_set * rfds, fd_set * wfds);
extern int socket_write (Socket * s, void *buf, int len);
/* serial.c */
extern TTY *serial_open (char *path, int lock_mode);
/* cmd.c */
extern int cmd_parse (Cmd * c, Context * ctx, ANSI * a, char *buf);
extern void cmd_show_status (Cmd * c, Context * ctx);
extern int cmd_key (Cmd * c, Context * ctx, ANSI * a, int key);
extern int cmd_deactivate (Cmd * c, Context * ctx);
extern int cmd_activate (Cmd * c, Context * ctx);
extern void cmd_new_status (Cmd * c, Context * ctx, char *msg);
extern Cmd *cmd_new (void);
/* lockfile.c */
extern Filelist *filelist_new (void);
extern void filelist_remove (Filelist * fl, Filelist_ent * fle);
extern void filelist_add (Filelist * fl, char *fn);
extern void filelist_free (Filelist * fl);
extern void filelist_print (Filelist * fl, FILE * f);
extern int lockfile_make (char *name);
extern void lockfile_add_places (Filelist * fl, char *leaf);
extern void lockfile_regularize_and_add (Filelist * fl, char *leaf);
extern void lockfile_add_name_from_path (Filelist * fl, char *file);
extern void lockfile_add_name_from_dev (Filelist * fl, dev_t dev);
extern void lockfile_check_dir_for_dev (Filelist * fl, char *dir, dev_t dev);
extern Filelist *lockfile_make_list (char *device);
extern void lockfile_remove_stale (Filelist * fl);
extern Filelist *lockfile_lock (Filelist * fl);
extern void lockfile_unlock (Filelist * fl);
extern int serial_lock_check (Serial_lock * l);
extern void serial_lock_free (Serial_lock * l);
extern Serial_lock *serial_lock_new (char *dev, int mode);
/* utf8.c */
extern int utf8_flush (Context * c);
extern int utf8_parse (Context * c, uint32_t ch);
extern UTF8 *utf8_new (void);
extern int utf8_encode (char *ptr, int ch);
extern int utf8_emit (TTY * t, int ch);
/* vt102_charset.c */
extern uint32_t vt102_charset_c0[128];
extern uint32_t vt102_charset_us[128];
extern uint32_t vt102_charset_uk[128];
extern uint32_t vt102_charset_vt52[128];
extern uint32_t vt102_charset_gl[128];
extern uint32_t *charset_from_csid[];
/* rotate.c */
extern void rotate_gzip (char *file);
extern void rotate (char *file);
extern int rotate_check (char *file);
/* raw.c */
extern RX *rx_new_raw (int rfd, int wfd);
extern TTY *terminal_new_raw (int rfd, int wfd);
extern ANSI *ansi_new_raw (int rfd, int wfd);
#ifdef __cplusplus
}
#endif

#endif /* __SYMPATHY_H__ */

/* 
 * symsocket.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: symsocket.h,v 1.6 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: symsocket.h,v $
 * Revision 1.6  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/20 18:31:53  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 18:05:06  james
 * *** empty log message ***
 *
 */

#ifndef __SYMSOCKET_H__
#define __SYMSOCKET_H__

typedef struct {
  int fd;

  Slide *read_buf;
  Slide *write_buf;

  IPC_Msg *msg;

  char *path_to_unlink;

} Socket;

#define SOCKET_IS_LISTENER(s) (!((s)->read_buf))

#endif /* __SYMSOCKET_H__ */

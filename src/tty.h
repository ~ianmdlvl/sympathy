/* 
 * tty.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: tty.h,v 1.16 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: tty.h,v $
 * Revision 1.16  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/22 19:12:05  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/15 23:52:12  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/14 10:36:18  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/14 10:34:30  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/13 01:08:18  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/09 15:47:28  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/07 00:43:27  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/06 20:26:58  james
 * *** empty log message ***
 *
 */

#ifndef __TTY_H__
#define __TTY_H__


#define SYM_CHAR_RESET 	(-1)

#define TTY_SIGNATURE \
	char name[1024]; \
        int blocked; \
	CRT_Pos size; \
	void (*close)(struct TTY_struct *); \
	int (*recv)(struct TTY_struct *,void *buf,int len); \
	int (*xmit)(struct TTY_struct *,void *buf,int len); \
	int rfd; \
	int wfd; \
	int hanging_up; \
	struct timeval hangup_clock; \
	int displayed_length;


#define TTY_BITFREQ_LEN	10

typedef struct {
  int in_dle;
  int in_errmark;

  int bitfreq[TTY_BITFREQ_LEN];
  int biterrs;

  struct timeval lasterr;
  int guessed_baud;
} TTY_Parser;


typedef struct TTY_struct {
  TTY_SIGNATURE;
} TTY;

typedef struct {
  int lines;
  int blocked;
  struct termios termios;
  int baud;
} TTY_Status;

#endif /* __TTY_H__ */

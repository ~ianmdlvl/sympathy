/* 
 * utf8.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: utf8.h,v 1.8 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: utf8.h,v $
 * Revision 1.8  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/22 19:12:05  james
 * *** empty log message ***
 *
 *
 */

#ifndef __UTF8_H__
#define __UTF8_H__


typedef struct {
  int in_utf8;

  uint8_t utf_buf[4];
  int utf_ptr;

  uint32_t ch;
  int sh;
} UTF8;

#endif /* __UTF8_H__ */

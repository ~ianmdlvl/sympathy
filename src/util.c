/* 
 * util.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: util.c,v 1.11 2008/03/07 14:13:40 james Exp $";

/* 
 * $Log: util.c,v $
 * Revision 1.11  2008/03/07 14:13:40  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/03/02 10:50:32  staffcvs
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/27 01:31:14  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/27 00:54:16  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/23 13:05:58  staffcvs
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/13 01:08:38  james
 * *** empty log message ***
 *
 */

#include "project.h"

int
wrap_read (int fd, void *buf, int len)
{
  int red;

  red = read (fd, buf, len);
#if 0
  if (!red)
    return -1;
#endif

  if ((red < 0) && (errno == EAGAIN))
    red = 0;

  return red;
}

int
wrap_write (int fd, void *buf, int len)
{
  int writ;

  errno = 0;

  writ = write (fd, buf, len);

  if (!writ)
    return -1;

  if ((writ < 0) && (errno == EAGAIN))
    writ = 0;

  return writ;
}


void
set_nonblocking (int fd)
{
  long arg;
  arg = fcntl (fd, F_GETFL, arg);
  arg |= O_NONBLOCK;
  fcntl (fd, F_SETFL, arg);
}

void
set_blocking (int fd)
{
  long arg;
  arg = fcntl (fd, F_GETFL, arg);
  arg &= ~O_NONBLOCK;
  fcntl (fd, F_SETFL, arg);
}



void
default_termios (struct termios *termios)
{
  termios->c_iflag = PARMRK | INPCK;
  termios->c_oflag = NL0 | CR0 | TAB0 | BS0 | VT0 | FF0;
  termios->c_lflag = 0;
  termios->c_cflag = CS8 | CREAD | CLOCAL;

  termios->c_cc[VINTR] = 003;
  termios->c_cc[VQUIT] = 034;
  termios->c_cc[VERASE] = 0177;
  termios->c_cc[VKILL] = 025;
  termios->c_cc[VEOF] = 004;
  termios->c_cc[VEOL] = 0;
  termios->c_cc[VEOL2] = 0;
  termios->c_cc[VSTART] = 021;
  termios->c_cc[VSTOP] = 023;
  termios->c_cc[VSUSP] = 032;
  termios->c_cc[VLNEXT] = 026;
  termios->c_cc[VWERASE] = 027;
  termios->c_cc[VREPRINT] = 022;
  termios->c_cc[VDISCARD] = 017;


}

void
client_termios (struct termios *termios)
{
  memset (termios, 0, sizeof (termios));

  termios->c_iflag = ICRNL | IXON | PARMRK | INPCK;
  termios->c_oflag = OPOST | ONLCR | NL0 | CR0 | TAB0 | BS0 | VT0 | FF0;
  termios->c_lflag =
    ISIG | ICANON | IEXTEN | ECHO | ECHOE | ECHOK | ECHOCTL | ECHOKE;
  termios->c_cflag = CS8 | CREAD | CLOCAL;

  termios->c_cc[VINTR] = 003;
  termios->c_cc[VQUIT] = 034;
  termios->c_cc[VERASE] = 0177;
  termios->c_cc[VKILL] = 025;
  termios->c_cc[VEOF] = 004;
  termios->c_cc[VEOL] = 0;
  termios->c_cc[VEOL2] = 0;
  termios->c_cc[VSTART] = 021;
  termios->c_cc[VSTOP] = 023;
  termios->c_cc[VSUSP] = 032;
  termios->c_cc[VLNEXT] = 026;
  termios->c_cc[VWERASE] = 027;
  termios->c_cc[VREPRINT] = 022;
  termios->c_cc[VDISCARD] = 017;


  cfsetispeed (termios, B9600);
  cfsetospeed (termios, B9600);
}

int
fput_cp (FILE * f, uint32_t ch)
{
  char buf[4];
  int i;
  i = utf8_encode (buf, ch);

  if (!i)
    return 0;

  return fwrite (buf, i, 1, f);
}

void
crash_out (char *why)
{
  terminal_atexit ();
  fprintf (stderr, "sympathy is aborting: %s\n", why ? why : "");
  exit (1);
}

void *
xmalloc (size_t s)
{
  void *ret = malloc (s);
  if (!ret)
    crash_out ("malloc failed");
  return ret;
}

void *
xrealloc (void *p, size_t s)
{
  p = realloc (p, s);
  if (!p)
    crash_out ("realloc failed");
  return p;
}

char *
xstrdup (const char *s)
{
  char *ret = strdup (s);
  if (!ret)
    crash_out ("strdup failed");
  return ret;
}

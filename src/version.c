/* 
 * version.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: version.c,v 1.4 2008/03/07 12:37:04 james Exp $";

/* 
 * $Log: version.c,v $
 * Revision 1.4  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 16:20:24  james
 * *** empty log message ***
 *
 *
 */

#include "version.h"

char *
libsympathy_version (void)
{
  return VERSION;
}

/* 
 * vt102.c:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

static char rcsid[] = "$Id: vt102.c,v 1.67 2010/07/27 14:49:35 james Exp $";

/* 
 * $Log: vt102.c,v $
 * Revision 1.67  2010/07/27 14:49:35  james
 * add support for byte logging
 *
 * Revision 1.66  2008/03/07 13:16:02  james
 * *** empty log message ***
 *
 * Revision 1.65  2008/03/07 12:42:08  james
 * *** empty log message ***
 *
 * Revision 1.64  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.63  2008/03/07 12:00:58  james
 * *** empty log message ***
 *
 * Revision 1.62  2008/03/06 21:34:09  james
 * *** empty log message ***
 *
 * Revision 1.61  2008/03/06 17:21:41  james
 * *** empty log message ***
 *
 * Revision 1.60  2008/03/06 16:49:39  james
 * *** empty log message ***
 *
 * Revision 1.59  2008/03/06 16:49:05  james
 * *** empty log message ***
 *
 * Revision 1.58  2008/03/06 01:41:48  james
 * *** empty log message ***
 *
 * Revision 1.57  2008/03/03 06:20:14  james
 * *** empty log message ***
 *
 * Revision 1.56  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.55  2008/03/02 12:32:57  staffcvs
 * *** empty log message ***
 *
 * Revision 1.54  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.53  2008/02/29 18:33:39  james
 * *** empty log message ***
 *
 * Revision 1.52  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.51  2008/02/28 16:37:16  james
 * *** empty log message ***
 *
 * Revision 1.50  2008/02/28 12:12:25  james
 * *** empty log message ***
 *
 * Revision 1.49  2008/02/28 11:27:48  james
 * *** empty log message ***
 *
 * Revision 1.48  2008/02/27 09:42:53  james
 * *** empty log message ***
 *
 * Revision 1.47  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.46  2008/02/27 01:31:38  james
 * *** empty log message ***
 *
 * Revision 1.45  2008/02/27 01:31:14  james
 * *** empty log message ***
 *
 * Revision 1.44  2008/02/27 00:54:16  james
 * *** empty log message ***
 *
 * Revision 1.43  2008/02/27 00:27:22  james
 * *** empty log message ***
 *
 * Revision 1.42  2008/02/26 23:56:12  james
 * *** empty log message ***
 *
 * Revision 1.41  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.40  2008/02/26 19:00:59  james
 * *** empty log message ***
 *
 * Revision 1.39  2008/02/26 16:54:06  james
 * *** empty log message ***
 *
 * Revision 1.38  2008/02/26 16:53:24  james
 * *** empty log message ***
 *
 * Revision 1.37  2008/02/24 12:22:53  james
 * *** empty log message ***
 *
 * Revision 1.36  2008/02/24 12:22:42  james
 * *** empty log message ***
 *
 * Revision 1.35  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.34  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.33  2008/02/22 23:39:27  james
 * *** empty log message ***
 *
 * Revision 1.32  2008/02/22 19:12:05  james
 * *** empty log message ***
 *
 * Revision 1.31  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.30  2008/02/22 14:51:54  james
 * *** empty log message ***
 *
 * Revision 1.29  2008/02/15 03:32:07  james
 * *** empty log message ***
 *
 * Revision 1.28  2008/02/14 10:34:30  james
 * *** empty log message ***
 *
 * Revision 1.27  2008/02/14 02:46:45  james
 * *** empty log message ***
 *
 * Revision 1.26  2008/02/14 01:55:57  james
 * *** empty log message ***
 *
 * Revision 1.25  2008/02/13 16:57:29  james
 * *** empty log message ***
 *
 * Revision 1.24  2008/02/13 09:12:21  james
 * *** empty log message ***
 *
 * Revision 1.23  2008/02/07 13:26:35  james
 * *** empty log message ***
 *
 * Revision 1.22  2008/02/07 13:22:51  james
 * *** empty log message ***
 *
 * Revision 1.21  2008/02/07 12:21:16  james
 * *** empty log message ***
 *
 * Revision 1.20  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/02/07 11:27:02  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/02/07 01:59:25  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/02/07 01:58:28  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/02/07 01:57:46  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/07 00:43:27  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/07 00:40:23  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/07 00:39:59  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/06 20:26:58  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/06 17:53:28  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/06 15:53:22  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/06 11:49:47  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/05 01:11:46  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/04 05:45:55  james
 * ::
 *
 * Revision 1.3  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 01:32:39  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:36:41  james
 * *** empty log message ***
 *
 */


/* 
 * Termcap he say:
 * 
 * VT102:
 * 
 * vt102|dec vt102:\ :mi:\
 * :al=\E[L:dc=\E[P:dl=\E[M:ei=\E[4l:im=\E[4h:tc=vt100:
 * 
 * vt100|vt100-am|dec vt100 (w/advanced video):\ :am:bs:ms:xn:xo:\
 * :co#80:it#8:li#24:vt#3:\
 * :DO=\E[%dB:LE=\E[%dD:RA=\E[?7l:RI=\E[%dC:SA=\E[?7h:\ :UP=\E[%dA:\
 * :ac=``aaffggjjkkllmmnnooppqqrrssttuuvvwwxxyyzz{{||}}~~:\
 * :ae=^O:as=^N:bl=^G:cb=\E[1K:cd=\E[J:ce=\E[K:cl=\E[H\E[J:\
 * :cm=\E[%i%d;%dH:cr=^M:cs=\E[%i%d;%dr:ct=\E[3g:do=^J:\
 * :eA=\E(B\E)0:ho=\E[H:kb=^H:kd=\EOB:ke=\E[?1l\E>:kl=\EOD:\
 * :kr=\EOC:ks=\E[?1h\E=:ku=\EOA:le=^H:mb=\E[5m:md=\E[1m:\
 * :me=\E[m\017:mr=\E[7m:nd=\E[C:rc=\E8:\
 * :rs=\E>\E[?3l\E[?4l\E[?5l\E[?7h\E[?8h:sc=\E7:se=\E[m:\
 * :sf=^J:so=\E[7m:sr=\EM:st=\EH:ta=^I:ue=\E[m:up=\E[A:\
 * :us=\E[4m:tc=vt100+fnkeys:
 * 
 * vt100+fnkeys|dec vt100 numeric keypad:\
 * :k0=\EOy:k5=\EOt:k6=\EOu:k7=\EOv:k8=\EOl:k9=\EOw:k;=\EOx:\
 * :tc=vt100+pfkeys:
 * 
 * vt100+pfkeys|dec vt100 numeric keypad:\
 * :@8=\EOM:k1=\EOP:k2=\EOQ:k3=\EOR:k4=\EOS:tc=vt100+keypad:
 * 
 * vt100+keypad|dec vt100 numeric keypad no fkeys:\
 * :K1=\EOq:K2=\EOr:K3=\EOs:K4=\EOp:K5=\EOn:
 * 
 */

/* 
 * so the parser needs to be able to at least do CTRL-G CTRL-H CTRL-I
 * CTRL-J CTRL-M CTRL-N
 * 
 * CTRL-O ESC7 ESC8 ESCH ESCM ESC>
 * 
 * ESC[%dA ESC[%dB ESC[%dC ESC[%dD ESC[H ESC[%d;%dH ESC[J ESC[K ESC[1K
 * ESC[L ESC[M ESC[P
 * 
 * ESC[3g ESC[4h ESC[4l ESC[m ESC[1m ESC[4m ESC[5m ESC[7m ESC[%d;%dr
 * 
 * 
 * ESC[?3l ESC[?4l ESC[?5l ESC[?7h ESC[?7h ESC[?7l ESC[?8h
 * 
 * ESC(B ESC)0
 * 
 * 
 * TODO:
 * 
 * ESC(B ESC)0
 * 
 * CTRL-O
 * 
 * ANSI:
 * 
 * 
 * 
 * ansi|ansi/pc-term compatible with color:\
 * :u6=\E[%i%d;%dR:u7=\E[6n:u9=\E[c:tc=ecma+color:\
 * :tc=klone+sgr:tc=ansi-m: ansi-m|ansi-mono|ANSI X3.64-1979 terminal with 
 * ANSI.SYS compatible attributes:\ :5i:\
 * :AL=\E[%dL:DC=\E[%dP:DL=\E[%dM:DO=\E[%dB:IC=\E[%d@:\
 * :LE=\E[%dD:RI=\E[%dC:SF=\E[%dS:SR=\E[%dT:UP=\E[%dA:\
 * :cb=\E[1K:ch=\E[%i%dG:ct=\E[2g:cv=\E[%i%dd:ec=\E[%dX:ei=:\
 * :im=:kB=\E[Z:kI=\E[L:kb=^H:kd=\E[B:kl=\E[D:kr=\E[C:ku=\E[A:\
 * :nw=\r\E[S:pf=\E[4i:po=\E[5i:s0=\E(B:s1=\E)B:s2=\E*B:\
 * :s3=\E+B:ta=\E[I:tc=pcansi-m: pcansi-m|pcansi-mono|ibm-pc terminal
 * programs claiming to be ansi (mono mode):\ :am:bs:mi:ms:\
 * :co#80:it#8:li#24:\
 * :al=\E[L:bl=^G:bt=\E[Z:cd=\E[J:ce=\E[K:cl=\E[H\E[J:\
 * :cm=\E[%i%d;%dH:cr=^M:ct=\E[2g:dc=\E[P:dl=\E[M:do=\E[B:\
 * :ho=\E[H:kb=^H:kd=\E[B:kh=\E[H:kl=\E[D:kr=\E[C:ku=\E[A:\
 * :le=\E[D:nd=\E[C:sf=^J:st=\EH:ta=^I:up=\E[A:\ :tc=klone+sgr-dumb:
 * klone+sgr-dumb|attribute control for ansi.sys displays (no ESC [ 11
 * m):\ :as=\E[12m:mb=\E[5m:md=\E[1m:me=\E[0;10m:mk=\E[8m:\
 * :mr=\E[7m:se=\E[m:so=\E[7m:ue=\E[m:us=\E[4m:tc=klone+acs:
 * klone+acs|alternate character set for ansi.sys displays:\
 * :ac=+\020,\021-\030.^Y0\333`\004a\261f\370g\361h\260j\331k\277l\332m\300n\305o~p\304q\304r\304s_t\303u\264v\301w\302x\263y\363z\362{\343|\330}\234~\376:\
 * :ae=\E[10m:as=\E[11m: ecma+color|color control for ECMA-48-compatible terminals:\ :Co#8:NC#3:pa#64:\ :AB=\E[4%dm:AF=\E[3%dm:op=\E[39;49m:
 * 
 * 
 * Ignoreing ones in the VT102 spec
 * 
 * ESC[%d@ ESC[I ESC[....R ESC[%dS ESC[%dT ESC[X ESC[Z
 * 
 * ESC[c *ESC[%db ESC[....d ESC[....f ESC[2g ESC[4i ESC[5i ESC[6n
 * 
 * ESC(B ESC)B ESC*B ESC+B
 * 
 * 
 */

#include "project.h"
#include "syslog.h"

#define TABLE_LENGTH 128
#undef DEBUG


static char terminal_id[] = "vt102";

/* 
 * number of aditional chars after \033 ... to complete the escape if it's
 * fixed length 
 */
int vt102_cmd_length[TABLE_LENGTH] = {
  ['('] = 1,
  [')'] = 1,
  ['+'] = 1,
  ['*'] = 1,
  ['%'] = 1,
  ['#'] = 1,
  ['Y'] = 2,
};

/* 
 * method for determining end if if's not fixed length, -ve numbers from
 * #defines, +ve are literals 
 */
#define CSI_ENDER -1
int vt102_cmd_termination[TABLE_LENGTH] = {
  [']'] = 7,
  ['['] = CSI_ENDER,
};





static inline int
safe_ch (int c)
{
  if (c < 32)
    return ' ';
  if (c > 126)
    return ' ';
  return c;
}

static inline int
csi_ender (int c)
{
  if ((c >= 'a') && (c <= 'z'))
    return 1;
  if ((c >= 'A') && (c <= 'Z'))
    return 1;
  if ((c == '@'))
    return 1;
  return 0;
}

static inline int
ctrl_chr (int ch, int term)
{
  if ((term > 0) && (ch == term))
    return 0;
  if (ch == 033)
    return 0;
  if ((ch > 0) && (ch < 040))
    return 1;
  return 0;
}


static inline int
in_margins (VT102 * v, CRT_Pos p)
{
  if (v->pos.x < v->top_margin.x)
    return 0;
  if (v->pos.y < v->top_margin.y)
    return 0;

  if (v->pos.x > v->bottom_margin.x)
    return 0;
  if (v->pos.y > v->bottom_margin.y)
    return 0;

  return 1;
}

void
vt102_crt_update (Context * c)
{
  VT102 *v = c->v;

  v->crt.pos = v->pos;
  v->crt.hide_cursor =
    v->private_modes[VT102_PRIVATE_MODE_SHOW_CURSOR] ? 0 : 1;

  if (v->current_line.y != v->pos.y) {
    vt102_log_line (c, v->current_line.y);
    v->current_line = v->pos;
  }

  if (c->d)
    cmd_show_status (c->d, c);
}

void
vt102_do_resize (Context * c)
{

  VT102 *v = c->v;

  v->crt.size = v->current_size;
  v->crt.size.y++;
  v->screen_end = v->current_size;
  v->screen_end.x--;
  v->screen_end.y--;
  v->top_margin = v->screen_start;
  v->bottom_margin = v->screen_end;
  vt102_cursor_home (v);
  crt_cls (&v->crt);

  if (c->t)
    tty_winch (c->t, v->current_size);

  log_f (c->l, "<size now %dx%d>", v->current_size.x, v->current_size.y);
  vt102_crt_update (c);
}


void
vt102_log_line (Context * c, int line)
{
  CRT_Pos e = { c->v->current_size.x - 1, line };
  CRT_Pos p = { 0, line };
  char logbuf[4 * (VT102_MAX_COLS + 1)], *logptr = logbuf;

  if (!c->l || c->byte_logging)
    return;

  for (; e.x > 0; --e.x) {
    if (c->v->crt.screen[CRT_ADDR_POS (&e)].chr != ' ')
      break;
  }

  for (; p.x <= e.x; ++p.x) {
    int ch = c->v->crt.screen[CRT_ADDR_POS (&p)].chr;
    if (ch < 32)
      ch = ' ';
    logptr += utf8_encode (logptr, ch);
  }
  *logptr = 0;

  c->l->log (c->l, logbuf);
}

/* 
 * Called for every upward scroll with same args 
 */
void
vt102_history (Context * c, CRT_Pos t, CRT_Pos b)
{
  /* 
   * Only log if it scrolls off the top 
   */
  if (t.y)
    return;

  t.x = 0;
  history_add (c->h, &(c->v->crt.screen[CRT_ADDR_POS (&t)]));
}

void
vt102_clip_cursor (VT102 * v, CRT_Pos tl, CRT_Pos br)
{
  if (v->pos.x < tl.x)
    v->pos.x = tl.x;
  if (v->pos.y < tl.y)
    v->pos.y = tl.y;

  if (v->pos.x > br.x)
    v->pos.x = br.x;
  if (v->pos.y > br.y)
    v->pos.y = br.y;
}


void
vt102_cursor_normalize (VT102 * v)
{
  CRT_Pos *top, *bottom;

  if (v->private_modes[VT102_PRIVATE_MODE_ORIGIN_MODE]) {
    vt102_clip_cursor (v, v->top_margin, v->bottom_margin);
  } else {
    vt102_clip_cursor (v, v->screen_start, v->screen_end);
  }
}


void
vt102_cursor_carriage_return (VT102 * v)
{
  v->pos.x = v->top_margin.x;
  v->pending_wrap = 0;
}

void
vt102_cursor_advance_line (Context * c)
{
  VT102 *v = c->v;
  int couldscroll = in_margins (v, v->pos);

  /* have wraped off end of last line in scrolling region */
  /* not necessary, but shuts compiler up */
  if (((v->pos.y == v->bottom_margin.y) || (v->pos.y == v->screen_end.y))
      && (couldscroll)) {
    vt102_log_line (c, v->pos.y);

    vt102_history (c, v->top_margin, v->bottom_margin);

    crt_scroll_up (&v->crt, v->top_margin, v->bottom_margin, 1, v->color);
    return;
  }

  if (v->pos.y != v->screen_end.y)
    v->pos.y++;
  v->pending_wrap = 0;
}


void
vt102_cursor_retreat_line (Context * c)
{
  VT102 *v = c->v;
  int couldscroll = in_margins (v, v->pos);

  /* have wraped off end of first line in scrolling region */
  /* (|| not necessary, but shuts compiler up */
  if (((v->pos.y == v->top_margin.y) || (v->pos.y == v->screen_start.y)) &&
      (couldscroll)) {
    vt102_log_line (c, v->pos.y);

    crt_scroll_down (&v->crt, v->top_margin, v->bottom_margin, 1, v->color);
    return;
  }

  if (v->pos.y != v->screen_start.y)
    v->pos.y--;
  v->pending_wrap = 0;
}


void
vt102_do_pending_wrap (Context * c)
{
  VT102 *v = c->v;
  int couldscroll = in_margins (v, v->pos);
  int autowrap = v->private_modes[VT102_PRIVATE_MODE_AUTO_WRAP];

  if (!v->pending_wrap)
    return;

  /* End of line but no autowrap, nothing to do */
  if (!autowrap)
    return;

  /* End of screen and not allowed to scroll, nothing to do */
  if ((v->pos.y == v->screen_end.y) && (!couldscroll))
    return;

  if (couldscroll) {
    v->pos.x = v->top_margin.x;
  } else {
    v->pos.x = 0;
  }

  vt102_cursor_advance_line (c);
}

void
vt102_cursor_advance (Context * c)
{
  VT102 *v = c->v;

  if (v->pos.x < v->bottom_margin.x) {
    /* Normal advance */
    v->pos.x++;
    v->pending_wrap = 0;
    return;
  }
  v->pending_wrap++;
  if (!c->v->xn_glitch)
    vt102_do_pending_wrap (c);
}



void
vt102_cursor_retreat (VT102 * v)
{
  if (v->pos.x != v->top_margin.x) {
    v->pos.x--;
  }

  v->pending_wrap = 0;
}

void
vt102_reset_tabs (VT102 * v)
{
  int i;

  memset (v->tabs, 0, sizeof (v->tabs));

  for (i = 0; i < VT102_MAX_COLS; i += 8) {
    v->tabs[i]++;
  }
}
void
vt102_cursor_advance_tab (VT102 * v)
{
  if (v->pos.x == v->bottom_margin.x)
    return;
  while (v->pos.x < v->bottom_margin.x) {
    v->pos.x++;
    if (v->tabs[v->pos.x])
      break;
  }
  v->pending_wrap = 0;
}

void
vt102_cursor_retreat_tab (VT102 * v)
{
  if (v->pos.x == v->top_margin.x)
    return;
  while (v->pos.x > v->top_margin.x) {
    v->pos.x--;
    if (v->tabs[v->pos.x])
      break;
  }
  v->pending_wrap = 0;
}

vt102_cursor_home (VT102 * v)
{
  v->pos = v->top_margin;
  vt102_cursor_normalize (v);
  v->pending_wrap = 0;

}

vt102_cursor_absolute (VT102 * v, int x, int y)
{
  if (v->private_modes[VT102_PRIVATE_MODE_ORIGIN_MODE]) {
    v->pos.x = x + v->top_margin.x;
    v->pos.y = y + v->top_margin.y;
  } else {
    v->pos.x = x;
    v->pos.y = y;
  }
  vt102_cursor_normalize (v);
  v->pending_wrap = 0;
}

vt102_cursor_relative (VT102 * v, int x, int y)
{
  v->pos.x += x;
  v->pos.y += y;
  vt102_cursor_normalize (v);
  v->pending_wrap = 0;
}



void
vt102_delete_from_line (VT102 * v, CRT_Pos p)
{
  int n = v->bottom_margin.x - p.x;

  if (n < 0)
    return;

  if (n) {

    memmove (&v->crt.screen[CRT_ADDR_POS (&p)],
             &v->crt.screen[CRT_ADDR_POS (&p) + 1], sizeof (CRT_CA) * n);
  }

  v->crt.screen[CRT_ADDR (p.y, v->bottom_margin.x)].chr = ' ';
  /* But not attr due to vt102 bug */
}

void
vt102_insert_into_line (VT102 * v, CRT_Pos p)
{
  int n = v->bottom_margin.x - p.x;

  if (n < 0)
    return;

  if (n) {

    memmove (&v->crt.screen[CRT_ADDR_POS (&p) + 1],
             &v->crt.screen[CRT_ADDR_POS (&p)], sizeof (CRT_CA) * n);
  }

  v->crt.screen[CRT_ADDR_POS (&p)].chr = ' ';
  v->crt.screen[CRT_ADDR_POS (&p)].attr = CRT_ATTR_NORMAL;
  v->crt.screen[CRT_ADDR_POS (&p)].color = CRT_COLOR_NORMAL;
}



void
vt102_change_mode (Context * c, int private, char *ns, int set)
{
  VT102 *v = c->v;
  int m;


  if (*ns) {
    m = atoi (ns);
  } else {
    m = 1;
  }

  if (m < 0)
    return;
  if (m >= VT102_NMODES)
    return;

  if (private) {
    v->private_modes[m] = set;
    switch (m) {
    case VT102_PRIVATE_MODE_CURSOR_MODE:
      if (v->application_keypad_mode)
        v->private_modes[m] = 0;
      break;
    case VT102_PRIVATE_MODE_ORIGIN_MODE:
      vt102_cursor_home (v);
      break;
    case VT102_PRIVATE_MODE_132COLS:
      v->current_size.x =
        v->
        private_modes[VT102_PRIVATE_MODE_132COLS] ? VT102_COLS_132 :
        VT102_COLS_80;

      vt102_do_resize (c);
      break;
    }

  } else
    v->modes[m] = set;
}

void
vt102_parse_mode_string (Context * c, char *buf, int len)
{
  VT102 *v = c->v;
  int private = 0;
  char last = buf[len - 1];
  char num[4];
  int o;

  memset (num, 0, sizeof (num));
  o = sizeof (num) - 1;

  len--;

  if (*buf == '?') {
    private++;
    buf++;
    len--;
  }

  if (len < 0)
    return;

  while (len--) {
    if (*buf == ';') {
      vt102_change_mode (c, private, &num[o], last == 'h');
      memset (num, 0, sizeof (num));
      o = sizeof (num) - 1;
      buf++;
      continue;
    }

    num[0] = num[1];
    num[1] = num[2];
    num[2] = *buf;

    if (o)
      o--;

    buf++;
  }

  vt102_change_mode (c, private, &num[o], last == 'h');

}


void
vt102_change_attr (VT102 * v, char *na)
{
  int a;


  if (*na) {
    a = atoi (na);
  } else {
    a = 0;
  }

  switch (a) {
  case 0:
    v->attr = CRT_ATTR_NORMAL;
    v->color = CRT_COLOR_NORMAL;
    break;
  case 1:
    v->attr |= CRT_ATTR_BOLD;
    break;
  case 21:
  case 22:
    v->attr &= ~CRT_ATTR_BOLD;
    break;
  case 4:
    v->attr |= CRT_ATTR_UNDERLINE;
    break;
  case 24:
    v->attr &= ~CRT_ATTR_UNDERLINE;
    break;
  case 5:
    v->attr |= CRT_ATTR_BLINK;
    break;
  case 25:
    v->attr &= ~CRT_ATTR_BLINK;
    break;
  case 7:
    v->attr |= CRT_ATTR_REVERSE;
    break;
  case 27:
    v->attr &= ~CRT_ATTR_REVERSE;
    break;
  case 30:
  case 31:
  case 32:
  case 33:
  case 34:
  case 35:
  case 36:
  case 37:
    v->color &= ~CRT_COLOR_FG_MASK;
    v->color |= ((a - 30) << CRT_COLOR_FG_SHIFT) & CRT_COLOR_FG_MASK;
    break;
  case 90:
  case 91:
  case 92:
  case 93:
  case 94:
  case 95:
  case 96:
  case 97:
    v->color &= ~CRT_COLOR_FG_MASK;
    v->color |=
      (((a -
         90) | CRT_COLOR_INTENSITY) << CRT_COLOR_FG_SHIFT) &
      CRT_COLOR_FG_MASK;
    break;
  case 39:
  case 99:
    v->color &= ~CRT_COLOR_FG_MASK;
    v->color |=
      (CRT_FGCOLOR_NORMAL << CRT_COLOR_FG_SHIFT) & CRT_COLOR_FG_MASK;
    break;
  case 40:
  case 41:
  case 42:
  case 43:
  case 44:
  case 45:
  case 46:
  case 47:
    v->color &= ~CRT_COLOR_BG_MASK;
    v->color |= ((a - 40) << CRT_COLOR_BG_SHIFT) & CRT_COLOR_BG_MASK;
    break;
  case 100:
  case 101:
  case 102:
  case 103:
  case 104:
  case 105:
  case 106:
  case 107:
    v->color &= ~CRT_COLOR_BG_MASK;
    v->color |=
      (((a -
         100) | CRT_COLOR_INTENSITY) << CRT_COLOR_BG_SHIFT) &
      CRT_COLOR_BG_MASK;
    break;
  case 49:
  case 109:
    v->color &= ~CRT_COLOR_BG_MASK;
    v->color |=
      (CRT_BGCOLOR_NORMAL << CRT_COLOR_BG_SHIFT) & CRT_COLOR_BG_MASK;
    break;
  }

}


void
vt102_parse_attr_string (VT102 * v, char *buf, int len)
{
  int private = 0;
  char last = buf[len - 1];
  char num[4];
  int o;

  memset (num, 0, sizeof (num));
  o = sizeof (num) - 1;

  len--;

  if (len < 0)
    return;

  while (len--) {
    if (*buf == ';') {
      vt102_change_attr (v, &num[o]);
      memset (num, 0, sizeof (num));
      o = sizeof (num) - 1;
      buf++;
      continue;
    }

    num[0] = num[1];
    num[1] = num[2];
    num[2] = *buf;

    if (o)
      o--;

    buf++;
  }
  vt102_change_attr (v, &num[o]);
}

void
vt102_save_state (VT102 * v)
{
  v->saved.pos = v->pos;
  v->saved.attr = v->attr;
  v->saved.color = v->color;
  v->saved.origin_mode = v->private_modes[VT102_PRIVATE_MODE_ORIGIN_MODE];
}

void
vt102_restore_state (VT102 * v)
{
  v->pos = v->saved.pos;
  v->attr = v->saved.attr;
  v->color = v->saved.color;
  v->private_modes[VT102_PRIVATE_MODE_ORIGIN_MODE] = v->saved.origin_mode;
  vt102_cursor_normalize (v);
  v->pending_wrap = 0;
}

void
vt102_regular_char (Context * c, VT102 * v, uint32_t ch)
{


  vt102_do_pending_wrap (c);


  if (v->modes[VT102_MODE_INSERT])
    vt102_insert_into_line (v, v->pos);

  v->last_reg_char = ch;


  if (ch < VT102_CHARSET_SIZE) {
    int cs;
    if ((cs = vt102_charset_c0[ch])) {
      ch = cs;
    } else if ((cs = charset_from_csid[v->g[v->cs]][ch])) {
      ch = cs;
    }
  }
  v->crt.screen[CRT_ADDR_POS (&v->pos)].chr = ch;
  v->crt.screen[CRT_ADDR_POS (&v->pos)].attr = v->attr;
  v->crt.screen[CRT_ADDR_POS (&v->pos)].color = v->color;
  vt102_cursor_advance (c);

}

vt102_send_id (Context * c, char *buf)
{

  if (c->t) {
    int l = strlen (buf);
    c->t->xmit (c->t, buf, l);
  }
}

void
vt102_scs (Context * c, int g, int s)
{
  VT102 *v = c->v;
  int cs = VT102_CSID_US;


  switch (s) {
  case 'A':
    cs = VT102_CSID_UK;
    break;
  case '1':
  case '2':
  case 'B':
    cs = VT102_CSID_US;
    break;
  case '0':
    cs = VT102_CSID_GL;
    break;
  }

  switch (g) {
  case '(':
    v->g[0] = cs;
    break;
  case ')':
    v->g[1] = cs;
    break;
  }

}

static int
vt102_parse_csi (Context * c, char *buf, int len)
{
  char last;
  char *ptr;
  char *arg = buf + 1;
  int narg;
  int err = 0;

  VT102 *v = c->v;

  buf[len] = 0;

  last = buf[len - 1];

  if (len > 2) {
    narg = atoi (arg);
  } else {
    narg = 1;
  }

  switch (buf[0]) {
  case '[':                    /* CSI */
    switch (last) {
    case '@':                  /* ICH */
      while (narg--)
        vt102_insert_into_line (v, v->pos);
      break;
    case 'A':                  /* CUU */
      vt102_cursor_relative (v, 0, -narg);
      break;
    case 'e':                  /* VPR */
    case 'B':                  /* CUD */
      vt102_cursor_relative (v, 0, narg);
      break;
    case 'a':                  /* HPR */
    case 'C':                  /* CUF */
      vt102_cursor_relative (v, narg, 0);
      break;
    case 'D':                  /* CUB */
      vt102_cursor_relative (v, -narg, 0);
      break;
    case 'E':                  /* CNL */
      vt102_cursor_relative (v, 0, narg);
      vt102_cursor_carriage_return (v);
      break;
    case 'F':                  /* CPL */
      vt102_cursor_relative (v, 0, -narg);
      vt102_cursor_carriage_return (v);
      break;
    case 'G':                  /* CHG */
      vt102_cursor_absolute (v, narg - 1, v->pos.y);
      break;
    case 'H':                  /* CUP */
    case 'f':                  /* HVP */
      {
        int x, y;

        y = narg - 1;

        ptr = index (arg, ';');
        if (ptr)
          x = atoi (ptr + 1) - 1;
        else
          x = 0;

        vt102_cursor_absolute (v, x, y);
      }
      break;
    case 'I':                  /* CHT */
      while (narg--)
        vt102_cursor_advance_tab (c->v);
      break;
    case 'J':                  /* ED */
      /* Different default */
      if (len == 2)
        narg = 0;
      switch (narg) {
      case 0:
        crt_erase (&v->crt, v->pos, v->screen_end, 1, v->color);
        break;
      case 1:
        crt_erase (&v->crt, v->screen_start, v->pos, 1, v->color);
        break;
      case 2:
        crt_erase (&v->crt, v->screen_start, v->screen_end, 1, v->color);
        break;
      }
      break;
    case 'K':                  /* EL */
      {
        CRT_Pos ls = { 0, v->pos.y };
        CRT_Pos le = { v->current_size.x - 1, v->pos.y };
        /* Different default */
        if (len == 2)
          narg = 0;

        switch (narg) {
        case 0:
          crt_erase (&v->crt, v->pos, le, 1, v->color);
          break;
        case 1:
          crt_erase (&v->crt, ls, v->pos, 1, v->color);
          break;
        case 2:
          crt_erase (&v->crt, ls, le, 1, v->color);
          break;
        }
      }
      break;

    case 'L':                  /* IL */
      if ((v->pos.y >= v->top_margin.y)
          && (v->pos.y <= v->bottom_margin.y)) {
        while (narg--)
          crt_scroll_down (&v->crt, v->pos, v->bottom_margin, 1, v->color);
      }
      break;

    case 'M':                  /* DL */
      if ((v->pos.y >= v->top_margin.y)
          && (v->pos.y <= v->bottom_margin.y)) {
        while (narg--) {
          vt102_history (c, v->pos, v->bottom_margin);
          crt_scroll_up (&v->crt, v->pos, v->bottom_margin, 1, v->color);
        }
      }
      break;
    case 'P':                  /* DCH */
      while (narg--)
        vt102_delete_from_line (v, v->pos);
      break;
    case 'S':                  /* SU */
      while (narg--) {
        vt102_history (c, v->top_margin, v->bottom_margin);
        crt_scroll_up (&v->crt, v->top_margin, v->bottom_margin, 1, v->color);
      }
      break;
    case 'T':                  /* SD */
      while (narg--)
        crt_scroll_down (&v->crt, v->top_margin, v->bottom_margin, 1,
                         v->color);
      break;
    case 'X':                  /* ECH */
      {
        CRT_Pos end = v->pos;
        if (!narg)
          narg++;

        end.x += narg - 1;
        if (end.x > v->bottom_margin.x)
          end.x = v->bottom_margin.x;

        crt_erase (&v->crt, v->pos, end, 1, v->color);
      }
      break;
    case 'Z':                  /* CBT */
      while (narg--)
        vt102_cursor_retreat_tab (c->v);
      break;
    case '`':                  /* HPA */
      vt102_cursor_absolute (v, narg - 1, v->pos.y);
      break;
    case 'b':                  /* REP */
      while (narg--) {
        vt102_regular_char (c, v, v->last_reg_char);
        err += vt102_rx_hook (c, v->last_reg_char);
      }
      break;
    case 'c':                  /* DA */
      /* For some obscure reason some programs seem to send */
      /* CSI [ ? .. c, which we ignore */
      if (buf[1] != '?')
        vt102_send_id (c, "\033[?2c");
      break;
    case 'd':                  /* VPA */
      vt102_cursor_absolute (v, v->pos.x, narg - 1);
      break;
    case 'g':                  /* TBC */
      /* Different default */
      if (len == 2)
        narg = 0;

      switch (narg) {
      case 0:
        v->tabs[v->pos.x] = 0;
        break;
      case 2:
        // FIXME: - LA120 says current line only WTF?
      case 3:
        memset (v->tabs, 0, sizeof (v->tabs));
        break;
      }
      break;

    case 'i':                  /* MC */
      // Printer commands
      // FIXME
      break;
    case 'h':                  /* SM */
    case 'l':                  /* RM */
      vt102_parse_mode_string (c, &buf[1], len - 1);
      break;
    case 'm':                  /* SGR */
      vt102_parse_attr_string (v, &buf[1], len - 1);
      break;
    case 'n':                  /* DSR */
      // Device status report
      switch (buf[1]) {
      case '5':
        vt102_send_id (c, "\033[0n");
        break;
      case '6':                /* CPR */
        if (c->t) {
          char buf[16];
          int i;
          i = sprintf (buf, "\033[%d;%dR", v->pos.y + 1, v->pos.x + 1);
          c->t->xmit (c->t, buf, i);
        }
        break;
      default:
        // ?15n printer status
        log_f (c->l, "<%s:%d unhandled DSR: \\033%s>", __FILE__,
               __LINE__, buf);
      }
      break;
    case 'q':                  /* DECLL */
      // Load LED on off 
      break;
    case 'r':                  /* DECSTBM */
      v->top_margin = v->screen_start;
      v->bottom_margin = v->screen_end;

      if ((len > 2) && (ptr = index (arg, ';'))) {
        ptr++;
        v->top_margin.y = narg - 1;
        v->bottom_margin.y = atoi (ptr) - 1;
      }

      if (v->top_margin.y < v->screen_start.y)
        v->top_margin.y = v->screen_start.y;
      if (v->top_margin.y > v->screen_end.y)
        v->top_margin.y = v->screen_end.y;
      if (v->bottom_margin.y < v->screen_start.y)
        v->bottom_margin.y = v->screen_start.y;
      if (v->bottom_margin.y > v->screen_end.y)
        v->bottom_margin.y = v->screen_end.y;

      vt102_cursor_home (v);
      break;
    case 's':                  /* SCP */
      v->saved.pos = v->pos;
      break;
    case 'u':                  /* RCP */
      v->pos = v->saved.pos;
      vt102_cursor_normalize (v);
      v->pending_wrap = 0;
      break;
    case 'y':                  /* DECTST */
      // Invoke confidence test
      break;
    case 'z':                  /* DECVERP */
      // request terminal parameters
      break;

    default:
      log_f (c->l, "<%s:%d unhandled CSI: \\033%s>", __FILE__, __LINE__, buf);

      ;
    }
    break;
  default:
    log_f (c->l, "<%s:%d unhandled sequence: \\033%s>", __FILE__,
           __LINE__, buf);
    ;
  }



  return err;
}

static int
vt102_parse_esc (Context * c)
{
  int err = 0;
  VT102 *v = c->v;
  VT102_parser *p = &v->parser;

#ifdef DEBUG
  p->cmd_buf[p->cmd_ptr] = 0;
  log_f (c->l, "<cmd in  x=%3d y=%2d aw=%d vt=%d <ESC>%s >", v->pos.x,
         v->pos.y, v->pending_wrap,
         !v->private_modes[VT102_PRIVATE_MODE_VT52], p->cmd_buf);
#endif

  if (!v->private_modes[VT102_PRIVATE_MODE_VT52]) {
    int ate = 1;

    switch (p->cmd_buf[0]) {
    case 'A':                  /* CURSOR UP */
      vt102_cursor_relative (v, 0, -1);
      break;
    case 'B':                  /* CURSOR DOWN */
      vt102_cursor_relative (v, 0, 1);
      break;
    case 'C':                  /* CURSOR RIGHT */
      vt102_cursor_relative (v, 1, 0);
      break;
    case 'D':                  /* CURSOR LEFT */
      vt102_cursor_relative (v, -1, 0);
      break;
    case 'F':                  /* ENTER GRAPHICS MODE */
      v->cs = 1;
      v->g[1] = VT102_CSID_VT52;
      break;
    case 'G':                  /* EXIT GRAPHICS MODE */
      v->cs = 0;
      v->g[0] = VT102_CSID_US;
      break;
    case 'H':                  /* CURSOR HOME */
      vt102_cursor_absolute (v, 0, 0);
      break;
    case 'I':                  /* REVERSE LINE FEED */
      vt102_cursor_retreat_line (c);
      break;
    case 'J':                  /* ERASE TO END OF SCREEN */
      crt_erase (&v->crt, v->pos, v->screen_end, 1, v->color);
      break;
    case 'K':                  /* ERASE TO END OF LINE */
      {
        CRT_Pos le = { v->current_size.x - 1, v->pos.y };
        crt_erase (&v->crt, v->pos, le, 1, v->color);
      }
      break;
    case 'Y':                  /* DIRECT CURSOR ADDRESS */
      vt102_cursor_absolute (v, p->cmd_buf[2] - 040, p->cmd_buf[1] - 040);
      break;
    case 'V':                  /* PRINT CURSOR LINE */
      // Print current line
      break;
    case 'W':
      // Printer on
      break;
    case 'X':
      // printer off
      break;
    case ']':                  /* PRINT SCREEN */
      // print screen
      break;
    case 'Z':                  // ID
      vt102_send_id (c, "\033/K");
      break;
    case '^':
      // Autoprint on
      break;
    case '_':
      // Autoprint off
      break;
    case '=':                  /* ENTER ALTERNATE KEYPAD MODE */
      v->application_keypad_mode = 1;
      break;
    case '>':                  /* EXIT ALTERNATE KEYPAD MODE */
      v->application_keypad_mode = 0;
      break;
    default:
      ate = 0;
    }
    if (ate)
      return;
  }


  /* 
   * If you edit this switch don't forget to edit the length and
   * termination tables 
   */
  switch (p->cmd_buf[0]) {
  case 'D':                    /* IND */
    vt102_cursor_advance_line (c);
    break;

  case 'E':                    /* NEL */
    vt102_cursor_advance_line (c);
    v->pos.x = v->top_margin.x;
    vt102_cursor_normalize (v);
    v->pending_wrap = 0;
    break;
    /* F SSA */
    /* G ESA */
  case 'H':                    /* HTS */
    v->tabs[v->pos.x]++;
    break;
    /* I HTJ */
    /* J VTS */
    /* K PLD */
    /* L PLU */
  case 'M':                    /* RI */
    vt102_cursor_retreat_line (c);
    break;
  case 'N':                    /* SS2 */
    // select G2 for one char
    break;
  case 'O':                    /* SS3 */
    // select G3 for one char
    break;
    /* P DCS */
    /* Q PU1 */
    /* R PU2 */
    /* S STS */
    /* T CCH */
    /* U MW */
    /* V SPA */
    /* W EPA */
    /* X SOS */
    /* Y SGCI */
  case 'Z':                    /* DECID */
    vt102_send_id (c, terminal_id);
    break;
  case 'c':                    /* RIS */
    vt102_reset (c);
    break;
  case '=':
    /* DECKPAM */
    v->application_keypad_mode = 1;
    break;
  case '>':
    /* DECKPNM */
    v->application_keypad_mode = 0;
    break;

  case '#':
    switch (p->cmd_buf[1]) {
    case '3':                  /* DECDHL */
      // top of double height line
    case '4':                  /* DECDHL */
      // bottom of double height line
    case '5':                  /* DECSWL */
      // single width line
    case '6':                  /* DECDWL */
      // double width line
      break;
    case '8':                  /* DECALN */
      {
        int i;
        crt_erase (&v->crt, v->screen_start, v->screen_end, 1,
                   CRT_COLOR_NORMAL);
        for (i = 0; i < CRT_ADDR_POS (&v->screen_end); ++i)
          v->crt.screen[i].chr = 'E';
      }
      break;
    default:

      log_f (c->l,
             "<%s:%d unhandled ESC: \\033 \\043 \\%03o (ESC # %c)>",
             __FILE__, __LINE__, p->cmd_buf[1], safe_ch (p->cmd_buf[1]));

    }
    break;
  case '<':                    /* DECANM */
    // Set ansi mode - ignored 
    break;
  case '7':                    /* DECSC */
    vt102_save_state (v);
    break;
  case '8':                    /* DECRC */
    vt102_restore_state (v);
    break;
  case ']':
    /* Set various titles in xterm - ignored */
    break;
  case '[':                    /* CSI */
    err += vt102_parse_csi (c, p->cmd_buf, p->cmd_ptr);
    break;
    /* Charsets */
  case '(':                    /* SCS */
  case ')':                    /* SCS */
  case '+':                    /* SCS */
  case '*':                    /* SCS */
  case '%':                    /* SCS */
    vt102_scs (c, p->cmd_buf[0], p->cmd_buf[1]);
    break;
  default:
    log_f (c->l, "<%s:%d unhandled ESC: \\033 \\%03o (ESC %c)>", __FILE__,
           __LINE__, p->cmd_buf[0], safe_ch (p->cmd_buf[0]));

    ;
  }

  p->cmd_buf[p->cmd_ptr] = 0;
#ifdef DEBUG
  log_f (c->l, "<cmd out x=%3d y=%2d aw=%d>", v->pos.x, v->pos.y,
         v->pending_wrap);
#endif
  return err;
}

void
vt102_status_line (VT102 * v, char *str)
{
  int i;
  CRT_CA *ca = &v->crt.screen[CRT_ADDR (v->current_size.y, 0)];

  for (i = 0; i < v->current_size.x; ++i) {
    ca->attr = CRT_ATTR_REVERSE;
    ca->color = CRT_COLOR_NORMAL;
    ca->chr = *str ? *str : ' ';
    if (*str)
      str++;
    ca++;
  }

  for (; i < VT102_MAX_COLS; ++i) {
    ca->attr = CRT_ATTR_NORMAL;
    ca->color = CRT_COLOR_NORMAL;
    ca->chr = ' ';
    ca++;
  }

}


void
vt102_parser_reset (VT102_parser * p)
{
  p->in_cmd = 0;
  p->cmd_more_bytes = 0;
  p->cmd_termination = 0;
  p->in_escape = 0;
}


void
vt102_reset_state (Context * c)
{
  VT102 *v = c->v;
  vt102_parser_reset (&v->parser);

  v->attr = CRT_ATTR_NORMAL;
  v->color = CRT_COLOR_NORMAL;

  v->application_keypad_mode = 0;

  v->current_size = v->original_size;

  vt102_do_resize (c);

  memset (v->modes, 0, VT102_NMODES);
  memset (v->private_modes, 0, VT102_NMODES);

  v->private_modes[VT102_PRIVATE_MODE_AUTO_WRAP] = 1;
  v->private_modes[VT102_PRIVATE_MODE_AUTO_REPEAT] = 1;
  v->private_modes[VT102_PRIVATE_MODE_SHOW_CURSOR] = 1;
  v->private_modes[VT102_PRIVATE_MODE_VT52] = 1;
  v->modes[VT102_MODE_LOCAL_ECHO_OFF] = 1;

  vt102_reset_tabs (v);

  v->g[0] = v->g[1] = VT102_CSID_US;
  v->cs = 0;
}

static void
pre_parse_cmd (int ch, VT102_parser * p)
{
  if (ch > TABLE_LENGTH)
    return;
  if (ch < 0)
    return;

  p->cmd_more_bytes = 0;
  p->in_cmd = 0;

  p->cmd_termination = vt102_cmd_termination[ch];
  if (p->cmd_termination) {
    p->in_cmd++;
    return;
  }

  p->cmd_more_bytes = vt102_cmd_length[ch];

  if (p->cmd_more_bytes) {
    p->in_cmd++;
    return;
  }
}

int
vt102_rx_hook (Context * c, int ch)
{
  if (!c->r)
    return 0;
  if (!c->r->rx)
    return 0;
  return c->r->rx (c->r, ch);
}

int
vt102_parse_char (Context * c, int ch)
{
  int err = 0;
  VT102 *v = c->v;
  VT102_parser *p = &v->parser;

#ifdef DEBUG
  log_f (c->l, "char %3d %c   ie=%d ic=%d cmb=%d ct=%3d    %2d %2d %d", ch,
         safe_ch (ch), p->in_escape, p->in_cmd, p->cmd_more_bytes,
         p->cmd_termination, v->pos.x, v->pos.y, v->pending_wrap);
#endif


  if (ch == SYM_CHAR_RESET) {
    vt102_reset_state (c);
  } else if (p->in_cmd && !ctrl_chr (ch, p->cmd_termination)) {
    p->cmd_buf[p->cmd_ptr++] = ch;
    if (p->cmd_ptr == VT102_CMD_LEN)
      p->in_cmd = 0;
    if (p->cmd_more_bytes) {
      p->cmd_more_bytes--;


      if (!p->cmd_more_bytes == 1)
        p->in_cmd = 0;
    }

    switch (p->cmd_termination) {
    case 0:
      break;
    default:
      if (p->cmd_termination == ch)
        p->in_cmd = 0;
      break;
    case CSI_ENDER:
      if (csi_ender (ch))
        p->in_cmd = 0;
      break;
    }

    if (!p->in_cmd) {
      err += vt102_parse_esc (c);
      p->cmd_more_bytes = 0;
      p->cmd_termination = 0;
    }
  } else if (p->in_escape && !ctrl_chr (ch, 0)) {
    p->cmd_ptr = 0;
    p->cmd_buf[p->cmd_ptr++] = ch;
    p->in_escape = 0;

    pre_parse_cmd (ch, p);

    if (!p->in_cmd)
      err += vt102_parse_esc (c);
  } else if ((ch >= 0x80) && (ch < 0xa0)) {
    /* C1 characters */
    switch (ch) {
    case 0x80:                 /* @ PAD */
    case 0x81:                 /* A HOP */
    case 0x82:                 /* B BPH */
    case 0x83:                 /* C NBH */
    case 0x84:                 /* D IND */
    case 0x85:                 /* E NEL */
    case 0x86:                 /* F SSA */
    case 0x87:                 /* G ESA */
    case 0x88:                 /* H HTS */
    case 0x89:                 /* I HTJ */
    case 0x8a:                 /* J VTS */
    case 0x8b:                 /* K PLD */
    case 0x8c:                 /* L PLU */
    case 0x8d:                 /* M RI */
    case 0x8e:                 /* N SS2 */
    case 0x8f:                 /* O SS3 */
    case 0x90:                 /* P DCS */
    case 0x91:                 /* Q PU1 */
    case 0x92:                 /* R PU2 */
    case 0x93:                 /* S STS */
    case 0x94:                 /* T CCH */
    case 0x95:                 /* U MW */
    case 0x96:                 /* V SPA */
    case 0x97:                 /* W EPA */
    case 0x98:                 /* X SOS */
    case 0x99:                 /* Y SGCI */
    case 0x9a:                 /* Z DECID */
    case 0x9b:                 /* [ CSI */
    case 0x9c:                 /* \ ST */
    case 0x9d:                 /* ] OSC */
    case 0x9e:                 /* ^ PM */
    case 0x9f:                 /* _ APC */
      /* C1 chars are equavlent to ESC ch-0x40 */
      p->cmd_ptr = 0;
      p->cmd_buf[p->cmd_ptr++] = ch;
      p->in_escape = 0;

      pre_parse_cmd (ch, p);

      if (!p->in_cmd)
        err += vt102_parse_esc (c);
      break;
    }
  } else {
#ifdef DEBUG
    if (ch != 27)
      log_f (c->l,
             "<chr in  x=%3d y=%2d aw=%d  \\%03o %3d %c",
             v->pos.x, v->pos.y, v->pending_wrap, ch, ch, safe_ch (ch));

#endif

    switch (ch) {
    case 0:                    /* NUL */
      break;
#ifdef HIDE_NON_PRINTABLES
    case 1:                    /* SOH */
    case 2:                    /* STX */
    case 3:                    /* ETX */
    case 4:                    /* EOT */
      break;
#endif
    case 5:                    /* ENQ */
      vt102_send_id (c, terminal_id);
      break;
#ifdef HIDE_NON_PRINTABLES
    case 6:                    /* ACK */
      break;
#endif
    case 7:                    /* BEL */
      // FIXME beep
      break;
    case 8:                    /* BS */
      vt102_cursor_retreat (c->v);
      err += vt102_rx_hook (c, ch);
      break;
    case 9:                    /* HT */
      vt102_cursor_advance_tab (c->v);
      err += vt102_rx_hook (c, ch);
      break;
    case 10:                   /* LF */
    case 11:                   /* VT */
    case 12:                   /* FF */
      vt102_cursor_advance_line (c);
      err += vt102_rx_hook (c, 10);
      if (!v->modes[VT102_MODE_NEWLINE_MODE])
        break;
    case 13:                   /* CR */
      vt102_cursor_carriage_return (v);
      err += vt102_rx_hook (c, 13);
      break;
    case 14:                   /* SO */
      /* select G1 */
      v->cs = 1;
      break;
    case 15:                   /* SI */
      /* select G0 */
      v->cs = 0;
      break;
#ifdef HIDE_NON_PRINTABLES
    case 16:                   /* DLE */
    case 17:                   /* DC1 */
    case 18:                   /* DC2 */
    case 19:                   /* DC3 */
    case 20:                   /* DC4 */
    case 21:                   /* NAK */
    case 22:                   /* SYN */
    case 23:                   /* ETB */
    case 24:                   /* CAN */
    case 25:                   /* EM */
    case 26:                   /* SUB */
      break;
#endif
    case 27:                   /* ESC */
      p->in_escape++;
      break;
#ifdef HIDE_NON_PRINTABLES
    case 28:                   /* FS */
    case 29:                   /* GS */
    case 30:                   /* RS */
    case 31:                   /* US */
    case 127:                  /* DEL */
      break;
#endif
    default:                   /* regular character */
      vt102_regular_char (c, v, ch);
      err += vt102_rx_hook (c, ch);
    }

#ifdef DEBUG
    if (ch != 27)
      log_f (c->l, "<chr out x=%3d y=%2d aw=%d>", v->pos.x, v->pos.y,
             v->pending_wrap);
#endif
  }

  vt102_crt_update (c);

  return err;
}


void
vt102_send (Context * c, uint8_t key)
{
  uint8_t ch;

  if (!c->t)
    return;

  if ((key > 31) && (key < 127)) {
    c->t->xmit (c->t, &key, 1);
    return;
  }

  switch (key) {
  case 0:                      /* NUL */
  case 1:                      /* SOH */
  case 2:                      /* STX */
  case 3:                      /* ETX */
  case 4:                      /* EOT */
  case 5:                      /* ENQ */
  case 6:                      /* ACK */
  case 7:                      /* BEL */
  case 8:                      /* BS */
  case 9:                      /* HT */
  case 10:                     /* LF */
  case 11:                     /* VT */
  case 12:                     /* FF */
    c->t->xmit (c->t, &key, 1);
    break;
  case 13:                     /* CR */
    c->t->xmit (c->t, &key, 1);
    if (c->v->modes[VT102_MODE_NEWLINE_MODE]) {
      ch = 10;
      c->t->xmit (c->t, &ch, 1);
    }
    break;
  case 14:                     /* SO */
  case 15:                     /* SI */
  case 16:                     /* DLE */
  case 17:                     /* DC1 */
  case 18:                     /* DC2 */
  case 19:                     /* DC3 */
  case 20:                     /* DC4 */
  case 21:                     /* NAK */
  case 22:                     /* SYN */
  case 23:                     /* ETB */
  case 24:                     /* CAN */
  case 25:                     /* EM */
  case 26:                     /* SUB */
    c->t->xmit (c->t, &key, 1);
    break;
  case 27:                     /* ESC */
  case 28:                     /* FS */
  case 29:                     /* GS */
  case 30:                     /* RS */
  case 31:                     /* US */
  case 127:                    /* DEL */
    c->t->xmit (c->t, &key, 1);
    break;

  case KEY_UP:
  case KEY_DOWN:
  case KEY_RIGHT:
  case KEY_LEFT:
  case KEY_HOME:
  case KEY_MIDDLE:
  case KEY_END:

    if (c->v->private_modes[VT102_PRIVATE_MODE_CURSOR_MODE]) {
      uint8_t buf[] = { 033, 'O', 'A' + (key - KEY_UP) };
      c->t->xmit (c->t, &buf, sizeof (buf));
    } else {
      uint8_t buf[] = { 033, '[', 'A' + (key - KEY_UP) };
      c->t->xmit (c->t, &buf, sizeof (buf));
    }
    break;
  case KEY_STAR:
  case KEY_PLUS:
  case KEY_COMMA:
  case KEY_PERIOD:
  case KEY_DIVIDE:
  case KEY_0:
  case KEY_1:
  case KEY_2:
  case KEY_3:
  case KEY_4:
  case KEY_5:
  case KEY_6:
  case KEY_7:
  case KEY_8:
  case KEY_9:
    if (c->v->application_keypad_mode) {
      uint8_t buf[] = { 033, 'O', 'a' + (key - KEY_154) };
      c->t->xmit (c->t, &buf, sizeof (buf));
    } else {
      static char kpoff[KEY_NUM] = {
        [KEY_STAR] = '*',
        [KEY_PLUS] = '+',
        [KEY_COMMA] = ',',
        [KEY_MINUS] = '-',
        [KEY_PERIOD] = '.',
        [KEY_DIVIDE] = '/',
        [KEY_0] = '0',
        [KEY_1] = '1',
        [KEY_2] = '2',
        [KEY_3] = '3',
        [KEY_4] = '4',
        [KEY_5] = '5',
        [KEY_6] = '6',
        [KEY_7] = '7',
        [KEY_8] = '8',
        [KEY_9] = '9'
      };

      c->t->xmit (c->t, &kpoff[key], 1);
    }
    break;
  case KEY_ENTER:
    if (c->v->application_keypad_mode) {
      uint8_t buf[] = { 033, 'O', 'M' };
      c->t->xmit (c->t, &buf, sizeof (buf));
    } else {
      ch = 13;
      c->t->xmit (c->t, &ch, 1);
      if (c->v->modes[VT102_MODE_NEWLINE_MODE]) {
        ch = 10;
        c->t->xmit (c->t, &ch, 1);
      }
    }
    break;
  case KEY_PF1:
  case KEY_PF2:
  case KEY_PF3:
  case KEY_PF4:
    {
      uint8_t buf[] = { 033, 'O', 'P' + (key - KEY_PF1) };
      c->t->xmit (c->t, &buf, sizeof (buf));
    }
    break;
  case KEY_VT220_HOME:
  case KEY_INSERT:
  case KEY_DELETE:
  case KEY_VT220_END:
  case KEY_PGUP:
  case KEY_PGDN:
  case KEY_F1:
  case KEY_F2:
  case KEY_F3:
  case KEY_F4:
  case KEY_F5:
  case KEY_F6:
  case KEY_F7:
  case KEY_F8:
  case KEY_F9:
  case KEY_F10:
  case KEY_F11:
  case KEY_F12:
  case KEY_F13:
  case KEY_F14:
  case KEY_F15:
  case KEY_F16:
  case KEY_F17:
  case KEY_F18:
  case KEY_F19:
  case KEY_F20:
    {
      uint8_t buf[16];
      int i;
      i = sprintf (buf, "\033[%d~", (key - KEY_180));
      c->t->xmit (c->t, &buf, i);
    }
    break;
  }

}

void
vt102_reset (Context * c)
{
  VT102 *v = c->v;
  VT102_parser *p = &v->parser;


  crt_cls (&v->crt);
  v->current_line = v->pos;
  v->pending_wrap = 0;

  v->screen_start.x = 0;
  v->screen_start.y = 0;
  v->current_size = v->original_size;
  v->crt.size = v->current_size;
  v->crt.size.y++;
  v->screen_end = v->current_size;
  v->screen_end.x--;
  v->screen_end.y--;

  vt102_cursor_home (v);
  vt102_status_line (v, "");

  vt102_reset_tabs (v);
  v->current_line = v->pos;

  vt102_parser_reset (p);
  vt102_reset_state (c);

  vt102_save_state (v);

  v->last_reg_char = ' ';
}

VT102 *
vt102_new (CRT_Pos * size)
{
  VT102 *v;

  v = (VT102 *) xmalloc (sizeof (VT102));

  v->xn_glitch = 1;


  if (size) {
    v->original_size = *size;

    if (v->original_size.x < 1)
      v->original_size.x = 1;
    if (v->original_size.y < 1)
      v->original_size.y = 1;

    if (v->original_size.x > VT102_MAX_COLS)
      v->original_size.x = VT102_MAX_COLS;
    if (v->original_size.y > VT102_MAX_ROWS)
      v->original_size.y = VT102_MAX_ROWS;

  } else {
    v->original_size.x = VT102_COLS_80;
    v->original_size.y = VT102_ROWS_24;
  }

  return v;
}

void
vt102_set_ansi (VT102 * v, int ansi)
{
  v->xn_glitch = ansi ? 0 : 1;
}

void
vt102_resize (Context * c, CRT_Pos size)
{
  log_f (c->l, "<size change to %dx%d requested>", size.x, size.y);

  if (!size.x)
    size.x = c->v->current_size.x;
  if (!size.y)
    size.y = c->v->current_size.y;

  if (size.x < 1)
    size.x = 1;
  if (size.y < 1)
    size.y = 1;

  if (size.x > VT102_MAX_COLS)
    size.x = VT102_MAX_COLS;
  if (size.y > VT102_MAX_ROWS)
    size.y = VT102_MAX_ROWS;

  c->v->current_size = size;
  vt102_do_resize (c);
}

void
vt102_free (VT102 * v)
{
  free (v);
}

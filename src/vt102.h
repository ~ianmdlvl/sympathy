/* 
 * vt102.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: vt102.h,v 1.25 2008/03/10 11:49:33 james Exp $
 */

/* 
 * $Log: vt102.h,v $
 * Revision 1.25  2008/03/10 11:49:33  james
 * *** empty log message ***
 *
 * Revision 1.24  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.23  2008/03/03 06:04:42  james
 * *** empty log message ***
 *
 * Revision 1.22  2008/03/02 10:37:56  james
 * *** empty log message ***
 *
 * Revision 1.21  2008/02/28 16:57:52  james
 * *** empty log message ***
 *
 * Revision 1.20  2008/02/27 09:42:22  james
 * *** empty log message ***
 *
 * Revision 1.19  2008/02/26 23:23:17  james
 * *** empty log message ***
 *
 * Revision 1.18  2008/02/26 19:08:27  james
 * *** empty log message ***
 *
 * Revision 1.17  2008/02/26 16:53:24  james
 * *** empty log message ***
 *
 * Revision 1.16  2008/02/24 12:22:42  james
 * *** empty log message ***
 *
 * Revision 1.15  2008/02/24 00:42:53  james
 * *** empty log message ***
 *
 * Revision 1.14  2008/02/23 11:48:37  james
 * *** empty log message ***
 *
 * Revision 1.13  2008/02/22 17:07:00  james
 * *** empty log message ***
 *
 * Revision 1.12  2008/02/22 14:51:54  james
 * *** empty log message ***
 *
 * Revision 1.11  2008/02/08 15:06:42  james
 * *** empty log message ***
 *
 * Revision 1.10  2008/02/07 12:16:04  james
 * *** empty log message ***
 *
 * Revision 1.9  2008/02/07 01:57:46  james
 * *** empty log message ***
 *
 * Revision 1.8  2008/02/07 00:39:13  james
 * *** empty log message ***
 *
 * Revision 1.7  2008/02/06 20:26:58  james
 * *** empty log message ***
 *
 * Revision 1.6  2008/02/06 17:53:28  james
 * *** empty log message ***
 *
 * Revision 1.5  2008/02/06 15:53:22  james
 * *** empty log message ***
 *
 * Revision 1.4  2008/02/06 11:30:37  james
 * *** empty log message ***
 *
 * Revision 1.3  2008/02/04 20:23:55  james
 * *** empty log message ***
 *
 * Revision 1.2  2008/02/04 02:05:06  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/03 23:36:41  james
 * *** empty log message ***
 *
 */

#ifndef __VT102_H__
#define __VT102_H__

#define VT102_CMD_LEN 128

#define VT102_MAX_ROWS		((CRT_ROWS) - 1)
#define VT102_ROWS_24		24
#define VT102_COLS_132		132
#define VT102_COLS_80		80
#define VT102_MAX_COLS		VT102_COLS_132
#define VT102_STATUS_ROW	24

#define VT102_NMODES		32


typedef struct {
  int in_escape;
  int in_cmd;

  int cmd_ptr;
  int cmd_more_bytes;
  int cmd_termination;
  char cmd_buf[VT102_CMD_LEN];
} VT102_parser;

typedef struct {
  CRT_Pos pos;
  int attr;
  int color;
  int origin_mode;
} VT102_State;


typedef struct {
  CRT_Pos top_margin, bottom_margin;
  CRT_Pos screen_start, screen_end;
  VT102_parser parser;
  int attr;
  int color;
  CRT crt;

  int pending_wrap;
  CRT_Pos pos, current_line;

  VT102_State saved;

  uint8_t modes[VT102_NMODES];
  uint8_t private_modes[VT102_NMODES];

  uint8_t tabs[VT102_COLS_132];

  int application_keypad_mode;

  int last_reg_char;
  int xn_glitch;

  CRT_Pos current_size;
  CRT_Pos original_size;

  int g[2];
  int cs;

} VT102;

#define VT102_PRIVATE_MODE_CURSOR_MODE		1
#define VT102_PRIVATE_MODE_VT52	 		2
#define VT102_PRIVATE_MODE_132COLS	 	3
#define VT102_PRIVATE_MODE_SMOOTH_SCROLL 	4
#define VT102_PRIVATE_MODE_REVERSE_SCREEN 	5
#define VT102_PRIVATE_MODE_ORIGIN_MODE		6
#define VT102_PRIVATE_MODE_AUTO_WRAP		7
#define VT102_PRIVATE_MODE_AUTO_REPEAT		8
#define VT102_PRIVATE_MODE_SHOW_CURSOR		25

#define VT102_MODE_KEYBOARD_DISABLE		2
#define VT102_MODE_INSERT			4
#define VT102_MODE_LOCAL_ECHO_OFF		12
#define VT102_MODE_NEWLINE_MODE			20

#endif /* __VT102_H__ */

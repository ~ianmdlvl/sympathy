/* 
 * vt102_charset.h:
 *
 * Copyright (c) 2008 James McKenzie <sympathy@madingley.org>,
 * All rights reserved.
 *
 */

/* 
 * $Id: vt102_charset.h,v 1.2 2008/03/07 12:37:04 james Exp $
 */

/* 
 * $Log: vt102_charset.h,v $
 * Revision 1.2  2008/03/07 12:37:04  james
 * *** empty log message ***
 *
 * Revision 1.1  2008/02/27 01:32:41  james
 * *** empty log message ***
 *
 */

#ifndef __VT102_CHARSET_H__
#define __VT102_CHARSET_H__

#define VT102_CHARSET_SIZE 128

#define VT102_CSID_US	0
#define VT102_CSID_UK	1
#define VT102_CSID_GL	2
#define VT102_CSID_VT52	3

#endif /* __VT102_CHARSET_H__ */

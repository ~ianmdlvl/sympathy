.TH sympathy 1  "After March 12, 2008" "Version 1.2.1 + Edits" "USER COMMANDS"
.\" $Id: sympathy.1,v 1.28 2008/05/09 12:31:55 james Exp $
.SH NAME
sympathy \- client/server terminal emulator with logging
.SH SYNOPSIS
.B sympathy \-\fBt\fP
[
.B \fIterminal_options\fP
] [
.B \fIdisplay_options\fP
]
.br
.B sympathy \-\fBs\fP
[
.B \fIterminal_options\fP
] [
.B \fIserver_options\fP
]
.br
.B sympathy 
[
.B \-\fBc\fP \-\fBs\fP
] [
.B \fIterminal_options\fP
] [
.B \fIserver_options\fP
] [
.B \fIclient_options\fP
] [
.B \fIdisplay_options\fP
]
.br
.B sympathy \-\fBc\fP
[
.B \fIclient_options\fP
] [
.B \fIdisplay_options\fP
] 
.br
.B sympathy \-\fBr\fP
id
[
.B \fIclient_options\fP
] [
.B \fIdisplay_options\fP
] 
.br
.B sympathy \-\fBC\fP \-\fBd\fP
serialdev 
.br
.B sympathy \-\fBls\fP
.br
.B sympathy \-\fBv\fP
.br
.SH DESCRIPTION
.I Sympathy
is a replacement for screen(1), minicom(1) and consolidate(1). It
is a VT52/VT100/ANSI terminal emulator with some special features. In normal use
.I sympathy
would sit between a terminal device (a serial port or a pseudo-tty) and
the user's usual terminal emulator (eg xterm(1)). 
.I Sympathy
renders data from the terminal device into an internal frame buffer
and then expresses changes in this frame buffer to the outer terminal
emulator using a small subset of ANSI escape codes. 
.I Sympathy 
always generates valid escape codes to the outer terminal, and will 
reset the state of its internal terminal emulator when it detects
receive errors on the terminal device.
.PP
.I Sympathy\fP,
unlike screen(1), takes care to preserve the scroll-back features
of the outer terminal emulator: lines that scroll off the top of the internal 
frame buffer are scrolled off the top of the outer terminal emulator. When
.I sympathy 
is used in client/server mode, some history is added to the outer 
terminal emulator when the client connects.
.PP
.I Sympathy
also supports automatic baud\-rate detection, and advanced logging features.
.I Sympathy
logs whenever any of the modem control lines change state, receive errors,
and the contents
of any line on the frame buffer as the cursor moves off it.
.SH OPTIONS
.B \fImajor mode options\fP:
.TP 5
.B \-C\fP 
clear lock files
.I sympathy 
will remove inactive lockfiles for the specified serial device, and then exit.
.TP 5
.B \-c\fP or \fB\-r\fP \fIid\fP
act as client only:
.I sympathy
connects to a 
.I sympathy 
server process and injects the history into the outer terminal
emulator,
and connects the user with the terminal device. One server process can support multiple 
client processes. This mode can also be used to obtain a dump of the current screen
in HTML format (see the \-\fBH\fP option), inject key\-presses (see \-\fBI\fP)
or allow control via a dumb terminal (see \-\fBN\fP).
The \-\fPr\fP option connects to a
server process socket called \fIid\fP, or if \fIid\fP is an integer
\fIhost-name\fP.\fIid\fP mimicking the behaviour of screen(1). 
.I Sympathy
searches for the socket in the following directories: \fI~/.sympathy\fP, 
\fI~/sympathy\fP, \fI/etc/sympathy\fP, \fI/var/sympathy\fP.
With the \-\fBc\fP option the socket must be specified with the \-\fBk\fP
option.
.TP 5
.B \fP[\fB \-c \-s \fP]
act as both client and server:
.I sympathy
forks. The child process becomes a server, and the original process becomes a client
which then connects to the server. This is the default major mode if no other is specified. 
Since the default terminal device is a pseudo-tty, running 
.I sympathy
with no arguments will start a new shell in a daemonised process and connect to it
as a client from the original process, mimicking the behaviour of screen(1)
.TP 5 
.B \-l\fP or \fB\-ls
show active sockets:
.I sympathy 
will show active sockets, ones to which a call to connect(2) succeeds, 
in ~/.sympathy. If the socket name begins with the host-name of the machine, and
the call to connect(2) fails, then socket will be unlinked. 
.TP 5
.B \-s
act as server only:
.I sympathy
opens the terminal device and renders into an internal frame buffer, listens for clients
on the socket and logs activity. By default the server will fork into a daemon process
but can be forced to remain in the foreground with the \-\fBF\fP option.
.TP 5
.B \-t
act as terminal emulator only:
.I sympathy
opens the terminal device and outputs into the outer terminal emulator. When 
.I sympathy exits the 
device is closed and no process remains. In this mode 
.I sympathy
behaves like a traditional
terminal emulator such as cu(1) or minicom(1).
.TP 5 
.B \-v
show current version:
.I sympathy 
will print the the version number of the code it was compiled from.
.TP 5
.B \-h
show help:
.I sympathy 
will show brief usage instructions
.PP
.B \fIterminal_options\fP:
.TP 5
.B \-d \fIserialdev\fP
connect to terminal device \fIserialdev\fP, eg /dev/ttyS0. 
By default 
.I sympathy
doesn't
lock the terminal device, but checks periodically for lock files of other processes. If 
.I sympathy
detects another lock file it displays \fBLocked\fP in the status line 
and refuses I/O on the device until the lock file is removed or becomes invalid.
To lock the device use the \-\fBK\fP option. 
.I Sympathy 
will in addition check that the name of the device does not occur in /proc/cmdline
as an argument to the \fIconsole\fP kernel parameter. 
The \-\fBd\fP option is incompatible with the \-\fBp\fP option. 
.TP 5
.B \-p
connect to a pseudo\-tty instead of a terminal device, and fork a login shell in
it. The \-\fBp\fP option is incompatible with the \-\fBd\fP option. This is the default 
terminal device if none is specified. The first non\-option command line
arugment is considered to be the a binary to run in the pseudo\-tty, subsequent
arguments are parsed to the binary. The current value of PATH is searched for
the binary if it does not start with '/'. If no binary is specified 
then '/bin/sh' is called with \fIargv[0]\fP set to '\-'.  
.TP 5
.B \-K
lock the terminal device specified in the \-\fBd\fP option. 
.I Sympathy
generates lock files in a staggering variety of formats and places. For locks
based on the name of the device 
.I sympathy 
generates lock files for all devices
with the same major and minor in /dev, /dev/usb and /dev/tts, it uses both
normal and lower case and replaces occurrences of `/' in the device name with
both `.' and `_'.
.I Sympathy
also generates locks based on the device major and minor numbers, and for all lock file
names generates them in any of the following directories that are writable:
/var/lock/uucp, /var/spool/lock, /var/spool/uucp, /etc/locks, /usr/spool/uucp, 
/var/spool/locks, /usr/spool/lock, /usr/spool/locks, /usr/spool/uucp/LCK, /var/lock. 
Lock files are assumed to be in HDB format.
.TP 5
.B \-b \fIbaud\-rate\fP
set the baud\-rate of the terminal device specified in the \-\fBd\fP to
\fIbaud\-rate\fP, if omitted the current baud\-rate of the serial port will be
used.
.TP 5
.B \-f
turn on flow control on the terminal device. This option adds \fICRTSCTS\fP to 
\fIsympathy\fP's default 
\fIc_cflag\fPs of \fICS8|CREAD|CLOCAL\fP.
.TP 5
.B \-L \fIlogfile\fP
log activity to the file \fIlogfile\fP. If \fIlogfile\fP is `-' then log to
\fIstdout\fP. Note that logging to \fIstdout\fP only makes sense with the
\-\fBF\fP \fIserver_option\fP. 
.I Sympathy 
will also close and reopen its log file when it receives a \fBSIGHUP\fP, which
with the \fB\-P\fP allows the use of logrotate(8).
.TP 5
.B \-P \fIpidfile\fP
write the pid of the server/terminal process to \fIpidfile\fP, which is 
removed on clean exit.
.TP 5
.B \-R
rotate log files. When the log\-file specified with the \fB\-L\fP option
grows large 
.I sympathy 
will rotate it and compress old log\-files. 
.TP 5
.B \-w \fIwidth\fP[x\fIheight\fP]
set the initial size of the terminal emulator's frame buffer to be \fIwidth\fP columns
by \fIheight\fP rows. If \fIheight\fP is omitted it defaults to 24, the default width
is 80. These values may
be overridden later by terminal escape sequences. If \-\fBp\fP is also specified
the pseudo-tty will have its window size set to match.
.PP
.B \fIdisplay_options\fP:
.TP 5
.B \-u
attempt to render Unicode characters in the internal frame buffer to the outer terminal 
emulator by using ISO-2202 sequences. 
.I Sympathy
currently only checks to see if an appropriate character appears in the VT102 
US character set, or in the VT102 `special characters and line drawing' character set.
If the character appears in neither of these then it will be rendered on the outer
terminal emulator as a `?'.
.TP 5
.B \-H
render the current state of the internal frame buffer to \fIstdout\fP as HTML, then quit.
.TP 5
.B \-N
don't render the internal frame buffer using ANSI characters, but instead write characters
that would be written to the screen to stdout. Take characters from stdin and send them
to the device. This feature is useful when you wish to use sympathy in conjunction with programs
like expect(1).
.PP
.B \fIclient_options\fP:
.TP 5
.B \-k \fIsocket\fP
set the name in the file-system of the socket to which
.I sympathy
should connect. This option is \fBmandatory\fP unless the \-\fBs\fP or \-\fBr\fP options
have also been given. If \-\fBs\fP is given then it will default to the socket which
the forked server process opens. See the discussion of the \-\fPr\fP option above, for
information on how 
.I sympathy 
chooses a socket name if \-\fBr\fP is specified.
.TP 5
.B \-I string
Inject \fIstring\fP to the device as if it had been typed at the keyboard.  Each character in 
the string is treated as a key\-press.
.I Sympathy
recognizes various common escapes for control characters. Other keys, for example the arrow keys,
are mapped to character codes between 128 and 255, see src/keys.h for details.
.PP
.B \fIserver_options\fP:
.TP 5
.B \-F
tells the 
.I sympathy
server process not to become a daemon but to remain the the foreground. This option is
incompatible with the \-\fBc\fP \-\fBs\fP major mode.
.TP 5
.B \-k \fIsocket\fP
set the name in the file-system of the socket on which
.I sympathy
should listen for clients. If this option is omitted 
.I sympathy 
will create a 
socket in ~/.sympathy, creating that directory if necessary, and named
\fIhost-name\fP.\fIpid\fP where \fIpid\fP is the process id of the 
.I sympathy
process that created the socket.
.TP 5
.B \-n \fInlines\fP
sets the number of lines of history that the server process stores to \fInlines\fP. When 
a client connects \fInlines\fP of history are injected into the outer terminal emulator
so that they can be seen when scrolling back. By default the server stores 200 lines of
history.
.TP 5
.B \-S
tells the 
.I sympathy
server process to log errors to syslog.
.SH OPERATION
When 
.I sympathy
is relaying data to the outer terminal emulator a reverse video status line 
will be visible at the bottom of the screen. The status line shows pertinent
information. The first item on the line reminds you what the current escape character
is, the second indicates the terminal device to which 
.I sympathy
is connected, and the third shows the current baud\-rate. Other messages are:
.TP 5
.B Flow
indicates that that RTS/CTS flow control is in operation on the terminal device.
.TP 5
.B RTS
indicates that the terminal device is asserting the RTS line which indicates that 
the local system is ready to accept data from the remote system. If RTS/CTS
flow control is in operation then the operating system or hardware may
de-assert RTS even if RTS is shown. See the section on SERIAL PORT THEORY for
more information.
.TP 5
.B CTS
indicates that the terminal device has detected that the local system's CTS
line is being asserted, indicating that the remote system is ready to receive
data from the local system. See the section on SERIAL PORT THEORY for
more information.
.TP 5
.B DTR
indicates that the terminal device is asserting the DTR line indicating that the local
system would like the local DCE to establish a connection to the remote
DCE.  See the section on SERIAL PORT THEORY for more information.
.TP 5
.B DSR
indicates that the terminal device has detected that the local system's DSR line is
being asserted, indicating that the local DCE is ready. See the section on
SERIAL PORT THEORY for more information.
.TP 5
.B CD
indicates that the terminal device has detected that the local system's CD line is
being asserted, indicating that the local DCE has a connection to the remote DCE.
See the section on SERIAL PORT THEORY for more information.
.TP 5
.B RI
indicates that the terminal device has detected that the local system's RI line is
being asserted, indicating that the DCE has detected a ringing signal or incoming 
connexion.
.TP 5
.B n clients
shows the number of connected client processes. In the \-\fBt\fP major mode, this will 
always be zero.
.TP 5
.B Locked
the terminal device was opened without the \-\fbK\fP flag and another process is
currently using it. I/O to the device is currently suspended until the process dies
or removes its lock file.
.TP 5
.B n errs
indicates the number of frames received by the terminal device with errors (indicating 
the wrong parity, baud\-rate or framing). The count resets if no errors are
detected by the device for 10 seconds.
.TP 5
.B try higher
.I Sympathy
thinks that you have set the wrong baud\-rate and is unable to determine the correct
one as the current baud\-rate is lower than the correct baud\-rate. Use the \fBbaud\fP command
to set a higher baud\-rate (eg 115200) and 
.I sympathy 
will try again.
.TP 5
.B try \fIrate\fBb 
.I Sympathy
thinks that you have set the wrong baud\-rate and thinks that the correct baud\-rate is
\fIrate\fP. Use the \fBbaud\fP command to change the current baud\-rate.
.SH COMMANDS
Commands are entered by sending the escape character, ascii(7) STX, from the outer terminal 
emulator (usually by pressing CTRL\-B), typing the command and pressing return. Whilst the
command is entered the status line changes to `:' and rudimentary line editing is available.
Whilst the command is entered the cursor \fBdoes not move\fP but remains where the terminal
emulator has placed it. Pressing the escape character whilst in command mode
will send the escape character to the terminal and cancel command mode. Valid
commands are:
.TP 7
.B ansi
switch from VT102 behaviour to ANSI behaviour. The most noticeable difference is 
the so\-called `xn' glitch.
.TP 7
.B noansi
switch from ANSI behaviour to VT102 behaviour.
.TP 7
.B baud \fInnnn\fB
set the current baud\-rate to nnnn
.TP 7
.B break
send the break signal by asserting the TX line for longer than a frame period.
.TP 7
.B flow
enable RTS/CTS flow control
.TP 7
.B noflow
disable RTS/CTS flow control
.TP 7
.B hangup
de-assert DTR for one second.
.TP 7
.B width \fInn\fB
set the current width of the screen to \fInn\fP, and reset the terminal emulator.
.TP 7
.B height \fInn\fB
set the current height of the screen to \fInn\fP, and reset the terminal emulator.
.TP 7
.B reset
reset the terminal emulator
.TP 7
.B expand
expand the size of the screen to fit the size of the current outer terminal emulator window
.TP 7
.B quit
exit this instance of 
.I sympathy 
(disconnect from the server if present)
.SH CHARACTER ENCODINGS
For characters between 32 and 126 
.I sympathy
interprets them as would a VT102 terminal by following the subset of ISO-2202 that 
the VT102 supports. Characters 128 thru 255 are assumed to be in UTF\-8(7), if
however the UTF\-8 is invalid they will instead be interpreted as characters
from ISO_8859-1(7). Character 155 (0x9b) when not part of a valid UTF\-8 sequence
will be interpreted as the one byte CSI character.
.PP
For the outer terminal emulator 
.I sympathy 
by default issues the 
ESC % G sequence to select UTF\-8 mode and emits valid UTF-8. If the outer terminal
does not, however, support UTF\-8 use the \-\fBu\fP switch to force 
.I sympathy 
to use the VT102 subset of ISO-2202.
.SH LOG FILES
Log files are made exclusively in the UTF\-8 encoding. Each line in the log file
starts with the date and time at which the entry was made \- for example:
.IP
Feb 27 23:24:42.509440
.PP
.I Sympathy
logs a line to the file whenever the cursor leaves the line. Additionally 
.I sympathy
.IP \(bu 3
logs certain other events to the file: 
.IP \(bu 3
serial port parameter changes: baud\-rate and flow control.
.IP \(bu 3
serial port control line state changes.
.IP \(bu 3
serial port line discipline errors.
.IP \(bu 3
serial port errors.
.IP \(bu 3
suggested baud rates and bit frequency analyses.
.IP \(bu 3
transmission of breaks.
.IP \(bu 3
sending of the hangup signal (dropping the DTR line).
.IP \(bu 3
unknown or un\-handled messages sent on the socket.
.IP \(bu 3
connexion and disconnexion of clients.
.IP \(bu 3
reception of SIGHUP.
.IP \(bu 3
invalid UTF-8 sequences.
.IP \(bu 3
terminal size changes
.IP \(bu 3
un\-handled terminal command sequences
.PP
The log file is rotated when it gets too large if the \fI\-R\fP option
is specified, and the log file is re-opened on receipt of a \fBSIGHUP\fP
which together with the \fB\-P\fP allows the use of of a program such
as logrotate(8)
.SH AUTOMATIC BAUD RATE ALGORITHM
If
.I sympathy
detects a framing error on the serial port it displays the count of
errors on the status line, and logs the error. 
.IP
<tty reports error: \\377 \\000 \\000>
.PP
The count is reset to zero after ten seconds
have elapsed during which no errors have occurred.
.I Sympathy 
looks at bit patterns of the characters received, and
measures the length (in units of the receiving UART's
bit period) of any runs of '1's delimited by '0's and vice\-versa. It
then calculates the statistics on the length of these runs, and logs these.
.IP
<tty_bit_analyse: 0000000001  [0,0,0,0,0,0,110,0,0,80]>
.PP
For a typical
stream of ASCII data, the most common run length will be the correct bit
period. 
.I Sympathy
uses this together with the current bit period to calculate the most probable
correct baud\-rate, which is displayed in the status line, and logged. 
If the correct baud\irate is higher than the current baud\-rate then the most common bit frequency will be '0' or '1' and the correct baud\-rate cannot be
determined. In this case sympathy will display and log the message 'try higher'.
.IP
<tty_analyse:     80 errors, current rate 115200b, suggest 19200b>
.PP
The algorithm only works well if the data stream is normal. If the data stream
is generated by the reception, at the wrong baud\-rate, of characters emitted 
by
.I sympathy
then the algorithm will be biased towards suggesting that the baud\-rate 
is too low. Noise on the line will also result in sympathy concluding that
the baud\-rate is too low.
.SH SIGNALS
.I Sympathy 
reacts to a set of signals. You can use the \fB-P\fP option
to easily determine the PID of the
.I sympathy
process to which you would like to send a signal.
.TP 8
.B SIGINT
.I Sympathy
will immediately try to restore the outer terminal emulator to its original
state and then exit.
.TP 8
.B SIGHUP
.I Sympathy
will close and reopen the log-file specified with the -L option, which allows
the use of programs like logrotate(8)
.TP 8
.B SIGWINCH
.I Sympathy
will redraw the display in the outer terminal emulator so that it will fit within
the new display size.
.TP 8
.B SIGCHLD
.I Sympathy
will wait for children if some were born (for example from compressing rotated logs)
.SH ENVIRONMENT
.I sympathy 
uses the \fBHOME\fP environment variable to determine the default
location for sockets.
.br
.I sympathy 
sets the value of \fBTERM\fP in pseudo-ttys spawned using the
\-\fBp\fP argument to `xterm'.
.br
.I Sympathy
will use \fBCSI ] 0 ;\fP to set the window title to the name of the
socket or device if \fBTERM\fP starts with \fIxterm\fP or \fIrxvt\fP.
.br
The \fBPATH\fP enviroment variable is searched for the binary to be run
in the pseudo\-tty.
.SH EMULATION
.I Sympathy
completely emulates a VT102 terminal (including the VT52 emulation). 
.I Sympathy 
also emulates a few extra sequences: the xterm(1) ESC ] ... sequences, and 
the ANSI CSI @ and CSI b sequences. The numeric keypad follows exactly the
sequences produced by an xterm rather than the exact VT102/VT220 sequences.
.I Sympathy
also recognises the ESC % G and the ESC % @ sequences to switch between ISO-2202
and UTF\-8 but ignores them (see CHARACTER ENCODING below)
.SH SERIAL PORT THEORY
A serial connexion was originally envisaged to connect a DTE (Data Terminal Equipment)
to a DCE (Data Circuit-terminating Equipment). The DCE (some sort of modem) would assert
the DTE's (the computer or terminal) DSR line to indicate it was ready. The DTE would
assert DTR to indicate to the DCE that it should attempt a connexion to the remote DCE.
Once a connexion was established the DCE would assert the DTE's CD pin. Data could then 
flow between the DTR and the remote DTE via the two DCEs. Flow control was provided
via the RTS and CTS lines. The DTE asserts RTS when it is capable of receiving new data,
and pauses its transmission of data when the CTS line is de-asserted. The local DCE
asserts CTS when the remote DCE detects RTS, and vice versa.
.PP
In modern usage the signals are slightly different, for a typical connexion using modems
DSR indicates that the modem is ready, a drop DTR is used to indicate to the
modem that it should break the connexion to the remote modem.  CD indicates that
the local modem is connected to the remote modem, and CTS and RTS behave as before. Connexion
is established by in-band signalling before CD is asserted.
.PP
For a \fBnull modem\fP cable local DSR and DTR are wired to remote CD, local
CTS to remote RTS, and local RTS to remote CTS. Thus asserting local DTR
asserts local DSR and remote CD, and asserting local RTS asserts remote CTS.
.PP
When RTS/CTS flow control is in operation and the receive buffer becomes full,
the operating system, or the hardware, de-asserts RTS, causing (via the DCEs or
the null modem cable) a de-assertion of remote CTS which in turn causes the
remote DTE to cease transmission.
.SH EXAMPLES
.PP 
using 
.I sympathy 
to mimic screen(1):
.IP
[foo@barhost ~]$ sympathy
.IP
.I Sympathy 
forks. The child becomes a daemon server and launches a new shell in a
pseudo-tty, the original process becomes a client and connects to the server
and shows the output. The user then uses the new shell and after some time
either hangs up on the client or issues CTRL\-B quit, and the client detaches from 
the server.
.IP
Later the user wishes to retrieve her session and to determine which sympathy
sessions are active and issues:
.IP
[foo@barhost ~]$ sympathy \-ls
.br
        /home/foo/.sympathy/barhost.8843       (Active)
.br
[foo@barhost ~]$ 
.IP
The user then issues:
.IP
[foo@barhost ~]$ sympathy \-r 8843
.IP
and is reconnected to her session.
.PP
using sympathy to mimic minicom(1):
.IP
.IP
[foo@barhost ~]$ sympathy \-t \-d /dev/modem \-b 9600 \-K
.IP
.I Sympathy
opens the device /dev/modem and locks it, sets the baud\-rate to 9600 baud and disables
flow control. A VT102 terminal emulator then displays the data from the modem. The user
quits the emulator by issuing CTRL\-B quit, which unlocks the modem and exits 
.I sympathy.
.PP
using 
.I sympathy 
to mimic consolidate(1):
.IP
.IP
[foo@barhost ~]$ sympathy \-s \-d /dev/ttyS13 \-b 19200 \-K \-k /var/sympathy/13 \-L /var/sympathy/13.log -R
.IP
.I Sympathy
becomes a daemon and detaches from the current tty. It then opens the device
/dev/ttyS13 and locks it, sets the baud\-rate to 19200 baud and disables flow
control. 
.I Sympathy
then listens for clients connecting on the socket \fI/var/sympathy/13\fP, whilst logging 
completed lines and changes in status to the file \fI/var/sympathy/13.log\fP, 
rotating the log file when it gets too large.
.IP
A user wishing to see the current status of /dev/ttyS13 issues:
.IP
[foo@barhost ~]$ sympathy \-c \-k /var/sympathy/13 
.br
or
.br
[foo@barhost ~]$ sympathy \-r 13
.IP
and the last 200 lines of history are injected into the history of her outer
terminal emulator and she is connected to /dev/ttyS13. The user disconnects from the
server by issuing CTRL\-B quit.
.PP
using 
.I sympathy 
to mimic script(1):
.IP
.IP
[foo@barhost ~]$ sympathy \-t \-L typescript 
.IP
.I Sympathy
starts a shell in a ptty and logs completed lines to the file typescript. When the shell exits
.I sympathy
will terminate, or the user can press CTRL-B which will close the ptty and send a hangup 
to its controlling process.
.SH SEE ALSO
screen(1) minicom(1) consolidate(1)
.SH STANDARDS
ANSI X3.64, ISO-6429, ECMA-48, ISO-2202, ISO-8859, ISO-10646, Digital Equipment Corp. VT102.
.SH BUGS
.PD
.IP \(bu 3
The command editor and parser should support better line editing.
.IP \(bu 3
It should be possible to change the escape character.
.IP \(bu 3
The HTML generated with the \-\fBH\fP option is ugly.
.IP \(bu 3
No useful error message is generated if opening the terminal device fails in  the
\-\fBc\fP \-\fBs\fP major mode.
.SH AUTHOR
James McKenzie, sympathy@madingley.org

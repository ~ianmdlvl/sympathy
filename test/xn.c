#include <stdio.h>
#include <termios.h>

void
putat (int i)
{
  while (i--)
    putchar ('@');
}

int
main (int argc, char *argv[])
{
  struct termios raw, old;

  tcgetattr (0, &old);
  tcgetattr (0, &raw);
  cfmakeraw (&raw);
  tcsetattr (0, TCSANOW, &raw);


  fputs ("\033[H\033[2J", stdout);
  putat (80);
  fputs ("\r\n", stdout);
  fputs ("*", stdout);
  fputs ("\r\n\n", stdout);

  putat (80);
  fputs ("a", stdout);
  fputs ("\r\n", stdout);
  fputs ("*", stdout);
  fputs ("\r\n\n", stdout);

  putat (80);
  fputs ("\rb\n", stdout);
  fputs ("*", stdout);
  fputs ("\r\n\n", stdout);

  putat (80);
  fputs ("\nc\n", stdout);
  fputs ("*", stdout);
  fputs ("\r\n\n", stdout);

  putat (80);
  fputs ("\033[Cd", stdout);
  fputs ("\r\n", stdout);
  fputs ("*", stdout);
  fputs ("\r\n\n", stdout);


  putat (80);
  fputs ("\033[De", stdout);
  fputs ("\r\n", stdout);
  fputs ("*", stdout);
  fputs ("\r\n\n", stdout);


  tcsetattr (0, TCSANOW, &old);

  return 0;
}
